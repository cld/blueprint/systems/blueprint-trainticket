package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

func CreateContactService (contactDatabase components.NoSQLDatabase) *ContactServiceImpl {
	
	return &ContactServiceImpl{
		db: contactDatabase,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type ContactService interface{
	GetAllContacts(ctx context.Context, token string)([]Contact, error)
	CreateNewContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	CreateNewContactsAdmin(ctx context.Context, contact Contact, token string) (Contact, error)
	DeleteContacts(ctx context.Context, contactId, token string) (string, error)
	ModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	FindContactsByAccountId(ctx context.Context, accountId, token string) (Contact, error)
	GetContactsByContactId(ctx context.Context, Id, token string) (Contact, error)
}

type ContactServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}

func(csi *ContactServiceImpl) GetAllContacts(ctx context.Context, token string)([]Contact, error){

	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var contacts []Contact
	err = result.All(&contacts)
	if err != nil{
		return nil, err
	}

	return contacts, nil
}

func(csi *ContactServiceImpl) CreateNewContacts(ctx context.Context, contact Contact, token string) (Contact, error){

	err := Authenticate(token, csi.roles[1])

	if err != nil {
		return Contact{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")
	
	query := fmt.Sprintf(`{"AccountId": %s }`, contact.AccountId)

	res, err := collection.FindOne(query) 

	if err == nil {
		var oldContact Contact

		res.Decode(&oldContact)
		
		if oldContact.Id != "" {
			return Contact{}, errors.New("Contact already exists!")
		}
	}
	
	contact.Id = uuid.New().String()
	err = collection.InsertOne(contact) 
	if err != nil{
		return Contact{}, err
	}

	return contact, nil
}

func(csi *ContactServiceImpl) CreateNewContactsAdmin(ctx context.Context, contact Contact, token string) (Contact, error){

	err := Authenticate(token, csi.roles[0])

	if err != nil {
		return Contact{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")
	
	query := fmt.Sprintf(`{"Id": %s }`, contact.Id)

	res, err := collection.FindOne(query) 

	if err == nil {
		var oldContact Contact

		res.Decode(&oldContact)
		
		if oldContact.Id != "" {
			return Contact{}, errors.New("Contact already exists!")
		}
	}
	
	//* for ADMIN we expect the contact to be sent along in the request
	//contact.Id = uuid.New().String()
	err = collection.InsertOne(contact) 
	if err != nil{
		return Contact{}, err
	}

	return contact, nil
}

func(csi *ContactServiceImpl) DeleteContacts(ctx context.Context, contactId, token string) (string, error){

	err := Authenticate(token, csi.roles...)
	if err != nil {
		return "", err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")
	query := fmt.Sprintf(`{"Id": %s }`, contactId)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Contact removed successfully", nil
}

func(csi *ContactServiceImpl) ModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error){

	err := Authenticate(token, csi.roles...)

	if err != nil {
		return Contact{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")
	
	query := fmt.Sprintf(`{"Id": %s }`, contact.Id)

	res, err := collection.FindOne(query) 

	if err != nil {
		return Contact{}, err
	}

	var existingContact Contact
	res.Decode(&existingContact)
	
	if existingContact.Id == ""{
		return Contact{}, errors.New("Could not find contact!")
	}

	query = fmt.Sprintf(`{"Id": %s}`, contact.Id)

	err = collection.ReplaceOne(query, contact)
	if err != nil{
		return Contact{}, err
	}

	return contact, nil
}

func(csi *ContactServiceImpl) FindContactsByAccountId(ctx context.Context, accountId, token string) (Contact, error){

	err := Authenticate(token)
	if err != nil {
		return Contact{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")

	query := fmt.Sprintf(`{"AccountId": %s}`, accountId)

	result, err := collection.FindOne(query)

	if err != nil{
		return Contact{}, err
	}

	var contact Contact
	err = result.Decode(&contact)
	if err != nil{
		return Contact{}, err
	}

	return contact, nil
}

func(csi *ContactServiceImpl) GetContactsByContactId(ctx context.Context, Id, token string) (Contact, error){

	err := Authenticate(token)
	if err != nil {
		return Contact{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("contacts")

	query := fmt.Sprintf(`{"Id": %s}`, Id)

	result, err := collection.FindOne(query)

	if err != nil{
		return Contact{}, err
	}

	var contact Contact
	err = result.Decode(&contact)
	if err != nil{
		return Contact{}, err
	}

	return contact, nil
}
