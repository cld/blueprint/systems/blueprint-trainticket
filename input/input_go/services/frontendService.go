package services

import (
	"context"
)

type FrontEndService interface {
	// Admin
	AdmingGetAllContacts(ctx context.Context, token string) ([]Contact, error)
	AdminDeleteContacts(ctx context.Context, contactID string, token string) (string, error)
	AdminModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	AdminAddContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	AdminGetAllStations(ctx context.Context, token string) ([]Station, error)
	AdminDeleteStation(ctx context.Context, station Station, token string) (string, error)
	AdminGetAllTrains(ctx context.Context, token string) ([]Train, error)
	AdminDeleteTrain(ctx context.Context, id string, token string) (string, error)
	AdminModifyTrain(ctx context.Context, train Train, token string) (Train, error)
	AdminAddTrain(ctx context.Context, train Train, token string) (Train, error)
	AdminGetAllConfigs(ctx context.Context, token string) ([]Config, error)
	AdminDeleteConfig(ctx context.Context, name string, token string) (string, error)
	AdminModifyConfig(ctx context.Context, config Config, token string) (Config, error)
	AdminAddConfig(ctx context.Context, config Config, token string) (Config, error)
	AdminGetAllPrices(ctx context.Context, token string) ([]PriceConfig, error)
	AdminDeletePrice(ctx context.Context, pc PriceConfig, token string) (string, error)
	AdminModifyPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
	AdminAddPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
	AdminGetAllOrders(ctx context.Context, token string) ([]Order, error)
	AdminDeleteOrder(ctx context.Context, orderID string, token string) (string, error)
	AdminUpdateOrder(ctx context.Context, order Order, token string) (Order, error)
	AdminAddOrder(ctx context.Context, order Order, token string) (Order, error)
	AdminGetAllRoutes(ctx context.Context, token string) ([]Route, error)
	AdminAddRoute(ctx context.Context, id string, startStation string, endStation string, stationList string, distanceList string, token string) (Route, error)
	AdminDeleteRoute(ctx context.Context, routeId string, token string) (string, error)
	AdminGetAllTravels(ctx context.Context, token string) ([]Trip, []Train, []Route, error)
	AdminAddTravel(ctx context.Context, tripID string, trainTypeID string, number string, routeID string, startingStationID string, stationIDs string, terminalStationId string, startingTime string, endTime string, token string) (Trip, error)
	AdminUpdateTravel(ctx context.Context, tripID string, trainTypeID string, number string, routeID string, startingStationID string, stationIDs string, terminalStationId string, startingTime string, endTime string, token string) (Trip, error)
	AdminDeleteTravel(ctx context.Context, tripID string, token string) (string, error)
	AdminGetAllUsers(ctx context.Context, token string) ([]User, error)
	AdminDeleteUser(ctx context.Context, userId string, token string) (string, error)
	AdminUpdateUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error)
	AdminAddUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error)
	// Foods
	FindAllFoodOrder(ctx context.Context, token string) ([]FoodOrder, error)
	CreateFoodOrder(ctx context.Context, foodOrder FoodOrder, token string) (FoodOrder, error)
	UpdateFoodOrder(ctx context.Context, foodOrder FoodOrder, token string) (FoodOrder, error)
	CancelFoodOrder(ctx context.Context, orderId string, token string) (string, error)
	FindFoodOrderByOrderId(ctx context.Context, orderId string, token string) ([]FoodOrder, error)
	GetAllFoods(ctx context.Context, date string, startStation string, endStation string, tripID string, token string) ([]Food, map[string]Store, error)
	// Route Planning
	GetCheapestRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetQuickestRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetMinStopStationsRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	// Consign Voucher
	InsertConsign(ctx context.Context, consign Consign, token string) (Consign, error)
	UpdateConsign(ctx context.Context, consign Consign, token string) (Consign, error)
	FindConsignByAccountId(ctx context.Context, accountID string, token string) ([]Consign, error)
	FindConsignByOrderId(ctx context.Context, orderID string, token string) ([]Consign, error)
	FindConsignByConsignee(ctx context.Context, consignee string, token string) ([]Consign, error)
	GetVoucher(ctx context.Context, orderId string, token string) (Voucher, error)
	PostVoucher(ctx context.Context, orderId string, typ string, token string) (Voucher, error)
	// Travel Planning
	GetTransferResult(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, []TripDetails, error)
	GetTripByCheapest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetTripByQuickest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetTripByMinStation(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	// Ticket Office
	GetRegionList(ctx context.Context, token string) (string, error)
	GetAllOffices(ctx context.Context, token string) ([]Office, error)
	GetSpecificOffices(ctx context.Context, province string, city string, region string, token string) ([]Office, error)
	AddOffice(ctx context.Context, province string, city string, region string, office Office, token string) (string, error)
	DeleteOffice(ctx context.Context, province string, city string, region string, officeName string, token string) (string, error)
	UpdateOffice(ctx context.Context, province string, city string, region string, oldOfficeName string, newOffice Office, token string) (string, error)
	// Insurance
	GetAllInsurances(ctx context.Context, token string) ([]Insurance, error)
	GetAllInsuranceTypes(ctx context.Context, token string) ([]InsuranceType, error)
	DeleteInsurance(ctx context.Context, insuranceId string, token string) (string, error)
	DeleteInsuranceByOrderId(ctx context.Context, orderID string, token string) (string, error)
	ModifyInsurance(ctx context.Context, insuranceId string, orderId string, typeIndex int, token string) (Insurance, error)
	CreateNewInsurance(ctx context.Context, typeIndex int, orderId string, token string) (Insurance, error)
	GetInsuranceById(ctx context.Context, id string, token string) (Insurance, error)
	FindInsuranceByOrderId(ctx context.Context, orderId string, token string) (Insurance, error)
	// Routes
	CreateAndModifyRoute(ctx context.Context, id string, startStation string, endStation string, stationList string, distanceList string, token string) (Route, error)
	DeleteRoute(ctx context.Context, routeId string, token string) (string, error)
	QueryRoutesById(ctx context.Context, routeId string, token string) (Route, error)
	QueryAllRoutes(ctx context.Context, token string) ([]Route, error)
	QueryRoutesByStartAndTerminal(ctx context.Context, startId string, terminalId string, token string) ([]Route, error)
	// Rebooking
	PayBookingDifference(ctx context.Context, info RebookInfo, token string) (Order, error)
	Rebook(ctx context.Context, info RebookInfo, token string) (Order, error)
	//Stations
	QueryStationsById(ctx context.Context, stationId string, token string) (string, error)
	QueryStations(ctx context.Context, token string) ([]Station, error)
	CreateStation(ctx context.Context, station Station, token string) (Station, error)
	UpdateStation(ctx context.Context, station Station, token string) (Station, error)
	DeleteStation(ctx context.Context, station Station, token string) (string, error)
	QueryForStationId(ctx context.Context, stationName string, token string) (string, error)
	QueryForStationIdBatch(ctx context.Context, stationNameList []string, token string) ([]string, error)
	QueryForStationNameBatch(ctx context.Context, stationIdList []string, token string) ([]string, error)
	// Cancellations
	CalculateRefund(ctx context.Context, orderId string, token string) (float32, error)
	CancelTicket(ctx context.Context, orderId string, loginId string, token string) (string, error)
	//Payments
	Pay(ctx context.Context, orderId string, price string, userId string, token string) (Payment, error)
	AddMoneyToAccount(ctx context.Context, userId string, price string, token string) (Payment, error)
	QueryPayments(ctx context.Context, token string) ([]Payment, error)
	PayInside(ctx context.Context, tripId string, userId string, orderId string, token string) (string, error)
	CreatePaymentAccount(ctx context.Context, money string, userId string, token string) (string, error)
	QueryAccount(ctx context.Context, token string) ([]Balances, error)
	DrawBack(ctx context.Context, userId string, money string, token string) (string, error)
	PayDifference(ctx context.Context, orderId string, userId string, price string, token string) (string, error)
	QueryAddMoney(ctx context.Context, token string) ([]AddMoney, error)
	// Notifications
	NotifyPreserveSuccess(ctx context.Context, info NotificationInfo, token string) error
	NotifyOrderCreateSuccess(ctx context.Context, info NotificationInfo,token string) error
	NotifyOrderChangedSuccess(ctx context.Context, info NotificationInfo, token string) error
	NotifyOrderCanceledSuccess(ctx context.Context, info NotificationInfo, token string) error
	// Basic Info and Ticket Info
	BasicQueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error)
	TicketInfoQueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error)
	//Prices
	QueryPrice(ctx context.Context, routeId string, trainType string, token string) (PriceConfig, error)
	QueryAllPrices(ctx context.Context, token string) ([]PriceConfig, error)
	CreatePriceConfig(ctx context.Context, info PriceConfig, token string) (PriceConfig, error)
	UpdatePriceConfig(ctx context.Context, info PriceConfig, token string) (PriceConfig, error)
	// Preserve (Booking)
	Preserve(ctx context.Context, info OrderInfo, token string) (string, error)
	PreserveOther(ctx context.Context, info OrderInfo, token string) (string, error)
	//Orders
	RefreshOrders(ctx context.Context, orderInfo OrderInfo, accountId string, token string) ([]Order, error)
	RefreshOtherOrders(ctx context.Context, orderInfo OrderInfo, accountId string, token string) ([]Order, error)
	//Contacts
	GetAllContacts(ctx context.Context, token string) ([]Contact, error)
	CreateNewContact(ctx context.Context, contact Contact, token string) (Contact, error)
	DeleteContact(ctx context.Context, contactId string, token string) (string, error)
	ModifyContact(ctx context.Context, contact Contact, token string) (Contact, error)
	FindContactsByAccountId(ctx context.Context, accountId string, token string) (Contact, error)
	GetContactsByContactId(ctx context.Context, id string, token string) (Contact, error)
	// Execution of tickets
	ExecuteTicket(ctx context.Context, orderId string, token string) (string, error)
	CollectTicket(ctx context.Context, orderId string, token string) (string, error)
	// Security
	FindAllSecurityConfigs(ctx context.Context, token string) ([]SecurityConfig, error)
	CreateSecurityConfig(ctx context.Context, name string, value string,description string, token string) (SecurityConfig, error)
	UpdateSecurityConfig(ctx context.Context, id string, name string, value string, description string, token string) (SecurityConfig, error)
	DeleteSecurityConfig(ctx context.Context, id string, token string) (string, error)
	PerformSecurityCheck(ctx context.Context, accountID string, token string) (string, error)
	// Configs
	QueryAllConfigs(ctx context.Context) ([]Config, error)
	CreateConfig(ctx context.Context, info Config) (Config, error)
	UpdateConfig(ctx context.Context, info Config) (Config, error)
	DeleteConfig(ctx context.Context, configName string) (string, error)
	RetrieveConfig(ctx context.Context, configName string) (Config, error)
	// Trains
	CreateTrainType(ctx context.Context, id string, economyClass int, comfortClass int, avgSpeed int, token string) (Train, error)
	UpdateTrainType(ctx context.Context, id string, economyClass int, comfortClass int, avgSpeed int, token string) (Train, error)
	DeleteTrainType(ctx context.Context, id string, token string) (string, error)
	QueryTrainTypes(ctx context.Context, token string) ([]Train, error)
	RetrieveTrainType(ctx context.Context, id string, token string) (Train, error)
	// Accounts and Verification
	GetAllUsers(ctx context.Context, token string) ([]User, error)
	GetUserById(ctx context.Context, userID string, token string) (User, error)
	GetUserByUsername(ctx context.Context, username string, token string) (User, error)
	DeleteUserById(ctx context.Context, userId string, token string) (string, error)
	UpdateUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error)
	RegisterUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error)
	GetAllUsersAuth(ctx context.Context, token string) ([]User, error)
	DeleteUserByIdAuth(ctx context.Context, userId string, token string) (string, error)
	CreateDefaultUser(ctx context.Context, username string, userId string, password string, token string) (string, error)
	Login(ctx context.Context, username string, password string, verificationCode string, captcha_cokie map[string]string) (string, error)
	GenerateCaptcha(ctx context.Context, cookie map[string]string) (Captcha, string, error)
	GetAvailableTrips(ctx context.Context, startingPlace string, endPlace string, departureTime string, token string) ([]TripDetails, error)
	GetAvailableTripsOther(ctx context.Context, startingPlace string, endPlace string, departureTime string, token string) ([]TripDetails, error)
}

type FrontEndServiceImpl struct {
	adminBasicInfoService AdminBasicInfoService
	adminOrderService AdminOrderService
	adminRouteService AdminRouteService
	adminTravelService AdminTravelService
	adminUserService AdminUserService
	foodService FoodService
	routePlanService RoutePlanService
	consignService ConsignService
	voucherService VoucherService
	travelPlanService TravelPlanService
	ticketOfficeService TicketOfficeService
	insuranceService InsuranceService
	routeService RouteService
	rebookService RebookService
	stationService StationService
	cancelService CancelService
	paymentService PaymentService
	insidePaymentService InsidePaymentService
	notificationService NotificationService
	basicService BasicService
	ticketInfoService TicketInfoService
	priceService PriceService
	preserveOtherService PreserveOtherService
	preserveService PreserveService
	orderOtherService OrderOtherService
	orderService OrderService
	contactService ContactService
	trainService TrainService
	userService UserService
	authService AuthService
	verificationCodeService VerificationCodeService
	travel2Service Travel2Service
	travelService TravelService
	executeService ExecuteService
	securityService SecurityService
	configService ConfigService
}

func NewFrontEndServiceImpl(travelService TravelService, travel2Service Travel2Service, verificationCodeService VerificationCodeService, authService AuthService, userService UserService, trainService TrainService, configService ConfigService, securityService SecurityService, executeService ExecuteService, contactService ContactService, orderService OrderService, orderOtherService OrderOtherService, preserveService PreserveService, preserveOtherService PreserveOtherService, priceService PriceService, ticketInfoService TicketInfoService, basicService BasicService, notificationService NotificationService, insidePaymentService InsidePaymentService, paymentService PaymentService, cancelService CancelService, stationService StationService, rebookService RebookService, routeService RouteService, insuranceService InsuranceService, ticketOfficeService TicketOfficeService, travelPlanService TravelPlanService, consignService ConsignService, voucherService VoucherService, routePlanService RoutePlanService, foodService FoodService, adminBasicInfoService AdminBasicInfoService, adminOrderService AdminOrderService, adminRouteService AdminRouteService, adminTravelService AdminTravelService, adminUserService AdminUserService) *FrontEndServiceImpl {
	return &FrontEndServiceImpl{
        adminBasicInfoService : adminBasicInfoService,
        adminOrderService : adminOrderService,
        adminRouteService : adminRouteService,
        adminTravelService : adminTravelService,
        adminUserService : adminUserService,
        foodService : foodService,
        routePlanService : routePlanService,
        consignService : consignService,
        voucherService : voucherService,
        travelPlanService : travelPlanService,
        ticketOfficeService : ticketOfficeService,
        insuranceService : insuranceService,
        routeService : routeService,
        rebookService : rebookService,
        stationService : stationService,
        cancelService : cancelService,
        paymentService : paymentService,
        insidePaymentService : insidePaymentService,
        notificationService : notificationService,
        basicService : basicService,
        ticketInfoService : ticketInfoService,
        priceService : priceService,
        preserveOtherService : preserveOtherService,
        preserveService : preserveService,
        orderOtherService : orderOtherService,
        orderService : orderService,
        contactService : contactService,
        executeService : executeService,
        securityService : securityService,
        configService : configService,
        trainService : trainService,
        userService : userService,
        authService : authService,
        verificationCodeService : verificationCodeService,
        travel2Service : travel2Service,
        travelService : travelService,
	}
}

func (f *FrontEndServiceImpl) AdmingGetAllContacts(ctx context.Context, token string) ([]Contact, error) {
	return f.adminBasicInfoService.GetAllContacts(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeleteContacts(ctx context.Context, contactID string, token string) (string, error) {
	return f.adminBasicInfoService.DeleteContacts(ctx, contactID, token)
}
func (f *FrontEndServiceImpl) AdminModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error) {
	return f.adminBasicInfoService.ModifyContacts(ctx, contact, token)
}
func (f *FrontEndServiceImpl) AdminAddContacts(ctx context.Context, contact Contact, token string) (Contact, error) {
	return f.adminBasicInfoService.AddContacts(ctx, contact, token)
}
func (f *FrontEndServiceImpl) AdminGetAllStations(ctx context.Context, token string) ([]Station, error) {
	return f.adminBasicInfoService.GetAllStations(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeleteStation(ctx context.Context, station Station, token string) (string, error) {
	return f.adminBasicInfoService.DeleteStation(ctx, station, token)
}
func (f *FrontEndServiceImpl) AdminGetAllTrains(ctx context.Context, token string) ([]Train, error) {
	return f.adminBasicInfoService.GetAllTrains(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeleteTrain(ctx context.Context, id string, token string) (string, error) {
	return f.adminBasicInfoService.DeleteTrain(ctx, id, token)
}
func (f *FrontEndServiceImpl) AdminModifyTrain(ctx context.Context, train Train, token string) (Train, error) {
	return f.adminBasicInfoService.ModifyTrain(ctx, train, token)
}
func (f *FrontEndServiceImpl) AdminAddTrain(ctx context.Context, train Train, token string) (Train, error) {
	return f.adminBasicInfoService.AddTrain(ctx, train, token)
}
func (f *FrontEndServiceImpl) AdminGetAllConfigs(ctx context.Context, token string) ([]Config, error) {
	return f.adminBasicInfoService.GetAllConfigs(ctx)
}
func (f *FrontEndServiceImpl) AdminDeleteConfig(ctx context.Context, name string, token string) (string, error) {
	return f.adminBasicInfoService.DeleteConfig(ctx, name, token)
}
func (f *FrontEndServiceImpl) AdminModifyConfig(ctx context.Context, config Config, token string) (Config, error) {
	return f.adminBasicInfoService.ModifyConfig(ctx, config)
}
func (f *FrontEndServiceImpl) AdminAddConfig(ctx context.Context, config Config, token string) (Config, error) {
	return f.adminBasicInfoService.AddConfig(ctx, config)
}
func (f *FrontEndServiceImpl) AdminGetAllPrices(ctx context.Context, token string) ([]PriceConfig, error) {
	return f.adminBasicInfoService.GetAllPrices(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeletePrice(ctx context.Context, pc PriceConfig, token string) (string, error) {
	return f.adminBasicInfoService.DeletePrice(ctx, pc, token)
}
func (f *FrontEndServiceImpl) AdminModifyPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error) {
	return f.adminBasicInfoService.ModifyPrice(ctx, pc, token)
}
func (f *FrontEndServiceImpl) AdminAddPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error) {
	return f.adminBasicInfoService.AddPrice(ctx, pc, token)
}
func (f *FrontEndServiceImpl) AdminGetAllOrders(ctx context.Context, token string) ([]Order, error) {
	return f.adminOrderService.GetAllOrders(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeleteOrder(ctx context.Context, orderID string, token string) (string, error) {
	return f.adminOrderService.DeleteOrder(ctx, orderID, token)
}
func (f *FrontEndServiceImpl) AdminUpdateOrder(ctx context.Context, order Order, token string) (Order, error) {
	return f.adminOrderService.UpdateOrder(ctx, order, token)
}
func (f *FrontEndServiceImpl) AdminAddOrder(ctx context.Context, order Order, token string) (Order, error) {
	return f.adminOrderService.AddOrder(ctx, order, token)
}
func (f *FrontEndServiceImpl) AdminGetAllRoutes(ctx context.Context, token string) ([]Route, error) {
	return f.adminRouteService.GetAllRoutes(ctx, token)
}
func (f *FrontEndServiceImpl) AdminAddRoute(ctx context.Context, id string, startStation string, endStation string, stationList []string, distanceList []uint16, token string) (Route, error) {
	routereq := RouteRequest{Id: id, StartStation: startStation, EndStation: endStation, Stations: stationList, Distances: distanceList}
	return f.adminRouteService.AddRoute(ctx, routereq, token)
}
func (f *FrontEndServiceImpl) AdminDeleteRoute(ctx context.Context, routeId string, token string) (string, error) {
	return f.adminRouteService.DeleteRoute(ctx, routeId, token)
}
func (f *FrontEndServiceImpl) AdminGetAllTravels(ctx context.Context, token string) ([]Trip, []Train, []Route, error) {
	return f.adminTravelService.GetAllTravels(ctx, token)
}
func (f *FrontEndServiceImpl) AdminAddTravel(ctx context.Context, tripID string, trainTypeID string, number string, routeID string, startingStationID string, stationIDs string, terminalStationId string, startingTime string, endTime string, token string) (Trip, error) {
	trip := Trip{tripID, trainTypeID, number, routeID, startingTime, endTime, startingStationID, stationIDs, terminalStationId}
	return f.adminTravelService.AddTravel(ctx, trip, token)
}
func (f *FrontEndServiceImpl) AdminUpdateTravel(ctx context.Context, tripID string, trainTypeID string, number string, routeID string, startingStationID string, stationIDs string, terminalStationId string, startingTime string, endTime string, token string) (Trip, error) {
	trip := Trip{tripID, trainTypeID, number, routeID, startingTime, endTime, startingStationID, stationIDs, terminalStationId}
	return f.adminTravelService.UpdateTravel(ctx, trip, token)
}
func (f *FrontEndServiceImpl) AdminDeleteTravel(ctx context.Context, tripID string, token string) (string, error) {
	return f.adminTravelService.DeleteTravel(ctx, tripID, token)
}
func (f *FrontEndServiceImpl) AdminGetAllUsers(ctx context.Context, token string) ([]User, error) {
	return f.adminUserService.GetAllUsers(ctx, token)
}
func (f *FrontEndServiceImpl) AdminDeleteUser(ctx context.Context, userId string, token string) (string, error) {
	return f.adminUserService.DeleteUser(ctx, userId, token)
}
func (f *FrontEndServiceImpl) AdminUpdateUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error) {
	user := User{Username: username, Password: password, Gender: uint16(gender), DocumentType: uint16(documentType), DocumentNumber: documentNum, Email: email}
	return f.adminUserService.UpdateUser(ctx, user, token)
}
func (f *FrontEndServiceImpl) AdminAddUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error) {
	user := User{Username: username, Password: password, Gender: uint16(gender), DocumentType: uint16(documentType), DocumentNumber: documentNum, Email: email}
	return f.adminUserService.AddUser(ctx, user, token)
}
func (f *FrontEndServiceImpl) FindAllFoodOrder(ctx context.Context, token string) ([]FoodOrder, error) {
	return f.foodService.FindAllFoodOrder(ctx, token)
}
func (f *FrontEndServiceImpl) CreateFoodOrder(ctx context.Context, foodOrder FoodOrder, token string) (FoodOrder, error) {
	return f.foodService.CreateFoodOrder(ctx, foodOrder, token)
}
func (f *FrontEndServiceImpl) UpdateFoodOrder(ctx context.Context, foodOrder FoodOrder, token string) (FoodOrder, error) {
	return f.foodService.UpdateFoodOrder(ctx, foodOrder, token)
}
func (f *FrontEndServiceImpl) CancelFoodOrder(ctx context.Context, orderId string, token string) (string, error) {
	return f.foodService.DeleteFoodOrder(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) FindFoodOrderByOrderId(ctx context.Context, orderId string, token string) ([]FoodOrder, error) {
	return f.foodService.FindFoodOrderByOrderId(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) GetAllFoods(ctx context.Context, date string, startStation string, endStation string, tripID string, token string) ([]Food, map[string]Store, error) {
	return f.foodService.GetAllFood(ctx, startStation, endStation, tripID, token)
}
func (f *FrontEndServiceImpl) GetCheapestRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.routePlanService.GetCheapestRoutes(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetQuickestRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.routePlanService.GetQuickestRoutes(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetMinStopStationsRoutes(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.routePlanService.GetMinStopStations(ctx, info, token)
}
func (f *FrontEndServiceImpl) InsertConsign(ctx context.Context, consign Consign, token string) (Consign, error) {
	return f.consignService.InsertConsign(ctx, consign, token)
}
func (f *FrontEndServiceImpl) UpdateConsign(ctx context.Context, consign Consign, token string) (Consign, error) {
	return f.consignService.UpdateConsign(ctx, consign, token)
}
func (f *FrontEndServiceImpl) FindConsignByAccountId(ctx context.Context, accountID string, token string) ([]Consign, error) {
	return f.consignService.FindByAccountId(ctx, accountID, token)
}
func (f *FrontEndServiceImpl) FindConsignByOrderId(ctx context.Context, orderID string, token string) ([]Consign, error) {
	return f.consignService.FindByOrderId(ctx, orderID, token)
}
func (f *FrontEndServiceImpl) FindConsignByConsignee(ctx context.Context, consignee string, token string) ([]Consign, error) {
	return f.consignService.FindByConsignee(ctx, consignee, token)
}
func (f *FrontEndServiceImpl) GetVoucher(ctx context.Context, orderId string, token string) (Voucher, error) {
	return f.voucherService.GetVoucher(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) PostVoucher(ctx context.Context, orderId string, typ string, token string) (Voucher, error) {
	return f.voucherService.Post(ctx, orderId, typ, token)
}
func (f *FrontEndServiceImpl) GetTransferResult(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, []TripDetails, error) {
	return f.travelPlanService.GetTransferResult(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetTripByCheapest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.travelPlanService.GetByCheapest(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetTripByQuickest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.travelPlanService.GetByQuickest(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetTripByMinStation(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error) {
	return f.travelPlanService.GetByMinStation(ctx, info, token)
}
func (f *FrontEndServiceImpl) GetRegionList(ctx context.Context, token string) (string, error) {
	return f.ticketOfficeService.GetRegionList(ctx, token)
}
func (f *FrontEndServiceImpl) GetAllOffices(ctx context.Context, token string) ([]Office, error) {
	return f.ticketOfficeService.GetAll(ctx, token)
}
func (f *FrontEndServiceImpl) GetSpecificOffices(ctx context.Context, province string, city string, region string, token string) ([]Office, error) {
	return f.ticketOfficeService.GetSpecificOffices(ctx, province, city, region, token)
}
func (f *FrontEndServiceImpl) AddOffice(ctx context.Context, province string, city string, region string, office Office, token string) (string, error) {
	return f.ticketOfficeService.AddOffice(ctx, province, city, region, token, office)
}
func (f *FrontEndServiceImpl) DeleteOffice(ctx context.Context, province string, city string, region string, officeName string, token string) (string, error) {
	return f.ticketOfficeService.DeleteOffice(ctx, province, city, region, officeName, token)
}
func (f *FrontEndServiceImpl) UpdateOffice(ctx context.Context, province string, city string, region string, oldOfficeName string, newOffice Office, token string) (string, error) {
	return f.ticketOfficeService.UpdateOffice(ctx, province, city, region, oldOfficeName, token, newOffice)
}
func (f *FrontEndServiceImpl) GetAllInsurances(ctx context.Context, token string) ([]Insurance, error) {
	return f.insuranceService.GetAllInsurances(ctx, token)
}
func (f *FrontEndServiceImpl) GetAllInsuranceTypes(ctx context.Context, token string) ([]InsuranceType, error) {
	return f.insuranceService.GetAllInsuranceTypes(ctx, token)
}
func (f *FrontEndServiceImpl) DeleteInsurance(ctx context.Context, insuranceId string, token string) (string, error) {
	return f.insuranceService.DeleteInsurance(ctx, insuranceId, token)
}
func (f *FrontEndServiceImpl) DeleteInsuranceByOrderId(ctx context.Context, orderID string, token string) (string, error) {
	return f.insuranceService.DeleteInsuranceByOrderId(ctx, orderID, token)
}
func (f *FrontEndServiceImpl) ModifyInsurance(ctx context.Context, insuranceId string, orderId string, typeIndex int, token string) (Insurance, error) {
	return f.insuranceService.ModifyInsurance(ctx, uint16(typeIndex),insuranceId, orderId, token)
}
func (f *FrontEndServiceImpl) CreateNewInsurance(ctx context.Context, typeIndex int, orderId string, token string) (Insurance, error) {
	return f.insuranceService.CreateNewInsurance(ctx, uint16(typeIndex), orderId, token)
}
func (f *FrontEndServiceImpl) GetInsuranceById(ctx context.Context, id string, token string) (Insurance, error) {
	return f.insuranceService.GetInsuranceById(ctx, id, token)
}
func (f *FrontEndServiceImpl) FindInsuranceByOrderId(ctx context.Context, orderId string, token string) (Insurance, error) {
	return f.insuranceService.FindInsuranceByOrderId(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) CreateAndModifyRoute(ctx context.Context, id string, startStation string, endStation string, stationList []string, distanceList []uint16, token string) (Route, error) {
	route := Route{Id: id, StartStationId: startStation, TerminalStationId: endStation, Stations: stationList, Distances: distanceList}
	return f.routeService.CreateAndModifyRoute(ctx, route, token)
}
func (f *FrontEndServiceImpl) DeleteRoute(ctx context.Context, routeId string, token string) (string, error) {
	return f.routeService.DeleteRoute(ctx, routeId, token)
}
func (f *FrontEndServiceImpl) QueryRoutesById(ctx context.Context, routeId string, token string) (Route, error) {
	return f.routeService.QueryById(ctx, routeId, token)
}
func (f *FrontEndServiceImpl) QueryAllRoutes(ctx context.Context, token string) ([]Route, error) {
	return f.routeService.QueryAll(ctx, token)
}
func (f *FrontEndServiceImpl) QueryRoutesByStartAndTerminal(ctx context.Context, startId string, terminalId string, token string) ([]Route, error) {
	return f.routeService.QueryByStartAndTerminal(ctx, startId, terminalId, token)
}
func (f *FrontEndServiceImpl) PayBookingDifference(ctx context.Context, info RebookInfo, token string) (Order, error) {
	return f.rebookService.PayDifference(ctx, info, token)
}
func (f *FrontEndServiceImpl) Rebook(ctx context.Context, info RebookInfo, token string) (Order, error) {
	return f.rebookService.Rebook(ctx, info, token)
}
func (f *FrontEndServiceImpl) QueryStationsById(ctx context.Context, stationId string, token string) (string, error) {
	return f.stationService.QueryById(ctx, stationId, token)
}
func (f *FrontEndServiceImpl) QueryStations(ctx context.Context, token string) ([]Station, error) {
	return f.stationService.Query(ctx, token)
}
func (f *FrontEndServiceImpl) CreateStation(ctx context.Context, station Station, token string) (Station, error) {
	return f.stationService.Create(ctx, station, token)
}
func (f *FrontEndServiceImpl) UpdateStation(ctx context.Context, station Station, token string) (Station, error) {
	return f.stationService.Update(ctx, station, token)
}
func (f *FrontEndServiceImpl) DeleteStation(ctx context.Context, station Station, token string) (string, error) {
	return f.stationService.Delete(ctx, station, token)
}
func (f *FrontEndServiceImpl) QueryForStationId(ctx context.Context, stationName string, token string) (string, error) {
	return f.stationService.QueryForStationId(ctx, stationName, token)
}
func (f *FrontEndServiceImpl) QueryForStationIdBatch(ctx context.Context, stationNameList []string, token string) ([]string, error) {
	return f.stationService.QueryForIdBatch(ctx, stationNameList, token)
}
func (f *FrontEndServiceImpl) QueryForStationNameBatch(ctx context.Context, stationIdList []string, token string) ([]string, error) {
	return f.stationService.QueryForNameBatch(ctx, stationIdList, token)
}
func (f *FrontEndServiceImpl) CalculateRefund(ctx context.Context, orderId string, token string) (float32, error) {
	return f.cancelService.Calculate(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) CancelTicket(ctx context.Context, orderId string, loginId string, token string) (string, error) {
	return f.cancelService.CancelTicket(ctx, orderId, loginId, token)
}
func (f *FrontEndServiceImpl) Pay(ctx context.Context, orderId string, price string, userId string, token string) (Payment, error) {
	return f.paymentService.Pay(ctx, orderId, price, userId, token)
}
func (f *FrontEndServiceImpl) AddMoneyToAccount(ctx context.Context, userId string, price string, token string) (Payment, error) {
	return f.paymentService.AddMoney(ctx, userId, price, token)
}
func (f *FrontEndServiceImpl) QueryPayments(ctx context.Context, token string) ([]Payment, error) {
	return f.paymentService.Query(ctx, token)
}
func (f *FrontEndServiceImpl) PayInside(ctx context.Context, tripId string, userId string, orderId string, token string) (string, error) {
	return f.insidePaymentService.Pay(ctx, tripId, userId, orderId, token)
}
func (f *FrontEndServiceImpl) CreatePaymentAccount(ctx context.Context, money string, userId string, token string) (string, error) {
	return f.insidePaymentService.CreateAccount(ctx, money, userId, token)
}
func (f *FrontEndServiceImpl) QueryAccount(ctx context.Context, token string) ([]Balances, error) {
	return f.insidePaymentService.QueryAccount(ctx, token)
}
func (f *FrontEndServiceImpl) DrawBack(ctx context.Context, userId string, money string, token string) (string, error) {
	return f.insidePaymentService.DrawBack(ctx, userId, money, token)
}
func (f *FrontEndServiceImpl) PayDifference(ctx context.Context, orderId string, userId string, price string, token string) (string, error) {
	return f.insidePaymentService.PayDifference(ctx, orderId, userId, price, token)
}
func (f *FrontEndServiceImpl) QueryAddMoney(ctx context.Context, token string) ([]AddMoney, error) {
	return f.insidePaymentService.QueryAddMoney(ctx, token)
}
func (f *FrontEndServiceImpl) NotifyPreserveSuccess(ctx context.Context, info NotificationInfo, token string) error {
	return f.notificationService.PreserveSuccess(ctx, info, token)
}
func (f *FrontEndServiceImpl) NotifyOrderCreateSuccess(ctx context.Context, info NotificationInfo,token string) error {
	return f.notificationService.OrderCreateSuccess(ctx, info, token)
}
func (f *FrontEndServiceImpl) NotifyOrderChangedSuccess(ctx context.Context, info NotificationInfo, token string) error {
	return f.notificationService.OrderChangedSuccess(ctx, info, token)
}
func (f *FrontEndServiceImpl) NotifyOrderCanceledSuccess(ctx context.Context, info NotificationInfo, token string) error {
	return f.notificationService.OrderCancelSuccess(ctx, info, token)
}
func (f *FrontEndServiceImpl) BasicQueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error) {
	return f.basicService.QueryForTravel(ctx, info, token)
}
func (f *FrontEndServiceImpl) TicketInfoQueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error) {
	return f.ticketInfoService.QueryForTravel(ctx, info, token)
}
func (f *FrontEndServiceImpl) QueryPrice(ctx context.Context, routeId string, trainType string, token string) (PriceConfig, error) {
	return f.priceService.Query(ctx, routeId, trainType, token)
}
func (f *FrontEndServiceImpl) QueryAllPrices(ctx context.Context, token string) ([]PriceConfig, error) {
	return f.priceService.QueryAll(ctx, token)
}
func (f *FrontEndServiceImpl) CreatePriceConfig(ctx context.Context, info PriceConfig, token string) (PriceConfig, error) {
	return f.priceService.Create(ctx, info, token)
}
func (f *FrontEndServiceImpl) UpdatePriceConfig(ctx context.Context, info PriceConfig, token string) (PriceConfig, error) {
	return f.priceService.Update(ctx, info, token)
}
func (f *FrontEndServiceImpl) Preserve(ctx context.Context, info OrderInfo, token string) (string, error) {
	return f.preserveService.Preserve(ctx, info, token)
}
func (f *FrontEndServiceImpl) PreserveOther(ctx context.Context, info OrderInfo, token string) (string, error) {
	return f.preserveOtherService.Preserve(ctx, info, token)
}
func (f *FrontEndServiceImpl) RefreshOrders(ctx context.Context, orderInfo OrderInfo, accountId string, token string) ([]Order, error) {
	return f.orderService.QueryOrdersForRefresh(ctx, orderInfo, accountId, token)
}
func (f *FrontEndServiceImpl) RefreshOtherOrders(ctx context.Context, orderInfo OrderInfo, accountId string, token string) ([]Order, error) {
	return f.orderOtherService.QueryOrdersForRefresh(ctx, orderInfo, accountId, token)
}
func (f *FrontEndServiceImpl) GetAllContacts(ctx context.Context, token string) ([]Contact, error) {
	return f.contactService.GetAllContacts(ctx, token)
}
func (f *FrontEndServiceImpl) CreateNewContact(ctx context.Context, contact Contact, token string) (Contact, error) {
	return f.contactService.CreateNewContacts(ctx, contact, token)
}
func (f *FrontEndServiceImpl) DeleteContact(ctx context.Context, contactId string, token string) (string, error) {
	return f.contactService.DeleteContacts(ctx, contactId, token)
}
func (f *FrontEndServiceImpl) ModifyContact(ctx context.Context, contact Contact, token string) (Contact, error) {
	return f.contactService.ModifyContacts(ctx, contact, token)
}
func (f *FrontEndServiceImpl) FindContactsByAccountId(ctx context.Context, accountId string, token string) (Contact, error) {
	return f.contactService.FindContactsByAccountId(ctx, accountId, token)
}
func (f *FrontEndServiceImpl) GetContactsByContactId(ctx context.Context, id string, token string) (Contact, error) {
	return f.contactService.GetContactsByContactId(ctx, id, token)
}
func (f *FrontEndServiceImpl) ExecuteTicket(ctx context.Context, orderId string, token string) (string, error) {
	return f.executeService.ExecuteTicket(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) CollectTicket(ctx context.Context, orderId string, token string) (string, error) {
	return f.executeService.CollectTicket(ctx, orderId, token)
}
func (f *FrontEndServiceImpl) FindAllSecurityConfigs(ctx context.Context, token string) ([]SecurityConfig, error) {
	return f.securityService.FindAllSecurityConfigs(ctx, token)
}
func (f *FrontEndServiceImpl) CreateSecurityConfig(ctx context.Context, name string, value string,description string, token string) (SecurityConfig, error) {
	return f.securityService.Create(ctx, name, value, description, token)
}
func (f *FrontEndServiceImpl) UpdateSecurityConfig(ctx context.Context, id string, name string, value string, description string, token string) (SecurityConfig, error) {
	return f.securityService.Update(ctx, id, name, value, description, token)
}
func (f *FrontEndServiceImpl) DeleteSecurityConfig(ctx context.Context, id string, token string) (string, error) {
	return f.securityService.Delete(ctx, id, token)
}
func (f *FrontEndServiceImpl) PerformSecurityCheck(ctx context.Context, accountID string, token string) (string, error) {
	return f.securityService.Check(ctx, accountID, token)
}
func (f *FrontEndServiceImpl) QueryAllConfigs(ctx context.Context) ([]Config, error) {
	return f.configService.QueryAll(ctx)
}
func (f *FrontEndServiceImpl) CreateConfig(ctx context.Context, info Config) (Config, error) {
	return f.configService.CreateConfig(ctx, info)
}
func (f *FrontEndServiceImpl) UpdateConfig(ctx context.Context, info Config) (Config, error) {
	return f.configService.UpdateConfig(ctx, info)
}
func (f *FrontEndServiceImpl) DeleteConfig(ctx context.Context, configName string) (string, error) {
	return f.configService.DeleteConfig(ctx, configName)
}
func (f *FrontEndServiceImpl) RetrieveConfig(ctx context.Context, configName string) (Config, error) {
	return f.configService.Retrieve(ctx, configName)
}
func (f *FrontEndServiceImpl) CreateTrainType(ctx context.Context, id string, economyClass int, comfortClass int, avgSpeed int, token string) (Train, error) {
	train := Train{Id: id, EconomyClass: uint16(economyClass), ComfortClass: uint16(comfortClass), AvgSpeed: uint16(avgSpeed)}
	return f.trainService.Create(ctx, train, token)
}
func (f *FrontEndServiceImpl) UpdateTrainType(ctx context.Context, id string, economyClass int, comfortClass int, avgSpeed int, token string) (Train, error) {
	train := Train{Id: id, EconomyClass: uint16(economyClass), ComfortClass: uint16(comfortClass), AvgSpeed: uint16(avgSpeed)}
	return f.trainService.Update(ctx, train, token)
}
func (f *FrontEndServiceImpl) DeleteTrainType(ctx context.Context, id string, token string) (string, error) {
	return f.trainService.Delete(ctx, id, token)
}
func (f *FrontEndServiceImpl) QueryTrainTypes(ctx context.Context, token string) ([]Train, error) {
	return f.trainService.Query(ctx, token)
}
func (f *FrontEndServiceImpl) RetrieveTrainType(ctx context.Context, id string, token string) (Train, error) {
	return f.trainService.Retrieve(ctx, id, token)
}
func (f *FrontEndServiceImpl) GetAllUsers(ctx context.Context, token string) ([]User, error) {
	return f.userService.GetAllUsers(ctx, token)
}
func (f *FrontEndServiceImpl) GetUserById(ctx context.Context, userID string, token string) (User, error) {
	return f.userService.GetUserById(ctx, userID, token)
}
func (f *FrontEndServiceImpl) GetUserByUsername(ctx context.Context, username string, token string) (User, error) {
	return f.userService.GetUserByUsername(ctx, username, token)
}
func (f *FrontEndServiceImpl) DeleteUserById(ctx context.Context, userId string, token string) (string, error) {
	return f.userService.DeleteUserById(ctx, userId, token)
}
func (f *FrontEndServiceImpl) UpdateUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error) {
	user := User{Username: username, Password: password, Gender: uint16(gender), DocumentType: uint16(documentType), DocumentNumber: documentNum, Email: email}
	return f.userService.UpdateUser(ctx, user, token)
}
func (f *FrontEndServiceImpl) RegisterUser(ctx context.Context, username string, password string, gender int, documentType int, documentNum string, email string, token string) (User, error) {
	user := User{Username: username, Password: password, Gender: uint16(gender), DocumentType: uint16(documentType), DocumentNumber: documentNum, Email: email}
	return f.userService.RegisterUser(ctx, user, token)
}
func (f *FrontEndServiceImpl) GetAllUsersAuth(ctx context.Context, token string) ([]User, error) {
	return f.authService.GetAllUsers(ctx, token)
}
func (f *FrontEndServiceImpl) DeleteUserByIdAuth(ctx context.Context, userId string, token string) (string, error) {
	return f.authService.DeleteUserById(ctx, userId, token)
}
func (f *FrontEndServiceImpl) CreateDefaultUser(ctx context.Context, username string, userId string, password string, token string) (string, error) {
	user := User{Username: username, Password: password, UserId: userId}
	return f.authService.CreateDefaultUser(ctx, user, token)
}
func (f *FrontEndServiceImpl) Login(ctx context.Context, username string, password string, verificationCode string, captcha Captcha) (string, error) {
	return f.authService.Login(ctx, username, password, verificationCode, captcha)
}
func (f *FrontEndServiceImpl) GenerateCaptcha(ctx context.Context, captcha Captcha) (Captcha, string, error) {
	return f.verificationCodeService.Generate(ctx, captcha)
}
func (f *FrontEndServiceImpl) GetAvailableTrips(ctx context.Context, startingPlace string, endPlace string, departureTime string, token string) ([]TripDetails, error) {
	return f.travelService.QueryInfo(ctx, startingPlace, endPlace, departureTime, token)
}
func (f *FrontEndServiceImpl) GetAvailableTripsOther(ctx context.Context, startingPlace string, endPlace string, departureTime string, token string) ([]TripDetails, error) {
	return f.travel2Service.QueryInfo(ctx, startingPlace, endPlace, departureTime, token)
}