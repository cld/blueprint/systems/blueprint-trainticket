package services

import (
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"context"
	"strconv"
	"errors"
	"fmt"
	"github.com/google/uuid"
)

func CreateInsidePaymentService (insidePaymentDatabase components.NoSQLDatabase, orderService OrderService, orderOtherService OrderOtherService, paymentService PaymentService) *InsidePaymentServiceImpl {
	return &InsidePaymentServiceImpl{
		db: insidePaymentDatabase,
		orderService: orderService,
		orderOtherService: orderOtherService,
		paymentService: paymentService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type InsidePaymentService interface{
	Pay(ctx context.Context, tripId, userId, orderId, token string) (string, error)
	CreateAccount(ctx context.Context, money, userId, token string) (string, error)
	AddMoney(ctx context.Context, userId, money, token string) (string, error)
	QueryPayment(ctx context.Context, token string) ([]Payment, error)
	QueryAccount(ctx context.Context, token string) ([]Balances, error)
	DrawBack(ctx context.Context, userId, money, token string) (string, error)
	PayDifference(ctx context.Context, orderId, userId, price, token string) (string, error)
	QueryAddMoney(ctx context.Context, token string) ([]AddMoney, error)
}

type InsidePaymentServiceImpl struct{
	db components.NoSQLDatabase
	orderService OrderService
	orderOtherService OrderOtherService
	paymentService PaymentService
	roles []string
}

func(ipsi *InsidePaymentServiceImpl) Pay(ctx context.Context, tripId, userId, orderId, token string) (string, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return "", err
	}

	var order Order
	if tripId[0:1] == "G" || tripId[0:1] == "D"{
		order, err = ipsi.orderService.GetOrderById(ctx, orderId, token)
	}else{
		order, err = ipsi.orderOtherService.GetOrderById(ctx, orderId, token)
	}

	newPayment := Payment{
		Id: uuid.New().String(),
		OrderId: orderId,
		UserId: userId,
		Price: fmt.Sprintf("%f",order.Price),
	}

	query := fmt.Sprintf(`{"UserId": %s }`, userId)
	collection := ipsi.db.GetDatabase("ts").GetCollection("payment")
	res, err := collection.FindMany(query) 
	if err != nil {
		return "", err
	}
	var payments []Payment
	res.All(&payments)

	totalExpand := order.Price
	for _, p := range payments{
		price, _ := strconv.ParseFloat(p.Price, 32)
		totalExpand += float32(price)
	}

	amCollection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	res, err = amCollection.FindMany(query)
	if err != nil{
		return "", err
	}
	var addMoney []AddMoney
	res.Decode(&addMoney)

	totalMoney := float32(0.0)
	for _, am := range addMoney{
		money, _ := strconv.ParseFloat(am.Money, 32)
		totalMoney += float32(money)
	}

	if totalExpand > totalMoney {
		_, err = ipsi.paymentService.Pay(ctx, orderId, fmt.Sprintf("%f", order.Price), userId, token)
		if err != nil {
			return "", err
		}
		newPayment.Type = OutsidePayment.String()
	}else{
		newPayment.Type = NormalPayment.String()
	}

	err = collection.InsertOne(newPayment)
	if err != nil {
		return "", err
	}

	if tripId[0:1] == "G" || tripId[0:1] == "D"{
		_, err = ipsi.orderService.ModifyOrder(ctx, orderId, uint16(Paid) ,token)
	}else{
		_, err = ipsi.orderOtherService.ModifyOrder(ctx, orderId, uint16(Paid), token)
	}

	if err != nil{
		return "", err
	}

	return "Payment successful", nil
}

func(ipsi *InsidePaymentServiceImpl) CreateAccount(ctx context.Context, money, userId, token string) (string, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return "", err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	query := fmt.Sprintf(`{"UserId": %s }`, userId)

	res, err := collection.FindOne(query)

	if err == nil{
		var am AddMoney
		err = res.Decode(&am)
		if err == nil {
			return "", errors.New("Account already exists for this user.")
		}
	}

	err = collection.InsertOne(AddMoney{
		Id: uuid.New().String(),
		Money: money,
		UserId: userId,
		Type: AddMoneyType.String(),
	})
	if err != nil{
		return "", err
	}

	return "Account created successfully.", nil

}

func(ipsi *InsidePaymentServiceImpl) AddMoney(ctx context.Context, userId, money, token string) (string, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return "", err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	
	query := fmt.Sprintf(`{"UserId": %s }`, userId)

	res, err := collection.FindOne(query)
	if err != nil{
		return "", err
	}

	var account AddMoney
	res.Decode(&account)

	uQuery := fmt.Sprintf(`{"Id": %s }`, account.Id)
	update := fmt.Sprintf(`{"$set": {Money: %s, Type: %s}}`, money, AddMoneyType.String())
	err = collection.UpdateOne(uQuery, update)
	if err != nil {
		return "", err
	}

	return "Money added successfully", nil
}

func(ipsi *InsidePaymentServiceImpl) QueryPayment(ctx context.Context, token string) ([]Payment, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return nil, err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("payment")
	
	res, err := collection.FindMany("")
	if err != nil{
		return nil, err
	}

	var payments []Payment
	err = res.All(&payments)
	if err != nil {
		return nil, err
	}
	
	return payments, nil
}

func(ipsi *InsidePaymentServiceImpl) QueryAccount(ctx context.Context, token string) ([]Balances, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return nil, err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	
	res, err := collection.FindMany("")
	if err != nil{
		return nil, err
	}

	var accounts []AddMoney
	err = res.All(&accounts)
	if err != nil {
		return nil, err
	}
	
	moneyMap := make(map[string]float32)

	for _, acc := range accounts{
		toAdd, _ := strconv.ParseFloat(acc.Money, 32)

		if _, ok := moneyMap[acc.UserId]; ok{
			moneyMap[acc.UserId] += float32(toAdd)
		}else{
			moneyMap[acc.UserId] = float32(toAdd)
		}
	}

	paymentsCollection := ipsi.db.GetDatabase("ts").GetCollection("payment")

	var resultBalances []Balances


	var totalExpand float32
	for userId, _ := range moneyMap{
		query := fmt.Sprintf(`{"UserId": %s }`, userId)
		userRes, err := paymentsCollection.FindOne(query)
		if err != nil{
			fmt.Println(err)
			continue
		}

		var paymentList []Payment
		err = userRes.All(&paymentList)
		if err != nil {
			continue
		}
		totalExpand = 0.0
		for _, p := range paymentList{
			price, _ := strconv.ParseFloat(p.Price, 32)
			totalExpand += float32(price)
		}

		resultBalances = append(resultBalances, Balances{
			UserId: userId,
			Balance: totalExpand,
		})
		
	}

	return resultBalances, nil

}

func(ipsi *InsidePaymentServiceImpl) DrawBack(ctx context.Context, userId, money, token string) (string, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return "", err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	query := fmt.Sprintf(`{"UserId": %s }`, userId)

	_, err = collection.FindOne(query)
	if err != nil{
		return "", err
	}

	err = collection.InsertOne(AddMoney{
		Id: uuid.New().String(),
		UserId: userId,
		Money: money,
		Type: DrawBackMoney.String(),
	})
	if err != nil{
		return "", err
	}

	return "Drawback successful", nil
}

func(ipsi *InsidePaymentServiceImpl) PayDifference(ctx context.Context, orderId, userId, price, token string) (string, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return "", err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("payment")
	query := fmt.Sprintf(`{"UserId": %s }`, userId)

	res, err := collection.FindMany(query)
	if err != nil{
		return "", err
	}
	var payments []Payment
	err = res.All(&payments)
	if err != nil {
		return "", err
	}

	totalExpand,_ := strconv.ParseFloat(price, 32)

	for _ , p := range payments{
		preyes, _ := strconv.ParseFloat(p.Price, 32)
		totalExpand += preyes
	}

	amCollection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")
	res, err = amCollection.FindMany(query)
	if err != nil {
		return "", err
	}
	var accounts []AddMoney
	err = res.All(&accounts)
	if err != nil {
		return "", err
	}

	totalMoney := float32(0.0)
	for _, a := range accounts{
		money, _ := strconv.ParseFloat(a.Money, 32)
		totalMoney += float32(money)
	}

	if float32(totalExpand) > totalMoney{
		ipsi.paymentService.Pay(ctx, orderId, userId, price, token)
	}

	newPayment := Payment{
		Id: uuid.New().String(),
		UserId: userId,
		OrderId: orderId,
		Price: price,
		Type: ExternalAndDifferencePayment.String(),
	}
	
	err = collection.InsertOne(newPayment)
	if err != nil {
		return "", err
	}

	return "Difference payment successful.", nil
}

func(ipsi *InsidePaymentServiceImpl) QueryAddMoney(ctx context.Context, token string) ([]AddMoney, error){
	err := Authenticate(token, ipsi.roles...)
	if err != nil {
		return nil, err
	}

	collection := ipsi.db.GetDatabase("ts").GetCollection("addMoney")

	res, err := collection.FindMany("")
	if err != nil{
		return nil, err
	}
	var accounts []AddMoney

	err = res.All(&accounts)
	if err != nil {
		return nil, err
	}
	
	return accounts, nil
}
