package services

import (
	"context"
	"sync"
	"math/rand"
	"time"
	"strconv"
	"fmt"
)

func CreateSeatService (travelService TravelService, travel2Service Travel2Service, orderService OrderService, orderOtherService OrderOtherService, configService ConfigService) *SeatServiceImpl {
	
	return &SeatServiceImpl{
		travelService: travelService,
		travel2Service: travel2Service,
		orderService: orderService,
		orderOtherService: orderOtherService,
		configService: configService,
		roles: []string{"ROLE_ADMIN"},
	}
}

type SeatService interface{
	Create(ctx context.Context, seatType uint16, travelDate, trainNumber, startStation, destStation, token string) (Ticket, error)
	GetLeftTicketOfInterval(ctx context.Context, seat Seat, token string) (uint16, error)
}

type SeatServiceImpl struct{
	travelService TravelService
	travel2Service Travel2Service
	orderService OrderService
	orderOtherService OrderOtherService
	configService ConfigService
	roles []string
}

func(ssi *SeatServiceImpl) Create(ctx context.Context, seatType uint16, travelDate, trainNumber, startStation, destStation, token string) (Ticket, error){
	err := Authenticate(token, ssi.roles[0])

	if err != nil {
		return Ticket{}, err
	}


	var wg sync.WaitGroup
	wg.Add(3)

	var err1, err2, err3 error
	var route Route
	var leftTicketInfo []Ticket
	var trainType Train

	if trainNumber[:1] == "G" || trainNumber[:1] == "D"{
		go func() { 
			defer wg.Done()
			route, err1 = ssi.travelService.GetRouteByTripId(ctx, trainNumber, token)
		}()
		
		go func() { 
			defer wg.Done()
			leftTicketInfo, err2 = ssi.orderService.GetTicketListByDateAndTripId(ctx, travelDate, trainNumber, token)
		}()

		go func() { 
			defer wg.Done()
			trainType, err3 = ssi.travelService.GetTrainTypeByTripId(ctx, trainNumber, token)
		}()
		wg.Wait()
	}else{

		go func() { 
			defer wg.Done()
			route, err1 = ssi.travel2Service.GetRouteByTripId(ctx, trainNumber, token)
		}()
		
		go func() { 
			defer wg.Done()
			leftTicketInfo, err2 = ssi.orderOtherService.GetTicketListByDateAndTripId(ctx, travelDate, trainNumber, token)
		}()

		go func() { 
			defer wg.Done()
			trainType, err3 = ssi.travel2Service.GetTrainTypeByTripId(ctx, trainNumber, token)
		}()
		wg.Wait()
	}


	if err1 != nil {
		return Ticket{}, err1
	}
	if err2 != nil {
		return Ticket{}, err2
	}
	if err3 != nil {
		return Ticket{}, err3
	}

	var seatTotalNum uint16

	if seatType == uint16(FirstClass){
		seatTotalNum = trainType.ComfortClass
	}else{
		seatTotalNum = trainType.EconomyClass
	}

	finalTicket := Ticket{
		StartStation: startStation,
		DestStation: destStation,
	}

	stationList := route.Stations

	var startStationIndex, destStationIndex int
	for idx, x := range stationList{
		if x == startStation{
			startStationIndex = idx
		}
	}
	for _, ticket := range leftTicketInfo{

		destStationIndex = 0
		for idx, x := range stationList{
			if x == ticket.DestStation{
				destStationIndex = idx
				break
			}
		}

		if destStationIndex < startStationIndex{
			finalTicket.SeatNo = ticket.SeatNo
			return finalTicket, nil
		}
	}

	
	seed := rand.NewSource(time.Now().UnixNano())
    randomizer := rand.New(seed)

	genSeatNo := randomizer.Intn(int(seatTotalNum - 1)) + 1

	for{
		if finalTicket.SeatNo == fmt.Sprintf("%d",genSeatNo){
			genSeatNo = randomizer.Intn(int(seatTotalNum - 1)) + 1
			continue
		}
		break
	}

	finalTicket.SeatNo = fmt.Sprintf("%d", genSeatNo)

	return finalTicket, nil
}

func(ssi *SeatServiceImpl) GetLeftTicketOfInterval(ctx context.Context, seat Seat, token string) (uint16, error){
	err := Authenticate(token, ssi.roles[0])

	if err != nil {
		return 0, err
	}

	var wg sync.WaitGroup
	wg.Add(3)

	var err1, err2, err3 error
	var route Route
	var leftTicketInfo []Ticket
	var trainType Train

	if seat.TrainNumber[:1] == "G" || seat.TrainNumber[:1] == "D"{
		go func() { 
			defer wg.Done()
			route, err1 = ssi.travelService.GetRouteByTripId(ctx, seat.TrainNumber, token)
		}()
		
		go func() { 
			defer wg.Done()
			leftTicketInfo, err2 = ssi.orderService.GetTicketListByDateAndTripId(ctx, seat.TravelDate, seat.TrainNumber, token)
		}()

		go func() { 
			defer wg.Done()
			trainType, err3 = ssi.travelService.GetTrainTypeByTripId(ctx, seat.TrainNumber, token)
		}()
		wg.Wait()
	}else{

		go func() { 
			defer wg.Done()
			route, err1 = ssi.travel2Service.GetRouteByTripId(ctx, seat.TrainNumber, token)
		}()
		
		go func() { 
			defer wg.Done()
			leftTicketInfo, err2 = ssi.orderOtherService.GetTicketListByDateAndTripId(ctx, seat.TravelDate, seat.TrainNumber, token)
		}()

		go func() { 
			defer wg.Done()
			trainType, err3 = ssi.travel2Service.GetTrainTypeByTripId(ctx, seat.TrainNumber, token)
		}()
		wg.Wait()
	}


	if err1 != nil {
		return 0, err1
	}
	if err2 != nil {
		return 0, err2
	}
	if err3 != nil {
		return 0, err3
	}

	var seatTotalNum uint16

	if seat.SeatType == uint16(FirstClass){
		seatTotalNum = trainType.ComfortClass
	}else{
		seatTotalNum = trainType.EconomyClass
	}

	stationList := route.Stations

	soldTicketCount := len(leftTicketInfo)

	var numTicketsLeft uint16

	var startStationIndex, destStationIndex int
	for idx, x := range stationList{
		if x == seat.StartStation{
			startStationIndex = idx
		}
	}
	for _, ticket := range leftTicketInfo{

		destStationIndex = 0
		for idx, x := range stationList{
			if x == ticket.DestStation{
				destStationIndex = idx
				break
			}
		}

		if destStationIndex < startStationIndex{
			numTicketsLeft += 1
		}
	}

	config, err := ssi.configService.Retrieve(ctx, "DirectTicketAllocationProportion")

	if err != nil {
		return 0, nil
	}

	direstPart, _ := strconv.ParseFloat(config.Value, 32)

	if route.Stations[0] != seat.StartStation || route.Stations[len(route.Stations)-1] != seat.DestStation{
		direstPart = 1.0 - direstPart
	}

	unusedNum := uint16(float64(seatTotalNum) * direstPart - float64(soldTicketCount))
	
	numTicketsLeft += unusedNum

	return numTicketsLeft, nil
}
