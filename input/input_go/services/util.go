package services

import (
	jwt "github.com/dgrijalva/jwt-go"
	"errors"
	"math/rand"
    "time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
  rand.NewSource(time.Now().UnixNano()))

var secret = "secret"

func StringWithCharset(length int, charset string) string {
  b := make([]byte, length)
  for i := range b {
    b[i] = charset[seededRand.Intn(len(charset))]
  }
  return string(b)
}

func GenerateRandomString(length int) string {
  return StringWithCharset(length, charset)
}

func Authenticate(token string,  roles ...string) error{

	keyFunc := func(token *jwt.Token) (interface{}, error) {
        _, ok := token.Method.(*jwt.SigningMethodHMAC)
        if !ok {
            return nil, errors.New("Invalid Token")
        }
        return []byte(secret), nil
    }

    jwtToken, err := jwt.ParseWithClaims(token, &TokenData{}, keyFunc)

	if err != nil{
		return err
	}

	payload, ok := jwtToken.Claims.(*TokenData)
    if !ok {
        return errors.New("Could not parse claims!")
    }

	// // * get intersection of given roles and claims' roles
	////  m := make(map[int]bool)

	//// for _, item := range roles {
	//// 	m[item] = true
	//// }

	//// for _, item := range payload.Roles {
	//// 	if _, ok := m[item]; ok {
	//// 		return nil
	//// 	}
	//// }

	if len(roles) == 0{
		return nil
	}

	for _, item := range roles {
		if item == payload.Role{
			return nil
		}
	}

	return errors.New("Could not authenticate due to missing permissions.")
}

func GenerateNewToken(tokenData TokenData) string{

	
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenData)
	tokenStr, _ := token.SignedString([]byte(secret))

	return tokenStr
}