package services

import (
	"context"
	"errors"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"golang.org/x/crypto/bcrypt"
)

func CreateAuthService(authDatabase components.NoSQLDatabase, verificationCodeService VerificationCodeService) *AuthServiceImpl {

	return &AuthServiceImpl{
		db:                      authDatabase,
		verificationCodeService: verificationCodeService,
		roles:                   []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type AuthService interface {
	GetAllUsers(ctx context.Context, token string) ([]User, error)
	DeleteUserById(ctx context.Context, userId, token string) (string, error)
	CreateDefaultUser(ctx context.Context, user User, token string) (string, error)
	UpdateUser(ctx context.Context, user User) (User, error)
	Login(ctx context.Context, username, password, verificationCode string, captcha Captcha) (string, error)
}

type AuthServiceImpl struct {
	db                      components.NoSQLDatabase
	verificationCodeService VerificationCodeService
	roles                   []string
}

func (*AuthServiceImpl) prepareSaltedHash(password string) string {

	hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	return string(hash)
}

func (asi *AuthServiceImpl) GetAllUsers(ctx context.Context, token string) ([]User, error) {
	err := Authenticate(token, asi.roles[0])

	if err != nil {
		return nil, err
	}

	collection := asi.db.GetDatabase("ts-auth-mongo").GetCollection("user")

	result, err := collection.FindMany("")

	if err != nil {
		return nil, err
	}

	var users []User
	err = result.All(&users)

	return users, err
}

func (asi *AuthServiceImpl) DeleteUserById(ctx context.Context, userId, token string) (string, error) {

	err := Authenticate(token, asi.roles[0])

	if err != nil {
		return "", err
	}

	collection := asi.db.GetDatabase("ts-auth-mongo").GetCollection("user")

	query := fmt.Sprintf(`{"Id": %s }`, userId)

	err = collection.DeleteOne(query)

	if err != nil {
		return "", err
	}

	return "User removed successfully", nil
}

func (asi *AuthServiceImpl) CreateDefaultUser(ctx context.Context, user User, token string) (string, error) {

	err := Authenticate(token)

	if err != nil {
		return "", err
	}

	if user.Username == "" || len(user.Password) < 6 {
		return "", errors.New("Invalid username or password")
	}

	collection := asi.db.GetDatabase("ts-auth-mongo").GetCollection("user")

	user.Password = asi.prepareSaltedHash(user.Password)

	user.Role = asi.roles[1]
	err = collection.InsertOne(user)
	if err != nil {
		return "", err
	}

	return "Default user created successfully", nil
}

func (asi *AuthServiceImpl) UpdateUser(ctx context.Context, username string, password string, token string) (User, error) {

	err := Authenticate(token)

	if err != nil {
		return User{}, err
	}

	collection := asi.db.GetDatabase("ts-auth-mongo").GetCollection("user")

	query := fmt.Sprintf(`{"Username": %s }`, username)
	result, err := collection.FindOne(query)

	if err != nil {
		return User{}, err
	}

	var user User
	result.Decode(&user)

	query = fmt.Sprintf(`{"UserId": %s }`, user.UserId)

	err = collection.DeleteOne(query)
	if err != nil {
		return User{}, err
	}

	newUser := User{
		Username: username,
		Password: asi.prepareSaltedHash(password),
		Role:     asi.roles[1],
		UserId:   user.UserId,
	}

	err = collection.InsertOne(newUser)
	if err != nil {
		return User{}, err
	}

	return newUser, nil
}

//! TODO
func (asi *AuthServiceImpl) Login(ctx context.Context, username, password, verificationCode string, captcha Captcha) (string, error) {

	if verificationCode != "" {
		_, validCode, err := asi.verificationCodeService.Verify(ctx, verificationCode, captcha)
		if err != nil {
			return "", err
		}

		if !validCode {
			return "", errors.New("Verification failed")
		}
	}

	collection := asi.db.GetDatabase("ts-auth-mongo").GetCollection("user")

	query := fmt.Sprintf(`{"Username": %s }`, username)
	result, err := collection.FindOne(query)

	if err != nil {
		return "", err
	}

	var user User
	result.Decode(&user)

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return "", errors.New("Incorrect password!")
	}

	tokenData := TokenData{
		UserId:    user.UserId,
		Username:  user.Username,
		Timestamp: uint64(time.Now().UnixMilli()),
		Ttl:       3600,
		Role:      asi.roles[0], // only one role needed here
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(60 * time.Minute).Unix(),
		},
	}

	/*tik*/
	tok := GenerateNewToken(tokenData)

	return tok, nil
}
