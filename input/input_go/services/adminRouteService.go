package services

import (
	"context"
)

func CreateAdminRouteService (routeService RouteService) *AdminRouteServiceImpl {
	return &AdminRouteServiceImpl{
		routeService: routeService,
		roles: []string{"ROLE_ADMIN"},
	}
}

type AdminRouteService interface{
	GetAllRoutes(ctx context.Context, token string) ([]Route, error)
	AddRoute(ctx context.Context, route RouteRequest, token string) (Route, error)
	DeleteRoute(ctx context.Context, routeId, token string) (string, error)	
}

type AdminRouteServiceImpl struct{
	routeService RouteService
	roles []string
}


func (arsi* AdminRouteServiceImpl) GetAllRoutes(ctx context.Context, token string) ([]Route, error){

	err := Authenticate(token, arsi.roles...)
	if err != nil {
		return nil, err
	}

	return arsi.routeService.QueryAll(ctx, token)
}

func (arsi* AdminRouteServiceImpl) AddRoute(ctx context.Context, routereq RouteRequest, token string) (Route, error){

	err := Authenticate(token, arsi.roles...)
	if err != nil {
		return Route{}, err
	}
	route := Route{Id: routereq.Id, StartStationId: routereq.StartStation, TerminalStationId: routereq.EndStation, Stations: routereq.Stations, Distances: routereq.Distances}
	return arsi.routeService.CreateAndModifyRoute(ctx, route, token)
}

func (arsi* AdminRouteServiceImpl) DeleteRoute(ctx context.Context, routeId, token string) (string, error){
	err := Authenticate(token, arsi.roles...)
	if err != nil {
		return "", err
	}
	
	return arsi.routeService.DeleteRoute(ctx, routeId, token)
}
