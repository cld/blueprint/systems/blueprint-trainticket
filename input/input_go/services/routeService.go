package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

func CreateRouteService (routeDatabase components.NoSQLDatabase) *RouteServiceImpl {
	
	return &RouteServiceImpl{
		db: routeDatabase,
		roles: []string{"ROLE_ADMIN"},
	}
}

type RouteService interface{
	CreateAndModifyRoute(ctx context.Context, route Route, token string)(Route, error)
	DeleteRoute(ctx context.Context, routeId, token string) (string, error)
	QueryById(ctx context.Context, routeId, token string) (Route, error)
	QueryAll(ctx context.Context, token string) ([]Route, error)
	QueryByStartAndTerminal(ctx context.Context, startId, terminalId, token string) ([]Route, error)
}

type RouteServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}

func(rsi *RouteServiceImpl) CreateAndModifyRoute(ctx context.Context, providedRoute RouteRequest, token string)(Route, error){
	err := Authenticate(token, rsi.roles[0])

	if err != nil {
		return Route{}, err
	}


	stations := providedRoute.Stations
	distances := providedRoute.Distances

	if len(stations) != len(distances) { 
		return Route{}, errors.New("Number of distances doesn't match that of stations.")
	}

	collection := rsi.db.GetDatabase("ts").GetCollection("routes")
	
	//* Insert
	if providedRoute.Id == "" || len(providedRoute.Id) < 10{

		route := Route{
			Id: uuid.New().String(),
			StartStationId: providedRoute.StartStation,
			TerminalStationId: providedRoute.EndStation,
			Stations: stations,
			Distances: distances,
		}


		err = collection.InsertOne(route)
		if err != nil{
			return Route{}, err
		}
	
		return route, nil
	}

	//* Update

	query := fmt.Sprintf(`{"Id": %s }`, providedRoute.Id)

	res, err := collection.FindOne(query) 
	if err != nil {
		return Route{}, err
	}

	var existingRoute Route
	res.Decode(&existingRoute)
	
	if existingRoute.Id == ""{
		return Route{}, errors.New("Could not find route!")
	}

	route := Route{
		Id: providedRoute.Id,
		StartStationId: providedRoute.StartStation,
		TerminalStationId: providedRoute.EndStation,
		Stations: stations,
		Distances: distances,
	}


	err = collection.ReplaceOne(query, route)
	if err != nil{
		return Route{}, err
	}

	return route, nil
}

func(rsi *RouteServiceImpl) DeleteRoute(ctx context.Context, routeId, token string) (string, error){
	err := Authenticate(token)

	if err != nil {
		return "", err
	}

	collection := rsi.db.GetDatabase("ts").GetCollection("routes")
	query := fmt.Sprintf(`{"Id": %s }`, routeId)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Route removed successfully", nil
}

func(rsi *RouteServiceImpl) QueryById(ctx context.Context, routeId, token string) (Route, error){
	err := Authenticate(token)
	if err != nil {
		return Route{}, err
	}

	collection := rsi.db.GetDatabase("ts").GetCollection("routes")

	query := fmt.Sprintf(`{"Id": %s}`, routeId)

	result, err := collection.FindOne(query)
	if err != nil{
		return Route{}, err
	}

	var route Route
	err = result.Decode(&route)
	if err != nil{
		return Route{}, err
	}

	return route, nil
}

func(rsi *RouteServiceImpl) QueryAll(ctx context.Context, token string) ([]Route, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := rsi.db.GetDatabase("ts").GetCollection("routes")

	result, err := collection.FindMany("") //TODO verify this query-string works!

	if err != nil{
		return nil, err
	}

	var routes []Route
	err = result.All(&routes)
	if err != nil{
		return nil, err
	}

	return routes, nil
}

func(rsi *RouteServiceImpl) QueryByStartAndTerminal(ctx context.Context, startId, terminalId, token string) ([]Route, error){

	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := rsi.db.GetDatabase("ts").GetCollection("routes")

	query := fmt.Sprintf(`{"StartStationId": %s, "TerminalStationId": %s}`, startId, terminalId)

	result, err := collection.FindMany(query) //TODO verify this query-string works!

	if err != nil{
		return nil, err
	}

	var routes []Route
	err = result.All(&routes)
	if err != nil{
		return nil, err
	}

	var filteredRoutes []Route

	for _, route := range routes {

		foundStart := false
		for _, station := range route.Stations{

			if startId == station {
				foundStart = true
				continue //to find the end
			}

			if terminalId == station{

				if foundStart == false{
					break // route discarded cause startIdx > endIdx or startIdx not in stations
				}
				
				//cause start should have already been found at this point
				filteredRoutes = append(filteredRoutes, route)
				break 
			}
		}
	}

	return filteredRoutes, nil
}
