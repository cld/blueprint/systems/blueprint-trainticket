package services

import (
	"context"
	"time"
	"fmt"
	"errors"
	"sync"
)

func CreateRebookService (orderService OrderService, orderOtherService OrderOtherService, insidePaymentService InsidePaymentService, seatService SeatService, stationService StationService, travelService TravelService, travel2Service Travel2Service) *RebookServiceImpl {
	
	return &RebookServiceImpl{
		orderService: orderService,
		orderOtherService: orderOtherService,
		insidePaymentService: insidePaymentService,
		seatService: seatService,
		stationService: stationService,
		travelService: travelService,
		travel2Service: travel2Service,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type RebookService interface{
	PayDifference(ctx context.Context, info RebookInfo, token string) (Order, error)
	Rebook(ctx context.Context, info RebookInfo, token string) (Order, error)
}

type RebookServiceImpl struct{
	orderService OrderService
	orderOtherService OrderOtherService
	insidePaymentService InsidePaymentService
	seatService SeatService
	stationService StationService
	travelService TravelService
	travel2Service Travel2Service
	roles []string
}

func (rsi *RebookServiceImpl) IsTripGD(tripId string) bool{
	return tripId[:1] == "G" || tripId[:1] == "D"
}

func(rsi *RebookServiceImpl) PayDifference(ctx context.Context, info RebookInfo, token string) (Order, error){
	err := Authenticate(token, rsi.roles...)
	if err != nil {
		return Order{}, err
	}

	var order Order
	if rsi.IsTripGD(info.OldTripId){
		order, err = rsi.orderService.GetOrderById(ctx, info.OrderId, token)
	}else{
		order, err = rsi.orderOtherService.GetOrderById(ctx, info.OrderId, token)
	}

	if err != nil {
		return Order{}, err
	}

	var wg sync.WaitGroup
	wg.Add(2)
	var err1, err2 error

	var from, to string
	go func() { 
		defer wg.Done()
		from, err1 = rsi.stationService.QueryById(ctx, order.From, token)
	}()
	go func() { 
		defer wg.Done()
		to, err2 = rsi.stationService.QueryById(ctx, order.To, token)
	}()
	wg.Wait()
	if err1 != nil{
		return Order{}, err1
	}
	if err2 != nil {
		return Order{}, err2
	}

	var	tripResponse TripDetails

	if rsi.IsTripGD(info.TripId){
		_, tripResponse, err = rsi.travelService.GetTripAllDetailInfo(ctx, info.TripId, from, to, info.Date, token)
	}else{
		_, tripResponse, err = rsi.travel2Service.GetTripAllDetailInfo(ctx, info.TripId, from, to, info.Date, token)
	}

	var ticketPrice float32
	if info.SeatType == uint16(FirstClass){
		ticketPrice = tripResponse.PriceForComfortClass
	}else if info.SeatType == uint16(SecondClass){
		ticketPrice = tripResponse.PriceForEconomyClass
	}

	_, err = rsi.insidePaymentService.PayDifference(ctx, info.OrderId, info.LoginId, fmt.Sprintf("%f", ticketPrice - order.Price), token)
	if err != nil{
		return Order{}, err
	}


	//* Identical to rebook func part
	_, err = rsi.seatService.Create(ctx, info.SeatType, info.Date, order.TrainNumber, order.From, order.To, token)

	if err != nil {
		return Order{}, err
	}

	//! Some details about the `date` parameter are changed here from the original system
	//! The `Order` object in the original system kept two separate attributes for date and time respectively.
	//! We need to verify any use cases that might be affected by this. For now, we just create orders
	//! with a "From" datetime equal to <date><trip.startingTime[time]>. We get the time portion only from startingTime.

	oldTripId := order.TrainNumber

	tmpDate, _ := time.Parse(time.ANSIC, info.Date)
	y, m, d := tmpDate.Date()
	startTime, _ := time.Parse(time.ANSIC, tripResponse.StartingTime)
	hr, min, sec := startTime.Clock()
	trDate := fmt.Sprintf("%s %s %s %s:%s:%s %s",tmpDate.Weekday().String()[:3], m.String()[:3], d, hr, min, sec, y)
	bgDate := time.Now()

	order.BoughtDate = bgDate.String()
	order.TravelDate = trDate
	order.TrainNumber = info.TripId
	order.SeatClass = info.SeatType
	order.Status = uint16(Change)
	order.Price = ticketPrice

	if (rsi.IsTripGD(oldTripId) && rsi.IsTripGD(info.TripId)) || (!rsi.IsTripGD(oldTripId) && !rsi.IsTripGD(info.TripId)) {
		
		if rsi.IsTripGD(info.TripId){
			order, err = rsi.orderService.SaveOrderInfo(ctx, order, token)
		}else{
			order, err = rsi.orderOtherService.SaveOrderInfo(ctx, order, token)
		}
		if err != nil {
			return Order{}, err
		}
	}else{

		if rsi.IsTripGD(oldTripId){
			_, err = rsi.orderService.DeleteOrder(ctx, order.Id, token)
		}else{
			_, err = rsi.orderOtherService.DeleteOrder(ctx, order.Id, token)
		}

		if err != nil {
			return Order{}, err
		}

		if rsi.IsTripGD(info.TripId){
			order, err = rsi.orderService.CreateNewOrder(ctx, order, token)
		}else{
			order, err = rsi.orderOtherService.CreateNewOrder(ctx, order, token)
		}
		if err != nil {
			return Order{}, err
		}
	}

	return order, nil
}


func(rsi *RebookServiceImpl) Rebook(ctx context.Context, info RebookInfo, token string) (Order, error){
	err := Authenticate(token, rsi.roles...)
	if err != nil {
		return Order{}, err
	}

	var order Order
	if rsi.IsTripGD(info.OldTripId){
		order, err = rsi.orderService.GetOrderById(ctx, info.OrderId, token)
	}else{
		order, err = rsi.orderOtherService.GetOrderById(ctx, info.OrderId, token)
	}

	if err != nil {
		return Order{}, err
	}

	if order.Status != uint16(Paid){
		return Order{}, errors.New("Order cannot be rebooked.")
	}

	travelDateTime, _ := time.Parse(time.ANSIC, order.TravelDate)
	diff := time.Now().Sub(travelDateTime)

	if diff.Hours() > 2{
		return Order{}, errors.New("Order can be rebooked only up to two hours after travel time.")
	}

	var wg sync.WaitGroup
	wg.Add(2)
	var err1, err2 error

	var from, to string
	go func() { 
		defer wg.Done()
		from, err1 = rsi.stationService.QueryById(ctx, order.From, token)
	}()
	go func() { 
		defer wg.Done()
		to, err2 = rsi.stationService.QueryById(ctx, order.To, token)
	}()
	wg.Wait()
	if err1 != nil{
		return Order{}, err1
	}
	if err2 != nil {
		return Order{},  err2
	}

	var	tripResponse TripDetails

	if rsi.IsTripGD(info.TripId){
		_, tripResponse, err = rsi.travelService.GetTripAllDetailInfo(ctx, info.TripId, from, to, info.Date, token)
	}else{
		_, tripResponse, err = rsi.travel2Service.GetTripAllDetailInfo(ctx, info.TripId, from, to, info.Date, token)
	}

	var ticketPrice float32
	if info.SeatType == uint16(FirstClass){
		ticketPrice = tripResponse.PriceForComfortClass
	}else if info.SeatType == uint16(SecondClass){
		ticketPrice = tripResponse.PriceForEconomyClass
	}

	oldPrice := order.Price

	if oldPrice > ticketPrice{
		difference := fmt.Sprintf("%f",oldPrice - ticketPrice)
		_, err = rsi.insidePaymentService.DrawBack(ctx, info.LoginId, difference, token)
		if err != nil{
			return Order{}, err
		}

	}else if oldPrice < ticketPrice{
		return Order{}, errors.New(fmt.Sprintf("Please pay difference.%f", ticketPrice - oldPrice))
	}


	//* Identical to rebook func part
	_, err = rsi.seatService.Create(ctx, info.SeatType, info.Date, order.TrainNumber, order.From, order.To, token)
	
	if err != nil {
		return Order{}, err
	}

	//! Some details about the `date` parameter are changed here from the original system
	//! The `Order` object in the original system kept two separate attributes for date and time respectively.
	//! We need to verify any use cases that might be affected by this. For now, we just create orders
	//! with a "From" datetime equal to <date><trip.startingTime[time]>. We get the time portion only from startingTime.

	oldTripId := order.TrainNumber

	tmpDate, _ := time.Parse(time.ANSIC, info.Date)
	y, m, d := tmpDate.Date()
	hr, min, sec := tmpDate.Clock()
	trDate := fmt.Sprintf("%s %s %s %s:%s:%s %s",tmpDate.Weekday().String()[:3], m.String()[:3], d, hr, min, sec, y)
	bgDate := time.Now()
	order.BoughtDate = bgDate.String()
	order.TravelDate = trDate
	order.TrainNumber = info.TripId
	order.SeatClass = info.SeatType
	order.Status = uint16(Change)
	order.Price = ticketPrice

	if (rsi.IsTripGD(oldTripId) && rsi.IsTripGD(info.TripId)) || (!rsi.IsTripGD(oldTripId) && !rsi.IsTripGD(info.TripId)) {
		
		if rsi.IsTripGD(info.TripId){
			order, err = rsi.orderService.SaveOrderInfo(ctx, order, token)
		}else{
			order, err = rsi.orderOtherService.SaveOrderInfo(ctx, order, token)
		}
		if err != nil {
			return Order{}, err
		}
	}else{

		if rsi.IsTripGD(oldTripId){
			_, err = rsi.orderService.DeleteOrder(ctx, order.Id, token)
		}else{
			_, err = rsi.orderOtherService.DeleteOrder(ctx, order.Id, token)
		}

		if err != nil {
			return Order{}, err
		}

		if rsi.IsTripGD(info.TripId){
			order, err = rsi.orderService.CreateNewOrder(ctx, order, token)
		}else{
			order, err = rsi.orderOtherService.CreateNewOrder(ctx, order, token)
		}
		if err != nil {
			return Order{}, err
		}
	}

	return order, nil
}
