package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

func CreateConsignService (consignDatabase components.NoSQLDatabase, consignPriceService ConsignPriceService) *ConsignServiceImpl {
	
	return &ConsignServiceImpl{
		consignPriceService: consignPriceService,
		db: consignDatabase,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type ConsignService interface{
	InsertConsign(ctx context.Context, consign Consign, token string) (Consign, error)
	UpdateConsign(ctx context.Context, consign Consign, token string)(Consign, error)
	FindByAccountId(ctx context.Context, accountId, token string)([]Consign, error)
	FindByOrderId(ctx context.Context, orderId, token string) ([]Consign, error)
	FindByConsignee(ctx context.Context, consignee, token string) ([]Consign, error)
}

type ConsignServiceImpl struct{
	consignPriceService ConsignPriceService
	db components.NoSQLDatabase
	roles []string
}


func(csi *ConsignServiceImpl) InsertConsign(ctx context.Context, consign Consign, token string) (Consign, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return Consign{}, err
	}

	price, err := csi.consignPriceService.GetPriceByWeightAndRegion(ctx, consign.Weight, consign.Within, token)
	if err != nil {
		return Consign{}, err
	}
	
	consign.Price = price
	consign.Id = uuid.New().String()

	collection := csi.db.GetDatabase("ts").GetCollection("consign_record")
	err = collection.InsertOne(consign) 
	if err != nil{
		return Consign{}, err
	}

	return consign, nil
}

func(csi *ConsignServiceImpl) UpdateConsign(ctx context.Context, consign Consign, token string)(Consign, error){
	
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return Consign{}, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("consign_record")

	query := fmt.Sprintf(`{"Id": %s }`, consign.Id)

	res, err := collection.FindOne(query)
	if err != nil {
		return Consign{}, err
	}

	var originalConsign Consign
	res.Decode(originalConsign)

	if originalConsign.Weight != consign.Weight{
		retval, err := csi.consignPriceService.GetPriceByWeightAndRegion(ctx, consign.Weight, consign.Within, token)
		if err != nil {
			return Consign{}, err
		}

		consign.Price = retval
	}

	err = collection.ReplaceOne(query, consign)
	if err != nil {
		return Consign{}, err
	}

	return consign, nil
}

func(csi *ConsignServiceImpl) FindByAccountId(ctx context.Context, accountId, token string)([]Consign, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return nil, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("consign_record")
	query := fmt.Sprintf(`{"AccountId": %s}`, accountId)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var consigns []Consign
	err = result.All(&consigns)
	if err != nil{
		return nil, err
	}

	return consigns, nil
}

func(csi *ConsignServiceImpl) FindByOrderId(ctx context.Context, orderId, token string) ([]Consign, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return nil, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("consign_record")
	query := fmt.Sprintf(`{"OrderId": %s}`, orderId)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var consigns []Consign
	err = result.All(&consigns)
	if err != nil{
		return nil, err
	}

	return consigns, nil
}

func(csi *ConsignServiceImpl) FindByConsignee(ctx context.Context, consignee, token string) ([]Consign, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return nil, err
	}

	collection := csi.db.GetDatabase("ts").GetCollection("consign_record")
	query := fmt.Sprintf(`{"Consignee": %s}`, consignee)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var consigns []Consign
	err = result.All(&consigns)
	if err != nil{
		return nil, err
	}

	return consigns, nil
}
