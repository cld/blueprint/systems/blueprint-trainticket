package services

import (
	"context"
	"errors"
)

func CreateExecuteService (orderService OrderService, orderOtherService OrderOtherService, userService UserService, notificationService NotificationService, insidePaymentService InsidePaymentService) *ExecuteServiceImpl {
	
	return &ExecuteServiceImpl{
		orderService: orderService,
		orderOtherService: orderOtherService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type ExecuteService interface{
	ExecuteTicket(ctx context.Context, orderId, token string) (string, error)
	CollectTicket(ctx context.Context, orderId, token string) (string, error)
}

type ExecuteServiceImpl struct{
	orderService OrderService
	orderOtherService OrderOtherService
	roles []string
}

func(esi* ExecuteServiceImpl) ExecuteTicket(ctx context.Context, orderId, token string) (string, error){
	err := Authenticate(token, esi.roles...)
	if err != nil {
		return "", err
	}

	var order Order
	first := true
	order, err = esi.orderService.GetOrderById(ctx, orderId, token)
	if err == nil {

		order, err = esi.orderOtherService.GetOrderById(ctx, orderId, token)
		if err != nil {
			return "", err
		}
		first = false
	}

	if order.Status != uint16(Paid) && order.Status != uint16(Change){
		return "", errors.New("Order cannot be collected!")
	}

	if first{
		_, err = esi.orderService.ModifyOrder(ctx, orderId, uint16(Collected), token)
	}else{
		_, err = esi.orderOtherService.ModifyOrder(ctx, orderId, uint16(Collected), token)
	}

	if err != nil {
		return "", err
	}

	return "Order collected successfully", nil
}

func(esi* ExecuteServiceImpl) CollectTicket(ctx context.Context, orderId, loginId, token string) (string, error){
	err := Authenticate(token, esi.roles...)
	if err != nil {
		return "", err
	}

	first := true
	_, err = esi.orderService.GetOrderById(ctx, orderId, token)
	if err == nil {

		_, err = esi.orderOtherService.GetOrderById(ctx, orderId, token)
		if err != nil {
			return "", err
		}
		first = false
	}

	if first{
		_, err = esi.orderService.ModifyOrder(ctx, orderId, uint16(Used), token)
	}else{
		_, err = esi.orderOtherService.ModifyOrder(ctx, orderId, uint16(Used), token)
	}

	if err != nil {
		return "", err
	}

	return "Order executed successfully", nil
}

