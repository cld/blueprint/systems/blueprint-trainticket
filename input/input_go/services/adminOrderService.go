package services

import (
	"sync"
	"errors"
	"context"
)

func CreateAdminOrderService (orderService OrderService, orderOtherService OrderOtherService) *AdminOrderServiceImpl {
	return &AdminOrderServiceImpl{
		orderService: orderService,
		orderOtherService: orderOtherService,
		roles: []string{"ROLE_ADMIN"},
	}
}

//! TODO
type AdminOrderService interface{
	GetAllOrders(ctx context.Context, token string) ([]Order, error)
	DeleteOrder(ctx context.Context, orderId, token string) (string, error)
	UpdateOrder(ctx context.Context, order Order, token string) (Order, error)
	AddOrder(ctx context.Context, order Order, token string) (Order, error)
}

type AdminOrderServiceImpl struct{
	orderService OrderService
	orderOtherService OrderOtherService
	roles []string
}


func (aosi* AdminOrderServiceImpl) GetAllOrders(ctx context.Context, token string) ([]Order, error){
	err := Authenticate(token, aosi.roles...)
	if err != nil {
		return nil, err
	}
	
	var err1, err2 error
	var ordersFirstBatch, ordersSecondBatch []Order
	var wg sync.WaitGroup
	wg.Add(2)

	go func() { 
		defer wg.Done()
		ordersFirstBatch, err1 = aosi.orderService.FindAllOrder(ctx, token)
	}()
	
	go func() { 
		defer wg.Done()
		ordersSecondBatch, err2 = aosi.orderOtherService.FindAllOrder(ctx, token)
	}()
	wg.Wait()

	if err1 != nil{
		return nil, err1
	}
	if err2 != nil{
		return nil, err2
	}


	orders := append(ordersFirstBatch, ordersSecondBatch...)

	if len(orders) == 0{
		return nil, errors.New("No orders found")
	}

	return orders, nil
}

func (aosi* AdminOrderServiceImpl) DeleteOrder(ctx context.Context, orderId, trainNumber, token string) (string, error){
	err := Authenticate(token, aosi.roles...)
	if err != nil {
		return "", err
	}

	var msg string
	if trainNumber[0:1] == "D" || trainNumber[0:1] == "G"{
		msg, err = aosi.orderService.DeleteOrder(ctx, orderId, token)

	}else{
		msg, err = aosi.orderOtherService.DeleteOrder(ctx, orderId, token)
	}

	if err != nil {
		return "", err
	}
	
	return msg, nil
}

func (aosi* AdminOrderServiceImpl) UpdateOrder(ctx context.Context, order Order, token string) (Order, error){
	err := Authenticate(token, aosi.roles...)
	if err != nil {
		return Order{}, err
	}

	if order.TrainNumber[0:1] == "D" || order.TrainNumber[0:1] == "G"{
		order, err = aosi.orderService.UpdateOrder(ctx, order, token)
	} else{
		order, err = aosi.orderOtherService.UpdateOrder(ctx, order, token)
	}

	if err != nil {
		return Order{}, err
	}

	return order, nil
}

func (aosi* AdminOrderServiceImpl) AddOrder(ctx context.Context, order Order, token string) (Order, error){
	err := Authenticate(token, aosi.roles...)
	if err != nil {
		return Order{}, err
	}

	if order.TrainNumber[0:1] == "D" || order.TrainNumber[0:1] == "G"{
		order, err = aosi.orderService.AddCreateNewOrder(ctx, order, token)
	} else{
		order, err = aosi.orderOtherService.AddCreateNewOrder(ctx, order, token)
	}

	if err != nil {
		return Order{}, err
	}

	return order, nil
}