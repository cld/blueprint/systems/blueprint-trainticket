package services

import (
	"context"
	"sync"
	"time"
	"errors"
	"fmt"
)

//! *****************************************************************************************************************************************
//! **                                                                                                                                     **
//! **          This service uses a queue to push email notifications, but we found it to be disabled in the original implementation 	   **
//! **          and thus removed it.   																									   **
//! **                                                                                                                                     **
//! *****************************************************************************************************************************************

func CreatePreserveOtherService (securityService SecurityService, contactService ContactService, travel2Service Travel2Service, stationService StationService, foodService FoodService, insuranceService InsuranceService, consignService ConsignService, orderOtherService OrderOtherService, seatService SeatService, userService UserService, ticketInfoService TicketInfoService) *PreserveOtherServiceImpl {
	
	return &PreserveOtherServiceImpl{
		securityService: securityService,
		contactService: contactService,
		travel2Service: travel2Service,
		stationService: stationService,
		foodService: foodService,
		insuranceService: insuranceService,
		consignService: consignService,
		orderOtherService: orderOtherService,
		seatService: seatService,
		userService: userService,
		ticketInfoService: ticketInfoService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type PreserveOtherService interface{
	Preserve(ctx context.Context, info OrderInfo, token string)(string, error)
}

type PreserveOtherServiceImpl struct{
	securityService SecurityService
	contactService ContactService
	travel2Service Travel2Service
	stationService StationService
	foodService FoodService
	insuranceService InsuranceService
	consignService ConsignService
	orderOtherService OrderOtherService
	seatService SeatService
	userService UserService
	ticketInfoService TicketInfoService
	roles []string
}

func(psi *PreserveOtherServiceImpl) Preserve(ctx context.Context, info OrderTicketInfo, token string)(string, error){
	err := Authenticate(token, psi.roles...)
	if err != nil {
		return "", err
	}

	var err1, err2, err3 error
	var contact Contact
	var trip Trip
	var tripData TripDetails

	var wg sync.WaitGroup
	wg.Add(3)

	go func() { 
		defer wg.Done()
		_, err1 = psi.securityService.Check(ctx, info.AccountId, token)
	}()
	
	go func() { 
		defer wg.Done()
		contact, err2 = psi.contactService.GetContactsByContactId(ctx, info.TripId, token)
	}()

	go func() { 
		defer wg.Done()
		trip, tripData, err3 = psi.travel2Service.GetTripAllDetailInfo(ctx, info.TripId, info.From, info.To, info.Date, token)
	}()

	wg.Wait()

	if err1 != nil{
		return "", err1
	}
	if err2 != nil{
		return "", err2
	}
	if err3 != nil{
		return "", err3
	}

//* ****************************************************************************************
	if info.SeatType == uint16(FirstClass) && tripData.ComfortClass == 0{
		return "", errors.New("Not enough seats to perform this booking.")
	}else if tripData.EconomyClass == 0{
		return "", errors.New("Not enough seats to perform this booking.")
	}

	wg.Add(3)

	var fromStationId, toStationId string
	var travelResult TravelResult
	go func() { 
		defer wg.Done()
		fromStationId, err1 = psi.stationService.QueryForStationId(ctx, info.From, token)
	}()
	
	go func() { 
		defer wg.Done()
		toStationId, err2 = psi.stationService.QueryForStationId(ctx, info.To, token)
	}()

	go func() { 
		defer wg.Done()
		travel := Travel{
			Trip: trip,
			StartingPlace: info.From,
			EndPlace: info.To,
			DepartureTime: time.Now().Format(time.ANSIC),
		}
		travelResult, err3 = psi.ticketInfoService.QueryForTravel(ctx, travel, token)
	}()
	wg.Wait()

	if err1 != nil{
		return "", err1
	}
	if err2 != nil{
		return "", err2
	}
	if err3 != nil{
		return "", err3
	}

//* ****************************************************************************************

	var ticket Ticket
	var orderSeatClass uint16
	var orderPrice float32
	if info.SeatType == uint16(FirstClass){
		orderSeatClass = 1
		orderPrice = travelResult.Prices["ComfortClass"]
		ticket, err = psi.seatService.Create(ctx, 1, info.Date, info.TripId, fromStationId, toStationId, token)
	}else{
		orderSeatClass = 2
		orderPrice = travelResult.Prices["EconomyClass"]
		ticket, err = psi.seatService.Create(ctx, 2, info.Date, info.TripId, fromStationId, toStationId, token)
	}
	if err != nil{
		return "", err
	}

	orderSeatNum := ticket.SeatNo

	tmpDate, _ := time.Parse(time.ANSIC, info.Date)
	y, m, d := tmpDate.Date()
	startTime, _ := time.Parse(time.ANSIC, tripData.StartingTime)
	hr, min, sec := startTime.Clock()

	order := Order{
		BoughtDate: time.Now().Format(time.ANSIC),
		TravelDate: fmt.Sprintf("%s %s %s %s:%s:%s %s",tmpDate.Weekday().String()[:3], m.String()[:3], d, hr, min, sec, y),
		AccountId: info.AccountId,
		ContactsName: info.ContactsId,
		DocumentType: contact.DocumentType,
		ContactsDocumentNumber: contact.DocumentNumber,
		TrainNumber: info.TripId,
		SeatClass: orderSeatClass,
		SeatNumber: orderSeatNum,
		From: fromStationId,
		To: toStationId,
		Status: 0,
		Price: orderPrice,
	}

	order, err = psi.orderOtherService.CreateNewOrder(ctx, order, token)
	if err != nil{
		return "", err
	}

	if info.Insurance != 0{
		_, err = psi.insuranceService.CreateNewInsurance(ctx, info.Insurance, order.Id, token)
	}
	if err != nil{
		return "", err
	}

	if info.FoodType != 0{
		foodOrder := FoodOrder{
			OrderId: order.Id, 
			FoodType: info.FoodType, 
			StationName: info.StationName, 
			StoreName: info.StoreName, 
			FoodName: info.FoodName, 
			Price: info.FoodPrice, 
		}

		_, err = psi.foodService.CreateFoodOrder(ctx, foodOrder, token)
	}
	if err != nil{
		return "", err
	}

	if info.ConsigneeName != ""{
		consign := Consign{
			OrderId: order.Id,
			AccountId: order.AccountId,
			HandleDate: info.HandleDate,
			TargetDate: order.TravelDate,
			From: order.From,
			To: order.To,
			Consignee: info.ConsigneeName,
			Phone: info.ConsigneePhone,
			Weight: info.ConsigneWeight,
			Within: info.IsWithin,
		}

		_, err = psi.consignService.InsertConsign(ctx, consign, token)
	}
	if err != nil{
		return "", err
	}

	//* Fetching the user would be needed if we use the email-queue logic
	//* We still make the function call for sameness' sake
	_, err = psi.userService.GetUserById(ctx, info.AccountId, token)
	if err != nil{
		return "", err
	}

	return "Booking successful", nil
}
