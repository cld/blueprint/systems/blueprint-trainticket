//! **********************************************************************************************************************************************
//! **                                                                                                                                          **
//! **          This service only has a queue-consumer functionality, but we found it to be disabled in the original implementation.            **
//! **          However, we wanted to have this implemented as an example of a functioning queue-subsystem between two services.                **
//! **                                                                                                                                          **
//! **********************************************************************************************************************************************
package services

import (
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"encoding/json"
	"log"
)

type DeliveryService interface {
	Entry()
}

type DeliveryServiceImpl struct {
	deliveryQueue components.Queue
	deliveryDB components.NoSQLDatabase
}

func NewDeliveryServiceImpl(deliveryQueue components.Queue, deliveryDB components.NoSQLDatabase) *DeliveryServiceImpl {
	return &DeliveryServiceImpl{deliveryQueue: deliveryQueue, deliveryDB: deliveryDB}
}

func (d *DeliveryServiceImpl) ProcessDelivery(payload []byte) {
	var delivery Delivery
	err := json.Unmarshal(payload, &delivery)
	if err != nil {
		log.Println(err)
		return
	}
	collection := d.deliveryDB.GetDatabase("ts").GetCollection("delivery")
	err = collection.InsertOne(delivery)
	if err != nil {
		log.Println(err)
	}
}

func (d *DeliveryServiceImpl) Entry() {
	d.deliveryQueue.Recv(d.ProcessDelivery)
}