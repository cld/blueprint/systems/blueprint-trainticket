package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
	"time"
	"errors"
)

func CreateTravelService (travelDatabase components.NoSQLDatabase, routeService RouteService, trainService TrainService, ticketInfoService TicketInfoService, orderService OrderService, seatService SeatService) *TravelServiceImpl {

	return &TravelServiceImpl{
		db: travelDatabase,
		routeService: routeService,
		trainService: trainService,
		ticketInfoService: ticketInfoService,
		orderService: orderService,
		seatService: seatService,
		roles: []string{"ROLE_ADMIN"},
	}
}
type TravelService interface {
	GetTrainTypeByTripId(ctx context.Context, tripId, token string) (Train, error)
	GetRouteByTripId(ctx context.Context, tripId, token string)(Route, error)
	GetTripsByRouteId(ctx context.Context, routeIds []string, token string) ([]Trip, error)
	UpdateTrip(ctx context.Context, trip Trip, token string) (Trip, error)
	Retrieve(ctx context.Context, tripId string, token string) (Trip, error)
	CreateTrip(ctx context.Context, trip Trip, token string) (Trip, error)
	DeleteTrip(ctx context.Context, tripId string, token string) (string, error)
	QueryInfo(ctx context.Context, startingPlace, endPlace, departureTime, token string) ([]TripDetails, error)
	GetTripAllDetailInfo(ctx context.Context, id, from, to, travelDate, token string)(Trip, TripDetails, error)
	GetTickets(ctx context.Context, id, from, to, travelDate, token string)(TripDetails, error)
	QueryAll(ctx context.Context, token string)([]Trip, error)
	AdminQueryAll(ctx context.Context, token string)([]Trip, []Train, []Route,  error)
}

type TravelServiceImpl struct{
	db components.NoSQLDatabase
	routeService RouteService
	trainService TrainService
	ticketInfoService TicketInfoService
	orderService OrderService
	seatService SeatService
	roles []string
}

func(tsi *TravelServiceImpl) GetTrainTypeByTripId(ctx context.Context, tripId, token string) (Train, error){
	err := Authenticate(token)
	if err != nil {
		return Train{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")
	
	//! TODO gotta verify this
	query := fmt.Sprintf(`{"Id": {"Type": %s, "Number": %s}}`, tripId[:1], tripId[1:])
	//! Can also do like this: 
	// query := fmt.Sprintf(`{"Id": %v }`, TripId{
	// 	Type: tripId[:1]
	// 	Number: tripId[1:]
	// })

	result, err := collection.FindOne(query) 
	if err != nil{
		return Train{}, err
	}

	var trip Trip
	err	= result.Decode(&trip)
	if err != nil {
		return Train{}, nil
	}

	trainType, err := tsi.trainService.Retrieve(ctx, trip.TrainTypeId, token)
	if err != nil {
		return Train{}, err
	}

	return trainType, nil
}

func(tsi *TravelServiceImpl) GetRouteByTripId(ctx context.Context, tripId, token string)(Route, error){
	err := Authenticate(token)
	if err != nil {
		return Route{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")
	
	//! TODO gotta verify this; see previous route
	query := fmt.Sprintf(`{"Id": {"Type": %s, "Number": %s}}`, tripId[:1], tripId[1:])
	
	result, err := collection.FindOne(query) 
	if err != nil{
		return Route{}, err
	}

	var trip Trip

	err	= result.Decode(&trip)
	if err != nil {
		return Route{}, err
	}

	route, err := tsi.routeService.QueryById(ctx, trip.RouteId, token)
	if err != nil {
		return Route{}, err
	}
	return route, nil
}

func(tsi *TravelServiceImpl) GetTripsByRouteId(ctx context.Context, routeIds []string, token string) ([]Trip, error){
	
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	var tripList []Trip
	for _, routeId := range routeIds{
		query := fmt.Sprintf(`{"RouteId": %s}`, routeId)

		result, err := collection.FindMany(query) 
		if err != nil{
			fmt.Print(err)
			continue
		}

		var trips []Trip
		result.All(&trips)
		tripList = append(tripList, trips...)
	}
	
	return tripList, nil
}

func(tsi *TravelServiceImpl) UpdateTrip(ctx context.Context, trip Trip, token string) (Trip, error){
	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return Trip{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")
	
	query := fmt.Sprintf(`{"Id": {"Type": %s, "Number": %s}}`, trip.TrainTypeId, trip.Number)
	
	result, err := collection.FindOne(query) 
	if err != nil{
		return Trip{}, err
	}

	var existingTrip Trip
	result.Decode(&existingTrip)
	if existingTrip.Id == ""{
		return Trip{}, errors.New("Trip not found.")
	}

	err = collection.ReplaceOne(query, trip)
	if err != nil {
		return Trip{}, err
	}

	return trip, nil
}

func(tsi *TravelServiceImpl) Retrieve(ctx context.Context, tripId string, token string) (Trip, error){
	err := Authenticate(token)
	if err != nil {
		return Trip{}, err
	}

	query := fmt.Sprintf(`{"Id": %s`, tripId)
	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	result, err := collection.FindOne(query) 
	if err != nil{
		return Trip{}, err
	}

	var existingTrip Trip
	err = result.Decode(&existingTrip)
	if err != nil {
		return Trip{}, nil
	}

	return existingTrip, nil
}

func(tsi *TravelServiceImpl) CreateTrip(ctx context.Context, trip Trip, token string) (Trip, error){
	err := Authenticate(token)
	if err != nil {
		return Trip{}, err
	}

	query := fmt.Sprintf(`{"Id": {"Type": %s, "Number": %s}}`, trip.TrainTypeId, trip.Number)
	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	result, err := collection.FindOne(query) 
	if err != nil{
		return Trip{}, err
	}

	var existingTrip Trip
	err = result.Decode(&existingTrip)
	if err != nil || existingTrip.Id == ""{
		return Trip{}, nil
	}
	
	err = collection.InsertOne(trip)
	if err != nil {
		return Trip{}, nil
	}

	return trip, nil
}

func(tsi *TravelServiceImpl) DeleteTrip(ctx context.Context, tripId string, token string) (string, error){
	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return "", err
	}

	query := fmt.Sprintf(`{"Id": %s}`, tripId)
	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	err = collection.DeleteOne(query) 
	if err != nil{
		return "", err
	}

	return "Trip deleted.", nil
}

func(tsi *TravelServiceImpl) QueryInfo(ctx context.Context, startingPlace, endPlace, departureTime, token string) ([]TripDetails, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}


	startingPlaceId, err := tsi.ticketInfoService.QueryForStationId(ctx, startingPlace, token)
	if err != nil { 
		return nil, err
	}
	
	endPlaceId, err := tsi.ticketInfoService.QueryForStationId(ctx, endPlace, token)
	if err != nil { 
		return nil, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	result, err := collection.FindMany("") 
	if err != nil{
		return nil, err
	}

	var trips []Trip
	result.All(&trips)

	var route Route

	var tripDetailsList []TripDetails
	var tripDetails TripDetails
	for _, trip := range trips{
		route, err = tsi.routeService.QueryById(ctx, trip.RouteId, token)
		if err != nil {
			continue
		}

		foundLeft := false
		for _, station := range route.Stations{
			if startingPlaceId == station{
				foundLeft = true
				continue
			}

			if endPlaceId == station{
				if foundLeft{
					tripDetails, err = tsi.GetTickets(ctx, trip, route, startingPlaceId, endPlaceId, startingPlace, endPlace, departureTime, token)
					if err != nil {
						break
					}
					tripDetailsList = append(tripDetailsList, tripDetails)
				}else{
					break
				}
			}
		}
	}

	return tripDetailsList, nil

}

func(tsi *TravelServiceImpl) GetTripAllDetailInfo(ctx context.Context, id, from, to, travelDate, token string)(Trip, TripDetails, error){
	err := Authenticate(token)
	if err != nil {
		return Trip{}, TripDetails{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")
	
	//! TODO gotta verify this; see previous route
	query := fmt.Sprintf(`{"Id": {"Type": %s, "Number": %s}}`, id[:1], id[1:])
	
	result, err := collection.FindOne(query) 
	if err != nil{
		return Trip{}, TripDetails{}, err
	}

	var trip Trip
	result.Decode(&trip)

	startingPlaceId, err := tsi.ticketInfoService.QueryForStationId(ctx, from, token)
	if err != nil {
		return Trip{}, TripDetails{}, err
	}

	endPlaceId, err := tsi.ticketInfoService.QueryForStationId(ctx, to, token)
	if err != nil {
		return Trip{}, TripDetails{}, err
	}

	route, err := tsi.routeService.QueryById(ctx, trip.RouteId, token)
	if err != nil {
		return Trip{}, TripDetails{}, err
	}

	tripResponse, err := tsi.GetTickets(ctx, trip, route, startingPlaceId, endPlaceId, from, to, travelDate, token)
	if err != nil {
		return Trip{}, TripDetails{}, err
	}

	return trip, tripResponse, nil
}

func(tsi *TravelServiceImpl) QueryAll(ctx context.Context, token string)([]Trip, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	result, err := collection.FindMany("")  //TODO verify
	if err != nil {
		return nil, err
	}

	var trips []Trip
	err = result.All(&trips)
	if err != nil {
		return nil, err
	}

	return trips, nil
}

func(tsi *TravelServiceImpl) AdminQueryAll(ctx context.Context, token string)([]Trip, []Train, []Route,  error){
	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return nil, nil, nil, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trips")

	result, err := collection.FindMany("")  //TODO verify
	if err != nil {
		return nil, nil, nil, err
	}

	var trips []Trip
	err = result.All(&trips)
	if err != nil {
		return nil, nil, nil, err
	}

	var routes []Route
	var trainTypes []Train

	for _, trip := range trips{

		route, _ := tsi.routeService.QueryById(ctx, trip.RouteId, token)
		tt, _ := tsi.trainService.Retrieve(ctx, trip.TrainTypeId, token)

		routes = append(routes, route)
		trainTypes = append(trainTypes, tt)
	}

	return trips, trainTypes, routes, nil
}

//*************************************************************************************

func(tsi *TravelServiceImpl) GetTickets(ctx context.Context, trip Trip, route Route, startingPlaceId, endPlaceId, startingPlaceName, endPlaceName, departureTime, token string) (TripDetails, error){


	dateFormat := "Sat Jul 26 00:00:00 2025"

	depart, _ := time.Parse(dateFormat, departureTime)

	if depart.After(time.Now()){
		return TripDetails{}, errors.New("Departure time in the past.")
	}

	resForTravel, err := tsi.ticketInfoService.QueryForTravel(ctx, Travel{
		Trip: trip,
		StartingPlace: startingPlaceName,
		EndPlace: endPlaceName,
		DepartureTime: departureTime,
	}, token)

	if err != nil {
		return TripDetails{}, err
	}

	soldTicket, err := tsi.orderService.CalculateSoldTicket(ctx, departureTime, trip.TrainTypeId+trip.Number, token)
	if err != nil {
		return TripDetails{}, err
	}

	seat := Seat{
		TravelDate: departureTime,
		TrainNumber: trip.TrainTypeId+trip.Number,
		StartStation: startingPlaceId,
		DestStation: endPlaceId,
		SeatType: uint16(FirstClass),
	}

	//! TODO revisit after seatService impl
	first, err := tsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
	if err != nil {
		return TripDetails{}, err
	}
	seat.SeatType = uint16(SecondClass)

	//! TODO revisit after seatService impl
	second, err := tsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
	if err != nil {
		return TripDetails{}, err
	}

	trainType, err := tsi.trainService.Retrieve(ctx, trip.TrainTypeId, token)
	if err != nil {
		return TripDetails{}, err
	}

	tripResponse := TripDetails{
		ComfortClass: first,
		EconomyClass: second,
		StartingStation: startingPlaceName,
		EndStation: endPlaceName,
	}

	var indexStart int
	var indexEnd int
	for idx, st := range route.Stations{

		if st == startingPlaceId{
			indexStart = idx
		}
		if st == endPlaceId{
			indexEnd = idx
		}
	}

	distanceStart := route.Distances[indexStart] - route.Distances[0]
	distanceEnd := route.Distances[indexEnd] - route.Distances[0]

	minutesStart := 60 * distanceStart / trainType.AvgSpeed
	minutesEnd := 60 * distanceEnd / trainType.AvgSpeed


	tmpTime, _ := time.Parse(dateFormat, trip.StartingTime)
	startingTime :=  tmpTime.Add(time.Minute * time.Duration(minutesStart))
	endTime := tmpTime.Add(time.Minute * time.Duration(minutesEnd))

	tripResponse.StartingTime = startingTime.String()
	tripResponse.EndTime = endTime.String()

	tripResponse.TripId = soldTicket.TrainNumber
	tripResponse.TrainTypeId = trip.TrainTypeId
	tripResponse.PriceForComfortClass = resForTravel.Prices["ComfortClass"]
	tripResponse.PriceForEconomyClass = resForTravel.Prices["EconomyClass"]

	return tripResponse, nil
}