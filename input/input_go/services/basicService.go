package services

import (
	"context"
	"sync"
)


func CreateBasicService (trainService TrainService, stationService StationService, routeService RouteService, priceService PriceService) *BasicServiceImpl {
	
	return &BasicServiceImpl{
		trainService: trainService,
		stationService: stationService,
		routeService: routeService,
		priceService: priceService,
	}	
}

type BasicService interface{
	QueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error)
	QueryForStationId(ctx context.Context, name, token string) (string, error)
}

type BasicServiceImpl struct{
	trainService TrainService
	stationService StationService
	routeService RouteService
	priceService PriceService
}

func(bsi *BasicServiceImpl) QueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error){
	err := Authenticate(token)
	if err != nil {
		return TravelResult{}, err
	}

	var wg sync.WaitGroup
	wg.Add(5)

	var train Train
	var err1 error
	go func(){
		defer wg.Done()
		train, err1 = bsi.trainService.Retrieve(ctx, info.Trip.TrainTypeId, token)
	}()

	var route Route
	var err2 error
	go func(){
		defer wg.Done()
		route, err2 = bsi.routeService.QueryById(ctx, info.Trip.RouteId, token)
	}()

	var priceConfig PriceConfig
	var err3 error
	go func(){
		defer wg.Done()
		priceConfig, err3 = bsi.priceService.Query(ctx, info.Trip.RouteId, info.Trip.TrainTypeId, token)
	}()

	var startingPlaceId string
	var err4 error
	go func(){
		defer wg.Done()
		startingPlaceId, err4 = bsi.stationService.QueryForStationId(ctx, info.StartingPlace, token)
	}()

	var endPlaceId string
	var err5 error
	go func(){
		defer wg.Done()
		endPlaceId, err5 = bsi.stationService.QueryForStationId(ctx, info.EndPlace, token)
	}()

	wg.Wait()

	if err1 != nil {
		return TravelResult{}, err1
	}
	if err2 != nil {
		return TravelResult{}, err2
	}
	if err3 != nil {
		return TravelResult{}, err3
	}
	if err4 != nil {
		return TravelResult{}, err4
	}
	if err5 != nil {
		return TravelResult{}, err5
	}
	
	var indexStart int
	var indexEnd int

	for idx, val := range route.Stations{
		if val == startingPlaceId {
			indexStart = idx
			if indexEnd != 0{
				break
			}
		}
		if val == endPlaceId{
			indexEnd = idx
			if indexStart != 0 {
				break
			}
		}
	}

	distance := float32(route.Distances[indexEnd] - route.Distances[indexStart])
	priceForEconomyClass := distance * priceConfig.BasicPriceRate
	priceForComfortClass := distance * priceConfig.FirstClassPriceRate

	return TravelResult{
		TrainType: train,
		Prices: map[string]float32{
			"EconomyClass": priceForEconomyClass,
			"ComfortClass": priceForComfortClass,
		},
		Percent: 1.0,
	},nil
	
}

func(bsi *BasicServiceImpl) QueryForStationId(ctx context.Context, name, token string) (string, error){
	err := Authenticate(token)
	if err != nil {
		return "", err
	}

	stationId, err := bsi.stationService.QueryForStationId(ctx, name, token)
	if err != nil {
		return "", err
	}

	return stationId, nil
}
