package services

import (
	"time"
	jwt "github.com/dgrijalva/jwt-go"
)

type OrderTicketInfo struct{
	AccountId string
	ContactsId string
	TripId string
	SeatType uint16
	Date string
	From string
	To string
	Insurance uint16
	FoodType uint16
	StationName string
	StoreName string
	FoodName string
	FoodPrice float32
	HandleDate string
	ConsigneeName string
	ConsigneePhone string
	ConsigneWeight float32
	IsWithin bool
}

func (OrderTicketInfo) remote(){}

type FoodOrder struct{
	Id string
	OrderId string
	FoodType uint16
	StationName string
	StoreName string
	FoodName string
	Price float32
}

func (FoodOrder) remote(){}

type RebookInfo struct{
	LoginId string
	OrderId string
	OldTripId string
	TripId string
	SeatType uint16
	Date string
}
func (RebookInfo) remote(){}

type RoutePlanInfo struct{
	FromStationName string
	ToStationName string
	TravelDate string

	ViaStationName string
	TrainType string
}
func (RoutePlanInfo) remote(){}

type Seat struct{
	TravelDate string
	TrainNumber string
	StartStation string
	DestStation string
	SeatType uint16
}
func (Seat) remote(){}

type TravelResult struct{
	TrainType Train
	Route Route
	PriceConfig PriceConfig
	Prices map[string]float32
	Percent float32
}
func (TravelResult) remote(){}

type Travel struct{
	Trip Trip
	StartingPlace string
	EndPlace string
	DepartureTime string
}
func (Travel) remote(){}

type TripSummary struct{
	Trip Trip
	Route Route
	TrainType Train
}
func (TripSummary) remote(){}

type TripDetails struct{
	ComfortClass uint16
	EconomyClass uint16
	StartingStation string
	EndStation string
	StartingTime string
	EndTime string
	TripId string
	TrainTypeId string
	PriceForComfortClass float32
	PriceForEconomyClass float32

	StopStations []string
	NumberOfRestTicketFirstClass uint16
	NumberOfRestTicketSecondClass uint16
}
func (TripDetails) remote(){}

type NotificationInfo struct{
	SendStatus bool
	Email string
	OrderNumber string
	Username string
	StartingPlace string
	EndPlace string
	StartingTime string
	Date string
	SeatClass string
	SeatNumber string
	Price string
}
func (NotificationInfo) remote(){}

type SecurityConfig struct{
	Id string
	Name string
	Value string
	Description string
}
func (SecurityConfig) remote(){}

type AddMoney struct{
	Id string
	UserId string
	Money string
	Type string
}
func (AddMoney) remote(){}

type Balances struct{
	UserId string
	Balance float32
}
func (Balances) remote(){}

type Voucher struct {
	VoucherId string
	OrderId string
	TravelDate string
	ContactName string
	TrainNumber string
	SeatClass uint16
	SeatNumber string
	StartStation string
	DestStation string
	Price float32
}

type OrderInfo struct{
	LoginId string
	TravelDateStart string
	TravelDateEnd string
	BoughtDateStart string
	BoughtDateEnd string
	State uint16
	EnableTravelDateQuery bool
	EnableBoughtDateQuery bool
	EnableStateQuery bool
}
type SoldTicket struct{
	TravelDate string
	TrainNumber string
	NoSeat uint16
	BusinessSeat uint16
	FirstClassSeat uint16
	SecondClassSeat uint16
	HardSeat uint16
	SoftSeat uint16
	HardBed uint16
	SoftBed uint16
	HighSoftBed uint16
}

type Ticket struct{
	SeatNo string
	StartStation string
	DestStation string
}


type Contact struct{
	Id string
	AccountId string
	Name string
	DocumentType uint16
	DocumentNumber string
	PhoneNumber string
}

type Station struct{
	Id string
	Name string
	StayTime uint16
}

type Train struct{
	Id string
	EconomyClass uint16
	ComfortClass uint16
	AvgSpeed uint16
}

type Config struct{
	Name string
	Value string
	Description string
}

type PriceConfig struct{
	Id string
	TrainType string
	RouteId string
	BasicPriceRate float32
	FirstClassPriceRate float32
}

type Consign struct {
	Id string
	OrderId string
	AccountId string
	HandleDate string
	TargetDate string
	From string
	To string
	Consignee string
	Phone string
	Weight float32
	Within bool
	Price float32
}

type ConsignPriceConfig struct{
	Id string 
	Index uint16 
	InitialWeight float32 
	InitialPrice float32
	WithinPrice float32 
	BeyondPrice float32
}

type Order struct{
	Id string
	BoughtDate string
	TravelDate string
	AccountId string
	ContactsName string
	DocumentType uint16
	ContactsDocumentNumber string
	TrainNumber string
	CoachNumber string
	SeatClass uint16
	SeatNumber string
	From string
	To string
	Status uint16
	Price float32
}

type RouteRequest struct{
	Id string
	StartStation string
	EndStation string
	Stations []string
	Distances []uint16
}

type Route struct{
	Id string
	StartStationId string
	TerminalStationId string
	Stations []string
	Distances []uint16
}

type Trip struct{
	Id string
	TrainTypeId string
	Number string
	RouteId string
	StartingTime string
	EndTime string
	StartingStationId string
	StationsId string
	TerminalStationId string
}

type User struct{
	Username string
	Password string
	Role string
	UserId string
	Email string
	DocumentType uint16
	DocumentNumber string
	Gender uint16
}

type Captcha struct{
	Name string
	Value string
	TTL time.Duration
}
type Insurance struct{
	Id string
	OrderId string
	Type InsuranceType
}
type InsuranceType struct{
	Id string
	Index uint16
	Name string
	Price float32
	TypeId string
}
type Payment struct{
	Id string
	OrderId string
	UserId string
	Price string
	Type string
}
type Store struct{
	Id string
	StationId string
	StoreName string
	Telephone string
	BusinessTime string
	DeliveryFee string
	FoodList []Food
}

type Delivery struct {
	FoodName string
	ID string
	StationName string
	StoreName string
}

func (d Delivery) remote(){}

type TrainFood struct{
	Id string
	TripId string
	FoodList []Food
}

type Food struct {
	FoodName string
	Price float32
}

func (f Food) remote() {}

type Office struct{
	OfficeName string
	Address string
	WorkTime string
	WindowNum uint16
}

func (Contact) remote(){}
func (Station) remote(){}
func (Train) remote(){}
func (Config) remote(){}
func (PriceConfig) remote(){}
func (Consign) remote(){}
func (ConsignPriceConfig) remote(){}
func (Route) remote(){}
func (Trip) remote(){}
func (User) remote(){}
func (Captcha) remote(){}
func (Insurance) remote(){}
func (Payment) remote(){}
func (Store) remote(){}
func (TrainFood) remote(){}
func (Order) remote(){}
func (Ticket) remote(){}
func (SoldTicket) remote(){}
func (OrderInfo) remote(){}
func (Voucher) remote(){}

type TokenData struct {
	UserId string
	Username string
	Timestamp uint64
	Ttl uint32
	Role string
	jwt.StandardClaims
}


//************************************** ENUMS ******************************************


type OrderStatus uint16

const (
    NotPaid OrderStatus = iota
    Paid
    Collected
    Change
	Cancel
	Refund
	Used
)

func (o OrderStatus) String() string {
    return [...]string{"NotPaid", "Paid", "Collected", "Change", "Cancel", "Refund", "Used"}[o]
}

type SeatClass uint16

const (
    None SeatClass = iota
    Business
    FirstClass
    SecondClass
	HardSeat
	SoftSeat
	HardBed
	SoftBed
	HighSoftBed
)

func (s SeatClass) String() string {
    return [...]string{"None", "Business", "FirstClass", "SecondClass", "HardSeat", "SoftSeat", "HardBed", "SoftBed", "HighSoftBed"}[s]
}

type PaymentType uint16

const (
    NormalPayment PaymentType = iota
    Difference
    OutsidePayment
    ExternalAndDifferencePayment
)

func (s PaymentType) String() string {
    return [...]string{"Payment", "Difference", "OutsidePayment", "ExternalAndDifferencePayment"}[s]
}


type MoneyType uint16

const (
    AddMoneyType MoneyType = iota
    DrawBackMoney
    
)

func (s MoneyType) String() string {
    return [...]string{"AddMoney", "DrawBackMoney"}[s]
}