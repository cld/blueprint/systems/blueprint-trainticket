package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
)

func CreateStationService (stationDatabase components.NoSQLDatabase) *StationServiceImpl {
	
	return &StationServiceImpl{
		db: stationDatabase,
		roles: []string{"ROLE_ADMIN"},
	}
}

type StationService interface{
	Query(ctx context.Context, token string) ([]Station, error)
	Create(ctx context.Context, station Station, token string)(Station, error)
	Update(ctx context.Context, station Station, token string)(Station, error)
	Delete(ctx context.Context, station Station, token string) (string, error)
	QueryForStationId(ctx context.Context, stationName, token string) (string, error)
	QueryForIdBatch(ctx context.Context, stationNameList []string, token string) ([]string, error)
	QueryById(ctx context.Context, stationId, token string)(string, error)
	QueryForNameBatch(ctx context.Context, stationIdList []string, token string) ([]string, error)
}

type StationServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}

func(ssi *StationServiceImpl) Query(ctx context.Context, token string) ([]Station, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("stations")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var stations []Station
	err = result.All(&stations)
	if err != nil{
		return nil, err
	}

	return stations, nil
}

func(ssi *StationServiceImpl) Create(ctx context.Context, station Station, token string)(Station, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return Station{}, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")
	query := fmt.Sprintf(`{"Id": %s }`, station.Id)

	res, err := collection.FindOne(query)
	if err == nil {
		var oldStation Station
		res.Decode(&oldStation)
		
		if oldStation.Id != "" {
			return Station{}, errors.New("Station already exists!")
		}
	}
	
	err = collection.InsertOne(station) 
	if err != nil{
		return Station{}, err
	}

	return station, nil
}

func(ssi *StationServiceImpl) Update(ctx context.Context, station Station, token string)(Station, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return Station{}, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")
	query := fmt.Sprintf(`{"Id": %s }`, station.Id)

	res, err := collection.FindOne(query)
	if err != nil {		
		return Station{}, err
	}

	var oldStation Station
	res.Decode(&oldStation)

	if oldStation.Id == "" {
		return Station{}, errors.New("Station does not exist!")
	}

	station.Id = oldStation.Id

	err = collection.ReplaceOne(query, station) 
	if err != nil{
		return Station{}, err
	}

	return station, nil
}

func(ssi *StationServiceImpl) Delete(ctx context.Context, station Station, token string) (string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return "", err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")
	query := fmt.Sprintf(`{"Id": %s }`, station.Id)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Station removed successfully", nil
}

func(ssi *StationServiceImpl) QueryForStationId(ctx context.Context, stationName, token string) (string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return "", err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")
	query := fmt.Sprintf(`{"Name": %s }`, stationName)

	res, err := collection.FindOne(query)
	if err != nil{
		return "", err
	}

	var station Station
	res.Decode(&station)

	return station.Id, nil
}

func(ssi *StationServiceImpl) QueryForIdBatch(ctx context.Context, stationNameList []string, token string) ([]string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return nil, err
	}

	var query string
	collection := ssi.db.GetDatabase("ts").GetCollection("station")

	_, err = collection.FindOne(query)
	if err != nil{
		return nil, err
	}

	var ids []string
	var station Station

	for _, name := range stationNameList {
		query = fmt.Sprintf(`{"Name": %s`, name)
		res, err := collection.FindOne(query)
		if err == nil{
			res.Decode(&station)
			ids = append(ids, station.Id)
		}
	}

	return ids, nil
}

func(ssi *StationServiceImpl) QueryById(ctx context.Context, stationId, token string)(string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return "", err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")
	query := fmt.Sprintf(`{"Id": %s }`, stationId)

	res, err := collection.FindOne(query)
	if err != nil{
		return "", err
	}

	var station Station
	res.Decode(&station)

	return station.Name, nil
}

func(ssi *StationServiceImpl) QueryForNameBatch(ctx context.Context, stationIdList []string, token string) ([]string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return nil, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("station")

	var names []string
	var query string
	var station Station

	for _, sId := range stationIdList {
		query = fmt.Sprintf(`{"Id": %s`, sId)
		res, err := collection.FindOne(query)
		if err == nil{
			res.Decode(&station)
			names = append(names, station.Name)
		}
	}

	return names, nil
}
