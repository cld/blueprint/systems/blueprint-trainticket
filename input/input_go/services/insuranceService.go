package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

const TA_INSURANCE = "TRAFFIC_ACCIDENT"

func CreateInsuranceService (insuranceDatabase components.NoSQLDatabase) *InsuranceServiceImpl {
	
	return &InsuranceServiceImpl{
		db: insuranceDatabase,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type InsuranceService interface{
	GetAllInsurances(ctx context.Context, token string)([]Insurance, error)
	GetAllInsuranceTypes(ctx context.Context, token string) ([]InsuranceType, error)
	DeleteInsurance(ctx context.Context, insuranceId, token string) (string, error)
	DeleteInsuranceByOrderId(ctx context.Context, orderId, token string) (string, error)
	ModifyInsurance(ctx context.Context, typeIndex uint16, insuranceId, orderId, token string) (Insurance, error)
	CreateNewInsurance(ctx context.Context, typeIndex uint16, orderId, token string) (Insurance, error)
	GetInsuranceById(ctx context.Context, Id, token string) (Insurance, error)
	FindInsuranceByOrderId(ctx context.Context, orderId, token string) (Insurance, error)
}

type InsuranceServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}


func(isi *InsuranceServiceImpl) GetAllInsurances(ctx context.Context, token string)([]Insurance, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return nil, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")

	result, err := collection.FindMany("") //TODO verify this query-string works!

	if err != nil{
		return nil, err
	}

	var insurances []Insurance
	err = result.All(&insurances)
	if err != nil{
		return nil, err
	}

	return insurances, nil
}

func(isi *InsuranceServiceImpl) GetAllInsuranceTypes(ctx context.Context, token string) ([]InsuranceType, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return nil, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurance_types")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var insuranceTypes []InsuranceType
	err = result.All(&insuranceTypes)
	if err != nil{
		return nil, err
	}

	return insuranceTypes, nil
}

func(isi *InsuranceServiceImpl) DeleteInsurance(ctx context.Context, insuranceId, token string) (string, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return "", err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	query := fmt.Sprintf(`{"Id": %s }`, insuranceId)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Insurance removed successfully", nil
}

func(isi *InsuranceServiceImpl) DeleteInsuranceByOrderId(ctx context.Context, orderId, token string) (string, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return "", err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	query := fmt.Sprintf(`{"OrderId": %s }`, orderId)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Insurance removed successfully", nil
}

func(isi *InsuranceServiceImpl) ModifyInsurance(ctx context.Context, typeIndex uint16, insuranceId, orderId, token string) (Insurance, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return Insurance{}, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	query := fmt.Sprintf(`{"OrderId": %s, "Id": %s}`, orderId, insuranceId)
	
	res, err := collection.FindOne(query) 
	if err != nil {
		return Insurance{}, err
	}

	var oldInsurance Insurance
	res.Decode(&oldInsurance)
	if oldInsurance.Id == "" {
		return Insurance{}, errors.New("Insurance does not exist!")
	}

	insTypesCollection := isi.db.GetDatabase("ts").GetCollection("insurance_types")
	typesQuery := fmt.Sprintf(`{"Index": %s }`, typeIndex)
	
	res, err = insTypesCollection.FindOne(typesQuery) 
	if err != nil {
		return Insurance{}, err
	}

	var insType InsuranceType
	res.Decode(&insType)

	if insType.Id == "" {
		return Insurance{}, errors.New("Insurance Type does not exist!")
	}

	newInsurance := Insurance{
		Id: insuranceId,
		OrderId: orderId,
		Type: insType,
	}

	err = collection.ReplaceOne(query, newInsurance) 
	if err != nil{
		return Insurance{}, err
	}

	return newInsurance, nil
}

func(isi *InsuranceServiceImpl) CreateNewInsurance(ctx context.Context, typeIndex uint16, orderId, token string) (Insurance, error){
	err := Authenticate(token, isi.roles...)
	if err != nil {
		return Insurance{}, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	
	query := fmt.Sprintf(`{"OrderId": %s }`, orderId)
	res, err := collection.FindOne(query) 
	if err == nil {
		var oldInsurance Insurance

		res.Decode(&oldInsurance)
		
		if oldInsurance.Id != "" {
			return Insurance{}, errors.New("Insurance already exists!")
		}
	}

	insTypesCollection := isi.db.GetDatabase("ts").GetCollection("insurance_types")
	query = fmt.Sprintf(`{"Index": %s }`, typeIndex)
	res, err = insTypesCollection.FindOne(query) 
	if err != nil {
		return Insurance{}, err
	}

	var insType InsuranceType
	res.Decode(&insType)

	insurance := Insurance{
		Id: uuid.New().String(),
		OrderId: orderId,
		Type: insType,
	}

	err = collection.InsertOne(insurance) 
	if err != nil{
		return Insurance{}, err
	}

	return insurance, nil
}

func(isi *InsuranceServiceImpl) GetInsuranceById(ctx context.Context, Id, token string) (Insurance, error){
	err := Authenticate(token)
	if err != nil {
		return Insurance{}, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	query := fmt.Sprintf(`{"Id": %s}`, Id)

	result, err := collection.FindOne(query)
	if err != nil{
		return Insurance{}, err
	}

	var insurance Insurance
	err = result.Decode(&insurance)
	if err != nil{
		return Insurance{}, err
	}

	return insurance, nil
}

func(isi *InsuranceServiceImpl) FindInsuranceByOrderId(ctx context.Context, orderId, token string) (Insurance, error){
	err := Authenticate(token)
	if err != nil {
		return Insurance{}, err
	}

	collection := isi.db.GetDatabase("ts").GetCollection("insurances")
	query := fmt.Sprintf(`{"OrderId": %s}`, orderId)
	
	result, err := collection.FindOne(query)
	if err != nil{
		return Insurance{}, err
	}

	var insurance Insurance
	err = result.Decode(&insurance)
	if err != nil{
		return Insurance{}, err
	}

	return insurance, nil
}
