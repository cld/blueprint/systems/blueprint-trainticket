package services

import (
	"github.com/google/uuid"
	"github.com/jellydator/ttlcache/v2"
	"time"
	"context"
)

func CreateVerificationCodeService () *VerificationCodeServiceImpl {

	var cache ttlcache.SimpleCache = ttlcache.NewCache() //returns *ttlcache.Cache
	expiry:= time.Duration(10*time.Second)

	// cache.SetTTL()
	// Doesn't work:
	//cache.SkipTtlExtensionOnHit(true)

	return &VerificationCodeServiceImpl{
		cache: cache,
		expiry: expiry,
	}
}

type VerificationCodeService interface {
	Verify(ctx context.Context, receivedCode string, cookie Captcha) (Captcha, bool, error)
	Generate(ctx context.Context, cookie Captcha) (Captcha, string, error)
}

type VerificationCodeServiceImpl struct{
	cache ttlcache.SimpleCache
	expiry time.Duration
}


func(vcsi *VerificationCodeServiceImpl) Verify(ctx context.Context, receivedCode string, cookie Captcha) (Captcha, bool, error){
	//? How is an empty struct to be checked whether empty or not? compare to Captcha{} ?
	
	//! User sends request to Verify
	//! If they have attached a Cookie, we check in the cache and compare the given code
	//! Else, we generate new cookie and return it (BUT not add it to the cache)
	//! In the latter case, we send an "Invalid" flag to the user as well.

	cookieId := ""
	var captchaCookie Captcha

	if cookie == (Captcha{}){

		captchaCookie = Captcha{
			Name: "YsbCaptcha",
			Value: cookieId,
			TTL: vcsi.expiry,
		}
	}else{
		cookieId = cookie.Value
	}

	entry, err := vcsi.cache.Get(cookieId)
	if err != nil{
		return captchaCookie, false, err
	}

	if entry == receivedCode{
		
		return captchaCookie, true, nil
	}

	return captchaCookie, false, nil
}

func(vcsi *VerificationCodeServiceImpl) Generate(ctx context.Context, cookie Captcha) (Captcha, string, error){

	//! Generate new captcha
	//! Add it to the TTL cache
	//! BUT, if already in cache, return what is in cache

	resCode := GenerateRandomString(4)

	var	captchaCookie Captcha

	var cookieId string

	if cookie == (Captcha{}) {

		cookieId = uuid.New().String()

		captchaCookie = Captcha{
			Name: "YsbCaptcha",
			Value: cookieId,
			TTL: vcsi.expiry,
		}

	}else{
		cookieId = cookie.Value
	}

	vcsi.cache.SetWithTTL(cookieId, resCode, vcsi.expiry) //* can also set TTL globally

	return captchaCookie, resCode, nil
}

