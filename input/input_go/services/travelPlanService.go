package services

import (
	"context"
	"sync"
)

func CreateTravelPlanService (routePlanService RoutePlanService, seatService SeatService, travelService TravelService, travel2Service Travel2Service, ticketInfoService TicketInfoService, stationService StationService) *TravelPlanServiceImpl {
	
	return &TravelPlanServiceImpl{
		travelService: travelService,
		travel2Service: travel2Service,
		seatService: seatService,
		stationService: stationService,
		ticketInfoService: ticketInfoService, 
		routePlanService: routePlanService,
		// roles: []string{"ROLE_ADMIN"}
	}
}

type TravelPlanService interface{
	GetTransferResult(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, []TripDetails, error)
	GetByCheapest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetByQuickest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error)
	GetByMinStation(ctx context.Context, info RoutePlanInfo, token string)([]TripDetails, error)
}

type TravelPlanServiceImpl struct{
	travelService TravelService
	travel2Service Travel2Service
	seatService SeatService
	stationService StationService
	ticketInfoService TicketInfoService
	routePlanService RoutePlanService
	// roles []string
}

func(tpsi *TravelPlanServiceImpl) GetTransferResult(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, []TripDetails, error){
	
	err := Authenticate(token)
	if err != nil {
		return nil, nil, err
	}

	var wg sync.WaitGroup
	wg.Add(4)

	var err1, err2, err3, err4 error
	var firstSectionFromHighSpeed, firstSectionFromNormal, secondSectionFromHighSpeed, secondSectionFromNormal []TripDetails

	go func() { 
		defer wg.Done()
		firstSectionFromHighSpeed, err1 = tpsi.travelService.QueryInfo(ctx, info.FromStationName, info.ViaStationName, info.TravelDate, token)
	}()
	
	go func() { 
		defer wg.Done()
		firstSectionFromNormal, err2 = tpsi.travel2Service.QueryInfo(ctx, info.FromStationName, info.ViaStationName, info.TravelDate, token)
	}()

	go func() { 
		defer wg.Done()
		secondSectionFromHighSpeed, err1 = tpsi.travelService.QueryInfo(ctx, info.ViaStationName, info.ToStationName, info.TravelDate, token)
	}()
	
	go func() { 
		defer wg.Done()
		secondSectionFromNormal, err2 = tpsi.travel2Service.QueryInfo(ctx, info.ViaStationName, info.ToStationName, info.TravelDate, token)
	}()
	wg.Wait()

	if err1 != nil{
		return nil, nil, err1
	}
	if err2 != nil{
		return nil, nil, err2
	}
	if err3 != nil{
		return nil, nil, err3
	}
	if err4 != nil{
		return nil, nil, err4
	}

	firstSection := append(firstSectionFromHighSpeed, firstSectionFromNormal...)
	secondSection := append(secondSectionFromHighSpeed, secondSectionFromNormal...)

	return firstSection, secondSection, nil
}

func(tpsi *TravelPlanServiceImpl) GetByCheapest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	cheapestTrips, err := tpsi.routePlanService.GetCheapestRoutes(ctx, info, token)
	if err != nil {
		return nil, err
	}

	var finalTrips []TripDetails

	var err1, err2, err3, err4, err5 error
	var wg sync.WaitGroup
	var stopNames []string
	var fromStationId, toStationId string
	var first, second uint16
	for _, trip := range cheapestTrips{

		wg.Add(3)
		go func() { 
			defer wg.Done()
			stopNames, err1 = tpsi.stationService.QueryForNameBatch(ctx, trip.StopStations, token)
		}()
		go func() { 
			defer wg.Done()
			fromStationId, err2 = tpsi.stationService.QueryForStationId(ctx, trip.StartingStation, token)
		}()
		go func() { 
			defer wg.Done()
			toStationId, err3 = tpsi.stationService.QueryForStationId(ctx, trip.EndStation, token)
		}()
		wg.Wait()
		
		seat := Seat{
			TravelDate: info.TravelDate,
			TrainNumber: trip.TripId,
			StartStation: fromStationId,
			DestStation: toStationId,
			SeatType: uint16(FirstClass),
		}

		if err1 != nil || err2 != nil || err3 != nil {
			continue
		}

		wg.Add(2)
		go func() { 
			defer wg.Done()
			first, err4 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()

		go func() { 
			seat.SeatType = uint16(SecondClass)
			defer wg.Done()
			second, err5 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()
		wg.Wait()

		if err4 != nil || err5 != nil {
			continue
		}

		trip.StopStations = stopNames
		trip.NumberOfRestTicketFirstClass = first
		trip.NumberOfRestTicketSecondClass = second

		finalTrips = append(finalTrips, trip)
	}

	return finalTrips, nil
}

func(tpsi *TravelPlanServiceImpl) GetByQuickest(ctx context.Context, info RoutePlanInfo, token string) ([]TripDetails, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	quickestTrips, err := tpsi.routePlanService.GetQuickestRoutes(ctx, info, token)
	if err != nil {
		return nil, err
	}

	var finalTrips []TripDetails

	var err1, err2, err3, err4, err5 error
	var wg sync.WaitGroup
	var stopNames []string
	var fromStationId, toStationId string
	var first, second uint16
	for _, trip := range quickestTrips{

		wg.Add(3)
		go func() { 
			defer wg.Done()
			stopNames, err1 = tpsi.stationService.QueryForNameBatch(ctx, trip.StopStations, token)
		}()
		go func() { 
			defer wg.Done()
			fromStationId, err2 = tpsi.stationService.QueryForStationId(ctx, trip.StartingStation, token)
		}()
		go func() { 
			defer wg.Done()
			toStationId, err3 = tpsi.stationService.QueryForStationId(ctx, trip.EndStation, token)
		}()
		wg.Wait()
		
		seat := Seat{
			TravelDate: info.TravelDate,
			TrainNumber: trip.TripId,
			StartStation: fromStationId,
			DestStation: toStationId,
			SeatType: uint16(FirstClass),
		}

		if err1 != nil || err2 != nil || err3 != nil {
			continue
		}
		
		wg.Add(2)
		go func() { 
			defer wg.Done()
			first, err4 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()

		go func() { 
			seat.SeatType = uint16(SecondClass)
			defer wg.Done()
			second, err5 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()
		wg.Wait()

		if err4 != nil || err5 != nil {
			continue
		}

		trip.StopStations = stopNames
		trip.NumberOfRestTicketFirstClass = first
		trip.NumberOfRestTicketSecondClass = second

		finalTrips = append(finalTrips, trip)
	}

	return finalTrips, nil

}

func(tpsi *TravelPlanServiceImpl) GetByMinStation(ctx context.Context, info RoutePlanInfo, token string)([]TripDetails, error){
	
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	lessHopTripsquickestTrips, err := tpsi.routePlanService.GetMinStopStations(ctx, info, token)
	if err != nil {
		return nil, err
	}

	var finalTrips []TripDetails

	var err1, err2, err3, err4, err5 error
	var wg sync.WaitGroup
	var stopNames []string
	var fromStationId, toStationId string
	var first, second uint16
	for _, trip := range lessHopTripsquickestTrips{

		wg.Add(3)
		go func() { 
			defer wg.Done()
			stopNames, err1 = tpsi.stationService.QueryForNameBatch(ctx, trip.StopStations, token)
		}()
		go func() { 
			defer wg.Done()
			fromStationId, err2 = tpsi.stationService.QueryForStationId(ctx, trip.StartingStation, token)
		}()
		go func() { 
			defer wg.Done()
			toStationId, err3 = tpsi.stationService.QueryForStationId(ctx, trip.EndStation, token)
		}()
		wg.Wait()
		
		seat := Seat{
			TravelDate: info.TravelDate,
			TrainNumber: trip.TripId,
			StartStation: fromStationId,
			DestStation: toStationId,
			SeatType: uint16(FirstClass),
		}

		if err1 != nil || err2 != nil || err3 != nil {
			continue
		}
		
		wg.Add(2)
		go func() { 
			defer wg.Done()
			first, err4 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()

		go func() { 
			seat.SeatType = uint16(SecondClass)
			defer wg.Done()
			second, err5 = tpsi.seatService.GetLeftTicketOfInterval(ctx, seat, token)
		}()
		wg.Wait()

		if err4 != nil || err5 != nil {
			continue
		}

		trip.StopStations = stopNames
		trip.NumberOfRestTicketFirstClass = first
		trip.NumberOfRestTicketSecondClass = second

		finalTrips = append(finalTrips, trip)
	}

	return finalTrips, nil

}

