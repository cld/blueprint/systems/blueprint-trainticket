package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

func CreatePriceService (priceDatabase components.NoSQLDatabase) *PriceServiceImpl {
	
	return &PriceServiceImpl{
		db: priceDatabase,
		roles: []string{"ROLE_ADMIN"},
	}
}

type PriceService interface{
	Query(ctx context.Context, routeId, trainType, token string)(PriceConfig, error)
	QueryAll(ctx context.Context, token string)([]PriceConfig, error)
	Create(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
	Delete(ctx context.Context, pc PriceConfig, token string) (string, error)
	Update(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
}

type PriceServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}

func(psi *PriceServiceImpl) Query(ctx context.Context, routeId, trainType, token string)(PriceConfig, error){
	err := Authenticate(token)

	if err != nil {
		return PriceConfig{}, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("price_config")

	query := fmt.Sprintf(`{"RouteId": %s, "TrainType": %s}`, routeId, trainType)

	result, err := collection.FindOne(query)

	if err != nil{
		return PriceConfig{}, err
	}

	var pc PriceConfig
	err = result.Decode(&pc)
	if err != nil{
		return PriceConfig{}, err
	}

	return pc, nil
}

func(psi *PriceServiceImpl) QueryAll(ctx context.Context, token string)([]PriceConfig, error){
	err := Authenticate(token)

	if err != nil {
		return nil, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("price_config")
	result, err := collection.FindMany("") //TODO verify this query-string works!

	if err != nil{
		return nil, err
	}

	var pcs []PriceConfig
	err = result.All(&pcs)
	if err != nil{
		return nil, err
	}

	return pcs, nil
}

func(psi *PriceServiceImpl) Create(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error){
	err := Authenticate(token, psi.roles...)

	if err != nil {
		return PriceConfig{}, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("price_config")
	
	query := fmt.Sprintf(`{"Id": %s }`, pc.Id)

	res, err := collection.FindOne(query) 

	if err == nil {
		var oldPc PriceConfig

		res.Decode(&oldPc)
		
		if oldPc.Id != "" {
			return PriceConfig{}, errors.New("Price Config already exists!")
		}
	}
	
	pc.Id = uuid.New().String()
	err = collection.InsertOne(pc) 
	if err != nil{
		return PriceConfig{}, err
	}

	return pc, nil
}

func(psi *PriceServiceImpl) Delete(ctx context.Context, pc PriceConfig, token string) (string, error){
	err := Authenticate(token, psi.roles...)

	if err != nil {
		return "", err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("price_config")
	query := fmt.Sprintf(`{"Id": %s }`, pc.Id)

	res, err := collection.FindOne(query) 

	if err != nil {
		return "", err
	}

	var existingPc PriceConfig
	res.Decode(&existingPc)
	if existingPc.Id == ""{
		return "", errors.New("Could not find price config!")
	}


	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Price Config removed successfully", nil
}

func(psi *PriceServiceImpl) Update(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error){
	err := Authenticate(token, psi.roles...)

	if err != nil {
		return PriceConfig{}, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("price_config")
	
	query := fmt.Sprintf(`{"Id": %s }`, pc.Id)

	res, err := collection.FindOne(query) 

	if err != nil {
		return PriceConfig{}, err
	}

	var existingPc PriceConfig
	res.Decode(&existingPc)
	
	if existingPc.Id == ""{
		return PriceConfig{}, errors.New("Could not find price config!")
	}

	query = fmt.Sprintf(`{"Id": %s}`, pc.Id)
	update := fmt.Sprintf(`{"$set": {"BasicPriceRate": %d, "FirstClassPriceRate": %d, "RouteId": %s, "TrainType": %s}`, pc.BasicPriceRate, pc.FirstClassPriceRate, pc.RouteId, pc.TrainType)

	err = collection.UpdateOne(query, update)
	if err != nil{
		return PriceConfig{}, err
	}

	return pc, nil
}

