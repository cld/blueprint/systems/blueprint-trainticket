package services

import (
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"context"
	"errors"
	"fmt"
)


func CreateConfigService (configDatabase components.NoSQLDatabase) *ConfigServiceImpl{

	return &ConfigServiceImpl{
		db: configDatabase,
	}
}

type ConfigService interface{
	QueryAll(ctx context.Context) ([]Config, error)
	CreateConfig(ctx context.Context, info Config) (Config, error)
	UpdateConfig(ctx context.Context, info Config) (Config, error)
	DeleteConfig(ctx context.Context, configName string) (string, error)
	Retrieve(ctx context.Context, configName string) (Config, error)
}

type ConfigServiceImpl struct {
	db components.NoSQLDatabase
}

func(csi *ConfigServiceImpl) QueryAll(ctx context.Context) ([]Config, error){
	collection := csi.db.GetDatabase("ts").GetCollection("config")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var configs []Config
	err = result.All(&configs)
	if err != nil{
		return nil, err
	}

	return configs, nil
}

func(csi *ConfigServiceImpl) CreateConfig(ctx context.Context, info Config) (Config, error){
	collection := csi.db.GetDatabase("ts").GetCollection("config")

	query := fmt.Sprintf(`{"Name": %s }`, info.Name)

	result, err := collection.FindOne(query)
	if err == nil{

		var oldConfig Config
		err = result.Decode(&oldConfig)
		if err == nil{
			return Config{}, errors.New("A config with this name already exists!")
		}
	}

	err = collection.InsertOne(info)
	if err != nil{
		return Config{}, err
	}

	return info, nil
}

func(csi *ConfigServiceImpl) UpdateConfig(ctx context.Context, info Config) (Config, error){
	collection := csi.db.GetDatabase("ts").GetCollection("config")

	query := fmt.Sprintf(`{"Name": %s }`, info.Name)

	_, err := collection.FindOne(query)
	if err != nil{
		return Config{}, err
	}

	err = collection.ReplaceOne(query, info)
	if err != nil{
		return Config{}, err
	}

	return info, nil
}

func(csi *ConfigServiceImpl) DeleteConfig(ctx context.Context, configName string) (string, error){
	collection := csi.db.GetDatabase("ts").GetCollection("config")

	query := fmt.Sprintf(`{"Name": %s }`, configName)

	_, err := collection.FindOne(query)
	if err != nil{
		return "", err
	}

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Config deleted.", nil
}

func(csi *ConfigServiceImpl) Retrieve (ctx context.Context, configName string) (Config, error){
	
	collection := csi.db.GetDatabase("ts").GetCollection("config")

	query := fmt.Sprintf(`{"Name": %s }`, configName)
	result, err := collection.FindOne(query) 
	if err != nil{
		return Config{}, err
	}

	var config Config
	err = result.Decode(&config)
	if err != nil{
		return Config{}, err
	}

	return config, nil
}
