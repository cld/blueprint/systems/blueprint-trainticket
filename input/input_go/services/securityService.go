package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"time"
	"fmt"
	"github.com/google/uuid"
	"strconv"
)

func CreateSecurityService (securityDatabase components.NoSQLDatabase, orderService OrderService, orderOtherService OrderOtherService) *SecurityServiceImpl {
	
	return &SecurityServiceImpl{
		db: securityDatabase,
		orderService: orderService,
		orderOtherService: orderOtherService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type SecurityService interface{
	FindAllSecurityConfigs(ctx context.Context, token string) ([]SecurityConfig, error)
	Create(ctx context.Context, name, value, description, token string) (SecurityConfig, error)
	Update(ctx context.Context, id, name, value, description, token string) (SecurityConfig, error)
	Delete(ctx context.Context, id, token string)(string, error)
	Check(ctx context.Context, accountId, token string)(string, error)
}

type SecurityServiceImpl struct{
	db components.NoSQLDatabase
	orderService OrderService
	orderOtherService OrderOtherService
	roles []string
}


func(ssi *SecurityServiceImpl) FindAllSecurityConfigs(ctx context.Context, token string) ([]SecurityConfig, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return nil, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("securityConfig")
	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var scs []SecurityConfig
	err = result.All(&scs)
	if err != nil {
		return nil, err
	}
	return scs, nil
}

func(ssi *SecurityServiceImpl) Create(ctx context.Context, id, name, value, description, token string) (SecurityConfig, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return SecurityConfig{}, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("securityConfig")
	
	query := fmt.Sprintf(`{"Name": %s }`, name)

	res, err := collection.FindOne(query) 

	if err == nil {
		var oldSc SecurityConfig

		res.Decode(&oldSc)
		
		if oldSc.Id != "" {
			return SecurityConfig{}, errors.New("Security config with given name already exists!")
		}
	}

	newSc := SecurityConfig{
		Id: uuid.New().String(),
		Name: name,
		Value: value,
		Description: description,
	}
	err = collection.InsertOne(newSc) 

	if err != nil{
		return SecurityConfig{}, err
	}

	return newSc, nil
}

func(ssi *SecurityServiceImpl) Update(ctx context.Context, id, name, value, description, token string) (SecurityConfig, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return SecurityConfig{}, err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("securityConfig")
	
	query := fmt.Sprintf(`{"Id": %s }`, id)

	res, err := collection.FindOne(query) 
	if err != nil {
		return SecurityConfig{}, err
	}
	var	existingSc SecurityConfig

	err = res.Decode(&existingSc)
	if err != nil {
		return SecurityConfig{}, err
	}

	updatedSc := SecurityConfig{
		Id: existingSc.Id,
		Name: name,
		Value: value,
		Description: description,
	}

	err = collection.ReplaceOne(query, updatedSc) 
	if err != nil{
		return SecurityConfig{}, err
	}

	return updatedSc, nil
}

func(ssi *SecurityServiceImpl) Delete(ctx context.Context, id, token string)(string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return "", err
	}

	collection := ssi.db.GetDatabase("ts").GetCollection("securityConfig")
	
	query := fmt.Sprintf(`{"Id": %s }`, id)

	err = collection.DeleteOne(query) 
	if err != nil {
		return "", err
	}

	return "Delete successful", nil
}

func(ssi *SecurityServiceImpl) Check(ctx context.Context, accountId, token string)(string, error){
	err := Authenticate(token, ssi.roles...)
	if err != nil {
		return "", err
	}

	dateFormat := "Sat Jul 26 00:00:00 2025"
	dtNow := time.Now().Format(dateFormat)

	orderResult, err := ssi.orderService.SecurityInfoCheck(ctx, dtNow, accountId, token)
	if err != nil {
		return "", err
	}

	orderOtherResult, err := ssi.orderOtherService.SecurityInfoCheck(ctx, dtNow, accountId, token)
	if err != nil {
		return "", err
	}

	orderInOneHour := orderResult["OrderNumInLastHour"] + orderOtherResult["OrderNumInLastHour"]
	totalValidOrders := orderResult["OrderNumOfValidOrder"] + orderOtherResult["OrderNumOfValidOrder"]

	collection := ssi.db.GetDatabase("ts").GetCollection("securityConfig")
	query := fmt.Sprintf(`{"Name": %s }`, "max_order_1_hour")

	res, _ := collection.FindOne(query)
	var maxInHourConfig SecurityConfig
	res.Decode(&maxInHourConfig)

	query = fmt.Sprintf(`{"Name": %s }`, "max_order_not_use")

	res, _ = collection.FindOne(query)
	var maxNotUseConfig SecurityConfig
	res.Decode(&maxNotUseConfig)

	oneHourLine, _ := strconv.ParseUint(maxInHourConfig.Value, 10, 32)
	totalValidLine, _ := strconv.ParseFloat(maxNotUseConfig.Value, 32)

	if orderInOneHour > uint16(oneHourLine) || totalValidOrders > uint16(totalValidLine){
		return "", errors.New("Too many orders in one hour or too many valid orders in total.")
	}
	
	return "Sucess", nil
}
