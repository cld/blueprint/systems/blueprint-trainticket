package services

import (
	"context"
	"time"
	"errors"
	"fmt"
	"sync"
)

func CreateCancelService (orderService OrderService, orderOtherService OrderOtherService, userService UserService, notificationService NotificationService, insidePaymentService InsidePaymentService) *CancelServiceImpl {
	
	return &CancelServiceImpl{
		orderService: orderService,
		orderOtherService: orderOtherService,
		userService: userService,
		notificationService: notificationService,
		insidePaymentService: insidePaymentService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type CancelService interface{
	Calculate(ctx context.Context, orderId, token string) (float32, error)
	CancelTicket(ctx context.Context, orderId, loginId, token string) (string, error)
}

type CancelServiceImpl struct{
	orderService OrderService
	orderOtherService OrderOtherService
	userService UserService
	notificationService NotificationService
	insidePaymentService InsidePaymentService
	roles []string
}

func(csi *CancelServiceImpl) Calculate(ctx context.Context, orderId, token string) (float32, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return 0.0, err
	}

	var order Order
	order, err = csi.orderService.GetOrderById(ctx, orderId, token)
	if err != nil {
		order, err = csi.orderOtherService.GetOrderById(ctx, orderId, token)
	}

	if err != nil {
		return 0.0, err
	}

	if OrderStatus(order.Status) == NotPaid{
		return 0.0, errors.New("Nothing to refund")
	}else if OrderStatus(order.Status) == Paid{
		nowDate := time.Now()
		trDate, _ := time.Parse(time.ANSIC, order.TravelDate)

		if nowDate.After(trDate){
			return 0.0, nil
		}else{
			price := order.Price * 0.8
			return price, nil
		}
	}else{
		return 0.0, errors.New("Refund not permitted")
	}
}

func(csi *CancelServiceImpl) CancelTicket(ctx context.Context, orderId, loginId, token string) (string, error){
	err := Authenticate(token, csi.roles...)
	if err != nil {
		return "", err
	}
	
	var order Order
	order, err = csi.orderService.GetOrderById(ctx, orderId, token)
	if err != nil {
		order, err = csi.orderOtherService.GetOrderById(ctx, orderId, token)
	}

	if err != nil {
		return "", err
	}

	status := OrderStatus(order.Status)
	if status != Paid && status != NotPaid && status != Change{
		return "", errors.New("Cancelation not permitted.")
	}

	nowDate := time.Now()
	trDate, _ := time.Parse(time.ANSIC, order.TravelDate)

	var refund string
	if nowDate.After(trDate) {
		refund = "0"
	}else{
		refund = fmt.Sprintf("%f", order.Price * 0.8)
	}

	var wg sync.WaitGroup
	wg.Add(2)
	var err1, err2 error

	var user User
	go func() { 
		defer wg.Done()
		_, err1 = csi.insidePaymentService.DrawBack(ctx, loginId, refund, token)
	}()
	go func() { 
		defer wg.Done()
		user, err2 = csi.userService.GetUserById(ctx, order.AccountId, token)
	}()
	wg.Wait()
	if err1 != nil{
		return "", err1
	}
	if err2 != nil {
		return "", err2
	}

	err = csi.notificationService.OrderCancelSuccess(ctx, NotificationInfo{
		Email: user.Email,
		OrderNumber: order.Id,
		Username: user.Username,
		StartingPlace: order.From,
		EndPlace: order.To,
		StartingTime: order.TravelDate,
		SeatClass: SeatClass(order.SeatClass).String(),
		Price: fmt.Sprintf("%f", order.Price),
	}, token)

	if err != nil {
		return "", nil
	}

	return "Cancelation successful", nil
}



