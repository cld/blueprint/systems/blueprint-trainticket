package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
)

func CreateFoodMapService (foodMapDatabase components.NoSQLDatabase) *FoodMapServiceImpl {
	
	return &FoodMapServiceImpl{db: foodMapDatabase}
}

type FoodMapService interface{
	GetAllFoodStores(ctx context.Context, token string)([]Store, error)
	GetFoodStoresOfStation(ctx context.Context, stationId, token string) ([]Store, error)
	GetFoodStoresByStationIds(ctx context.Context, stationIds []string, token string) ([]Store, error)
	GetAllTrainFood(ctx context.Context, token string) ([]Food, error)
	GetTrainFoodOfTrip(ctx context.Context, tripId, token string) ([]Food, error)
}

type FoodMapServiceImpl struct{
	db components.NoSQLDatabase
}

func (fsi *FoodMapServiceImpl) GetAllFoodStores(ctx context.Context, token string)([]Store, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := fsi.db.GetDatabase("ts").GetCollection("stores")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var stores []Store
	err = result.All(&stores)
	if err != nil{
		return nil, err
	}

	return stores, nil
}

func (fsi *FoodMapServiceImpl) GetFoodStoresOfStation(ctx context.Context, stationId, token string) ([]Store, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := fsi.db.GetDatabase("ts").GetCollection("stores")

	query := fmt.Sprintf(`{"StationId": %s }`, stationId)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var stores []Store
	err = result.All(&stores)
	if err != nil{
		return nil, err
	}

	return stores, nil
}

func (fsi *FoodMapServiceImpl) GetFoodStoresByStationIds(ctx context.Context, stationIds []string, token string) ([]Store, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := fsi.db.GetDatabase("ts").GetCollection("stores")

	query := fmt.Sprintf(`{"StationId": {"$in": %v} }`, stationIds)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var stores []Store
	err = result.All(&stores)
	if err != nil{
		return nil, err
	}

	return stores, nil
}

func (fsi *FoodMapServiceImpl) GetAllTrainFood(ctx context.Context, token string) ([]Food, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := fsi.db.GetDatabase("ts").GetCollection("trainfoods")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var foods []Food
	err = result.All(&foods)
	if err != nil{
		return nil, err
	}

	return foods, nil
}

func (fsi *FoodMapServiceImpl) GetTrainFoodOfTrip(ctx context.Context, tripId, token string) ([]Food, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := fsi.db.GetDatabase("ts").GetCollection("trainfoods")

	query := fmt.Sprintf(`{"TripId": %s }`, tripId)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var foods []Food
	err = result.All(&foods)
	if err != nil{
		return nil, err
	}

	return foods, nil
}
