package services

import "context"

func CreateTicketInfoService (basicService BasicService) *TicketInfoServiceImpl {
	
	return &TicketInfoServiceImpl{basicService: basicService}
}

type TicketInfoService interface{
	QueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error)
	QueryForStationId(ctx context.Context, name, token string) (string, error)
}

type TicketInfoServiceImpl struct{
	basicService BasicService
}

func(tisi *TicketInfoServiceImpl) QueryForTravel(ctx context.Context, info Travel, token string) (TravelResult, error){
	err := Authenticate(token)
	if err != nil {
		return TravelResult{}, err
	}

	tr, err := tisi.basicService.QueryForTravel(ctx, info, token)
	if err != nil {
		return TravelResult{}, err
	}

	return tr, nil
}

func(tisi *TicketInfoServiceImpl) QueryForStationId(ctx context.Context, name, token string) (string, error){
	err := Authenticate(token)
	if err != nil {
		return "", err
	}

	stationId, err := tisi.basicService.QueryForStationId(ctx, name, token)
	if err != nil {
		return "", err
	}

	return stationId, nil
}

