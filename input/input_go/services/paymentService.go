package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"github.com/google/uuid"
	"fmt"
)

func CreatePaymentService (paymentDatabase components.NoSQLDatabase) *PaymentServiceImpl {
	
	return &PaymentServiceImpl{
		db: paymentDatabase,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type PaymentService interface{
	Query(ctx context.Context, token string)([]Payment, error)
	Pay(ctx context.Context, orderId, price, userId, token string) (Payment, error)
	AddMoney(ctx context.Context, userId, price, token string)(Payment, error)
}

type PaymentServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}


func(psi *PaymentServiceImpl) Query(ctx context.Context, token string)([]Payment, error){
	err := Authenticate(token, psi.roles...)
	if err != nil {
		return nil, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("payment")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var payments []Payment
	err = result.All(&payments)
	if err != nil{
		return nil, err
	}

	return payments, nil
}

func(psi *PaymentServiceImpl) Pay(ctx context.Context, orderId, price, userId, token string) (Payment, error){
	err := Authenticate(token, psi.roles...)
	if err != nil {
		return Payment{}, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("payment")
	query := fmt.Sprintf(`{"OrderId": %s }`, orderId)

	res, err := collection.FindOne(query)
	if err == nil {
		var oldPayment Payment
		res.Decode(&oldPayment)
		
		if oldPayment.Id != "" {
			return Payment{}, errors.New("Payment already exists for this order!")
		}
	}
	
	newPayment := Payment{
		Id: uuid.New().String(),
		UserId: userId,
		OrderId: orderId,
		Price: price,
	}

	err = collection.InsertOne(newPayment) 
	if err != nil{
		return Payment{}, err
	}

	return newPayment, nil
}

//! This func is deprecated
func(psi *PaymentServiceImpl) AddMoney(ctx context.Context, userId, price, token string)(Payment, error){
	err := Authenticate(token, psi.roles...)
	if err != nil {
		return Payment{}, err
	}

	collection := psi.db.GetDatabase("ts").GetCollection("payment")

	moneyToAdd := Payment{
		Id: uuid.New().String(),
		UserId: userId,
		Price: price,
	}

	err = collection.InsertOne(moneyToAdd) 
	if err != nil{
		return Payment{}, err
	}

	return moneyToAdd, nil
}


