package services

import (
	"errors"
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
)

func CreateTrainService (trainDatabase components.NoSQLDatabase) *TrainServiceImpl {
	
	return &TrainServiceImpl{
		db: trainDatabase,
		roles: []string{"ROLE_ADMIN"},
	}
}

type TrainService interface{
	Create(ctx context.Context, train Train, token string) (Train, error)
	Update(ctx context.Context, train Train, token string) (Train, error)
	Delete(ctx context.Context, Id, token string) (string, error)
	Query(ctx context.Context, token string)([]Train, error)
	Retrieve(ctx context.Context, Id, token string)(Train, error)
}

type TrainServiceImpl struct{
	db components.NoSQLDatabase
	roles []string
}

//* This endpoint expects the `train` object to have an ID
func(tsi *TrainServiceImpl) Create(ctx context.Context, train Train, token string) (Train, error){

	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return Train{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trainType")
	
	query := fmt.Sprintf(`{"Id": %s }`, train.Id)

	res, err := collection.FindOne(query) 

	if err == nil {
		var oldTrain Train

		res.Decode(&oldTrain)
		
		if oldTrain.Id != "" {
			return Train{}, errors.New("Train type already exists!")
		}
	}

	
	err = collection.InsertOne(train) 
	if err != nil{
		return Train{}, err
	}

	return train, nil
}

func(tsi *TrainServiceImpl) Update(ctx context.Context, train Train, token string) (Train, error){
	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return Train{}, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trainType")
	
	query := fmt.Sprintf(`{"Id": %s }`, train.Id)

	res, err := collection.FindOne(query) 
	if err != nil {
		return Train{}, err
	}

	var existingTrain Train
	res.Decode(&existingTrain)

	if existingTrain.Id == ""{
		return Train{}, errors.New("Could not update train type!")
	}
	
	query = fmt.Sprintf(`{"Id": %s}`, existingTrain.Id)
	update := fmt.Sprintf(`{"$set": {"EconomyClass": %d, "ComfortClass": %d, "AvgSpeed": %d}`, train.EconomyClass, train.ComfortClass, train.AvgSpeed)

	err = collection.UpdateOne(query, update)
	if err != nil{
		return Train{}, err
	}

	return train, nil
}

func(tsi *TrainServiceImpl) Delete(ctx context.Context, Id, token string) (string, error){
	err := Authenticate(token, tsi.roles[0])
	if err != nil {
		return "", err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trainType")
	
	query := fmt.Sprintf(`{"Id": %s }`, Id)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "Train type removed successfully", nil
}

func(tsi *TrainServiceImpl) Query(ctx context.Context, token string)([]Train, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := tsi.db.GetDatabase("ts").GetCollection("trainType")
	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var trains []Train
	err = result.All(&trains)
	if err != nil {
		return nil, err
	}
	return trains, nil
}

func(tsi *TrainServiceImpl) Retrieve(ctx context.Context, Id, token string)(Train, error){

	err := Authenticate(token)

	if err != nil {
		return Train{}, err
	}

	query := fmt.Sprintf(`{"Id": %s }`, Id)
	collection := tsi.db.GetDatabase("ts").GetCollection("trainType")

	res, err := collection.FindOne(query) 
	if err != nil {
		return Train{}, err
	}

	var existingTrain Train

	res.Decode(&existingTrain)

	return existingTrain, nil
}
