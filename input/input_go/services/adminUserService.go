package services

import (
	"context"
)

func CreateAdminUserService (userService UserService) *AdminUserServiceImpl {
	return &AdminUserServiceImpl{
		userService: userService,
		roles: []string{"ROLE_ADMIN"},
	}
}

type AdminUserService interface{
	GetAllUsers(ctx context.Context, token string) ([]User, error)
	AddUser(ctx context.Context, user User, token string) (User, error)
	UpdateUser(ctx context.Context, user User, token string) (User, error)
	DeleteUser(ctx context.Context, userId, token string) (string, error)	
}

type AdminUserServiceImpl struct{
	userService UserService
	roles []string
}



func (ausi* AdminUserServiceImpl) GetAllUsers(ctx context.Context, token string) ([]User, error){

	err := Authenticate(token, ausi.roles...)
	if err != nil {
		return nil, err
	}
	
	return ausi.userService.GetAllUsers(ctx, token)
}

func (ausi* AdminUserServiceImpl) AddUser(ctx context.Context, user User, token string) (User, error){

	err := Authenticate(token, ausi.roles...)
	if err != nil {
		return User{}, err
	}
	
	return ausi.userService.RegisterUser(ctx, user, token)
}

func (ausi* AdminUserServiceImpl) UpdateUser(ctx context.Context, user User, token string) (User, error){

	err := Authenticate(token, ausi.roles...)
	if err != nil {
		return User{}, err
	}
	
	return ausi.userService.UpdateUser(ctx, user, token)
}


func (ausi* AdminUserServiceImpl) DeleteUser(ctx context.Context, userId, token string) (string, error){

	err := Authenticate(token, ausi.roles...)
	if err != nil {
		return "", err
	}
	
	return ausi.userService.DeleteUserById(ctx, userId, token)
}
