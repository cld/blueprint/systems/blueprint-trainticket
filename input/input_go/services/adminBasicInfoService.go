package services

import (
	"context"
)

func CreateAdminBasicInfoService (stationService StationService, trainService TrainService, configService ConfigService, priceService PriceService, contactService ContactService) *AdminBasicInfoServiceImpl {
	return &AdminBasicInfoServiceImpl{
		stationService: stationService,
		trainService: trainService,
		configService: configService,
		priceService: priceService,
		contactService: contactService,
		roles: []string{"ROLE_ADMIN"},
	}
}

type AdminBasicInfoService interface{
	GetAllContacts(ctx context.Context, token string) ([]Contact, error)
	DeleteContacts(ctx context.Context, contactId, token string) (string, error)
	ModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	AddContacts(ctx context.Context, contact Contact, token string) (Contact, error)
	GetAllStations(ctx context.Context, token string) ([]Station, error)
	DeleteStation(ctx context.Context, station Station, token string) (string, error)
	GetAllTrains(ctx context.Context, token string) ([]Train, error)
	DeleteTrain(ctx context.Context, trainId string, token string) (string, error)
	ModifyTrain(ctx context.Context, train Train, token string) (Train, error)
	AddTrain(ctx context.Context, train Train, token string) (Train, error)
	GetAllPrices(ctx context.Context, token string)([]PriceConfig, error)
	DeletePrice(ctx context.Context, pc PriceConfig, token string) (string, error)
	ModifyPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
	AddPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error)
	//* Token-less
	GetAllConfigs(ctx context.Context) ([]Config, error)
	DeleteConfig(ctx context.Context, name string, token string) (string, error)
	ModifyConfig(ctx context.Context, config Config) (Config, error)
	AddConfig(ctx context.Context, config Config) (Config, error)
}

type AdminBasicInfoServiceImpl struct{
	stationService StationService
	trainService TrainService
	configService ConfigService
	priceService PriceService
	contactService ContactService
	roles []string
}


func (abis* AdminBasicInfoServiceImpl) GetAllContacts(ctx context.Context, token string) ([]Contact, error){

	err := Authenticate(token, abis.roles...)

	if err != nil {
		return nil, err
	}

	return abis.contactService.GetAllContacts(ctx, token)
}

func (abis* AdminBasicInfoServiceImpl) DeleteContacts(ctx context.Context, contactId, token string) (string, error){

	err := Authenticate(token, abis.roles...)

	if err != nil {
		return "", err
	}

	return abis.contactService.DeleteContacts(ctx, contactId, token)
}

func (abis* AdminBasicInfoServiceImpl)	ModifyContacts(ctx context.Context, contact Contact, token string) (Contact, error){

	err := Authenticate(token, abis.roles...)

	if err != nil {
		return Contact{}, err
	}

	return  abis.contactService.ModifyContacts(ctx, contact, token)
}

func (abis* AdminBasicInfoServiceImpl) AddContacts(ctx context.Context, contact Contact, token string) (Contact, error){

	err := Authenticate(token, abis.roles...)

	if err != nil {
		return Contact{}, err
	}

	return abis.contactService.CreateNewContactsAdmin(ctx, contact, token)
}

func (abis* AdminBasicInfoServiceImpl) GetAllStations(ctx context.Context, token string) ([]Station, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return nil, err
	}

	return abis.stationService.Query(ctx, token)
}

func (abis* AdminBasicInfoServiceImpl) DeleteStation(ctx context.Context, station Station, token string) (string, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return "", err
	}

	return abis.stationService.Delete(ctx, station, token)
}

func (abis* AdminBasicInfoServiceImpl) GetAllTrains(ctx context.Context, token string) ([]Train, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return nil, err
	}

	return abis.trainService.Query(ctx, token)
}

func (abis* AdminBasicInfoServiceImpl) DeleteTrain(ctx context.Context, trainId string, token string) (string, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return "", err
	}

	return abis.trainService.Delete(ctx, trainId, token)
}

func (abis* AdminBasicInfoServiceImpl) ModifyTrain(ctx context.Context, train Train, token string) (Train, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return Train{}, err
	}

	return abis.trainService.Update(ctx, train, token)
}

func (abis* AdminBasicInfoServiceImpl) AddTrain(ctx context.Context, train Train, token string) (Train, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return Train{}, err
	}

	return abis.trainService.Create(ctx, train, token)
}

func (abis* AdminBasicInfoServiceImpl) GetAllPrices(ctx context.Context, token string)([]PriceConfig, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return nil, err
	}

	return abis.priceService.QueryAll(ctx, token)
}

func (abis* AdminBasicInfoServiceImpl) DeletePrice(ctx context.Context, pc PriceConfig, token string) (string, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return "", err
	}

	return abis.priceService.Delete(ctx, pc, token)
}

func (abis* AdminBasicInfoServiceImpl) ModifyPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return PriceConfig{}, err
	}

	return abis.priceService.Update(ctx, pc, token)
}

func (abis* AdminBasicInfoServiceImpl) AddPrice(ctx context.Context, pc PriceConfig, token string) (PriceConfig, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return PriceConfig{}, err
	}

	return abis.priceService.Create(ctx, pc, token)
}

//*******************************************************************************************************

func (abis* AdminBasicInfoServiceImpl) GetAllConfigs(ctx context.Context, token string) ([]Config, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return nil, err
	}

	return abis.configService.QueryAll(ctx)
}

func (abis* AdminBasicInfoServiceImpl) DeleteConfig(ctx context.Context, name, token string) (string, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return "", err
	}

	return abis.configService.DeleteConfig(ctx, name)
}

func (abis* AdminBasicInfoServiceImpl) ModifyConfig(ctx context.Context, config Config, token string) (Config, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return Config{}, err
	}
	
	return abis.configService.UpdateConfig(ctx, config)
}

func (abis* AdminBasicInfoServiceImpl) AddConfig(ctx context.Context, config Config, token string) (Config, error){

	err := Authenticate(token, abis.roles...)
	if err != nil {
		return Config{}, err
	}

	return abis.configService.CreateConfig(ctx, config)
}