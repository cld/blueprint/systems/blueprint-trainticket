package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
)


func CreateVoucherService (voucherDatabase components.RelationalDB, orderService OrderService, orderOtherService OrderOtherService) *VoucherServiceImpl {
	
	return &VoucherServiceImpl{
		db: voucherDatabase,
		orderService: orderService,
		orderOtherService: orderOtherService,
	}
}

type VoucherService interface{
	Post(ctx context.Context, orderId string, typ string, token string) (Voucher, error)
	GetVoucher(ctx context.Context, orderId string, token string) (Voucher, error)
}

type VoucherServiceImpl struct{
	db components.RelationalDB
	orderService OrderService
	orderOtherService OrderOtherService
}

func(vsi *VoucherServiceImpl) Post(ctx context.Context, orderId string, typ string, token string) (Voucher, error){

	err := Authenticate(token)
	if err != nil {
		return Voucher{}, err
	}

	db, err := vsi.db.Open("user", "pass", "data")
	if err != nil {
        return Voucher{}, err
    }
	defer db.Close()

	findQuery:= "SELECT * FROM voucher where order_id = ? LIMIT 1"
    res, err := db.Query(findQuery, orderId)
    if err != nil {
        return Voucher{}, err
    }

	var voucher Voucher

	//* only get one voucher
	if res.Next(){
		res.Scan(&voucher.VoucherId, &voucher.OrderId, &voucher.TravelDate, &voucher.ContactName, &voucher.TrainNumber, &voucher.SeatClass, &voucher.SeatNumber, &voucher.StartStation, &voucher.DestStation, &voucher.Price)
		return voucher, nil
	}

	//* Insert

	var order Order
	if typ == "0" {
		order, err = vsi.orderService.GetOrderById(ctx, orderId, token)
	} else{
		order, err = vsi.orderOtherService.GetOrderById(ctx, orderId, token)
	}

	query := "INSERT INTO voucher (order_id,travelDate,contactName,trainNumber,seatClass,seatNumber,startStation,destStation,price)VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);"

	err = db.Exec(query, order.Id, order.TravelDate, order.ContactsName, order.TrainNumber, order.SeatClass, order.SeatNumber, order.From, order.To, order.Price)
	if err != nil {
		return Voucher{}, err
	}
	
	//! TODO this part is redundant
	//? Double check this

	res, err = db.Query(findQuery, orderId)
    if err != nil {
        return Voucher{}, err
    }

	var insertedVoucher Voucher

	if res.Next(){
		res.Scan(&insertedVoucher.VoucherId, &insertedVoucher.OrderId, &insertedVoucher.TravelDate, &insertedVoucher.ContactName, &insertedVoucher.TrainNumber, &insertedVoucher.SeatClass, &insertedVoucher.SeatNumber, &insertedVoucher.StartStation, &insertedVoucher.DestStation, &insertedVoucher.Price)
		return insertedVoucher, nil
	}

	return Voucher{}, nil
}

func(vsi *VoucherServiceImpl) GetVoucher(ctx context.Context, orderId, token string) (Voucher, error){
	
	err := Authenticate(token)
	if err != nil {
		return Voucher{}, err
	}

	db, err := vsi.db.Open("user", "pass", "data")
	if err != nil {
        return Voucher{}, err
    }
	defer db.Close()

	findQuery:= "SELECT * FROM voucher where order_id = ? LIMIT 1"

	res, err := db.Query(findQuery, orderId)
    if err != nil {
        return Voucher{}, err
    }

	var voucher Voucher

	if res.Next(){
		res.Scan(&voucher.VoucherId, &voucher.OrderId, &voucher.TravelDate, &voucher.ContactName, &voucher.TrainNumber, &voucher.SeatClass, &voucher.SeatNumber, &voucher.StartStation, &voucher.DestStation, &voucher.Price)
		return voucher, nil
	}

	return Voucher{}, nil
}

