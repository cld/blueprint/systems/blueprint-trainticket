package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"io/ioutil"
	"fmt"
)

func CreateTicketOfficeService (ticketOfficeDatabase components.NoSQLDatabase) *TicketOfficeServiceImpl {
	
	return &TicketOfficeServiceImpl{db: ticketOfficeDatabase}
}

type TicketOfficeService interface{
	GetRegionList(ctx context.Context, token string)(string, error)
	GetAll(ctx context.Context, token string) ([]Office, error)
	GetSpecificOffices(ctx context.Context, province, city, region, token string) ([]Office, error)
	AddOffice(ctx context.Context, province, city, region, token string, office Office) (string, error)
	DeleteOffice(ctx context.Context, province, city, region, officeName, token string) (string, error)
	UpdateOffice(ctx context.Context, province, city, region, oldOfficeName, token string, office Office) (string, error)
}

type TicketOfficeServiceImpl struct{
	db components.NoSQLDatabase
}

func(tosi *TicketOfficeServiceImpl) GetRegionList(ctx context.Context, token string)(string, error){

	err := Authenticate(token)
	if err != nil {
		return "", err
	}

	regions, err := ioutil.ReadFile("./schema.sql")
	if err != nil {
		return "", err
	}

	return string(regions), nil
}

func(tosi *TicketOfficeServiceImpl) GetAll(ctx context.Context, token string) ([]Office, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := tosi.db.GetDatabase("ts").GetCollection("office")

	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var offices []Office
	err = result.All(&offices)
	if err != nil{
		return nil, err
	}

	return offices, nil
}

func(tosi *TicketOfficeServiceImpl) GetSpecificOffices(ctx context.Context, province, city, region, token string) ([]Office, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := tosi.db.GetDatabase("ts").GetCollection("office")

	query := fmt.Sprintf(`{"Province": %s, "City": %s, "Region": %s}`, province, city, region)

	result, err := collection.FindMany(query)
	if err != nil{
		return nil, err
	}

	var offices []Office
	err = result.All(&offices)
	if err != nil{
		return nil, err
	}

	return offices, nil
}

func(tosi *TicketOfficeServiceImpl) AddOffice(ctx context.Context, province, city, region, token string, office Office) (string, error){
	err := Authenticate(token)
	if err != nil {
		return "", err
	}
	
	collection := tosi.db.GetDatabase("ts").GetCollection("office")
	query := fmt.Sprintf(`{"Province": %s, "City": %s, "Region": %s}`, province, city, region)

	update := fmt.Sprintf(`{"$push": {"Offices": %v }}`, office) //TODO check if this works @choice
	
	err = collection.UpdateOne(query, update) 
	if err != nil{
		return "", err	
	}

	return "Office added", nil
}

func(tosi *TicketOfficeServiceImpl) DeleteOffice(ctx context.Context, province, city, region, officeName, token string) (string, error){
	err := Authenticate(token)
	if err != nil {
		return "", err
	}
	
	collection := tosi.db.GetDatabase("ts").GetCollection("office")
	query := fmt.Sprintf(`{"Province": %s, "City": %s, "Region": %s}`, province, city, region)

	update := fmt.Sprintf(`{"$pull": {"Offices": {"OfficeName": %s}}}`, officeName) //TODO check if this works @choice
	
	err = collection.UpdateOne(query, update) 
	if err != nil{
		return "", err	
	}

	return "Office deleted", nil
}

func(tosi *TicketOfficeServiceImpl) UpdateOffice(ctx context.Context, province, city, region, oldOfficeName, token string, office Office) (string, error){
	err := Authenticate(token)
	if err != nil {
		return "", err
	}
	
	collection := tosi.db.GetDatabase("ts").GetCollection("office")
	query := fmt.Sprintf(`{"Province": %s, "City": %s, "Region": %s, "Offices.OfficeName": %s}`, province, city, region, oldOfficeName)
	
	update := fmt.Sprintf(`{"$set": {"Offices": [%v]} }`, office) //TODO check if this works @choice
	
	err = collection.UpdateOne(query, update) 
	if err != nil{
		return "", err	
	}

	return "Office updated", nil
}

