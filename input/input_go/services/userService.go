package services

import (
	"context"
	"gitlab.mpi-sws.org/cld/blueprint/blueprint-compiler/stdlib/components"
	"fmt"
	"github.com/google/uuid"
	"errors"
)

func CreateUserService (userDatabase components.NoSQLDatabase, authService AuthService) *UserServiceImpl {
	return &UserServiceImpl{
		db: userDatabase,
		authService: authService,
		roles: []string{"ROLE_ADMIN", "ROLE_USER"},
	}
}

type UserService interface{
	GetAllUsers(ctx context.Context, token string)([]User, error)
	GetUserById(ctx context.Context, userId, token string) (User, error)
	GetUserByUsername(ctx context.Context, username, token string) (User, error)
	DeleteUserById(ctx context.Context, userId, token string) (string, error)
	UpdateUser(ctx context.Context, userData User, token string) (User, error)
	RegisterUser(ctx context.Context, userData User, token string) (User, error)
}

type UserServiceImpl struct{
	db components.NoSQLDatabase
	authService AuthService
	roles []string
}

func(usi* UserServiceImpl) GetAllUsers(ctx context.Context, token string)([]User, error){
	err := Authenticate(token)
	if err != nil {
		return nil, err
	}

	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")
	result, err := collection.FindMany("") //TODO verify this query-string works!
	if err != nil{
		return nil, err
	}

	var users []User
	err = result.All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func(usi* UserServiceImpl) GetUserById(ctx context.Context, userId, token string) (User, error){
	err := Authenticate(token)
	if err != nil {
		return User{}, err
	}

	query := fmt.Sprintf(`{"UserId": %s }`, userId)
	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")

	res, err := collection.FindOne(query) 
	if err != nil {
		return User{}, err
	}

	var existingUser User

	res.Decode(&existingUser)

	return existingUser, nil
}

func(usi* UserServiceImpl) GetUserByUsername(ctx context.Context, username, token string) (User, error){
	err := Authenticate(token)
	if err != nil {
		return User{}, err
	}

	query := fmt.Sprintf(`{"Username": %s }`, username)
	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")

	res, err := collection.FindOne(query) 
	if err != nil {
		return User{}, err
	}

	var existingUser User

	res.Decode(&existingUser)

	return existingUser, nil
}

func(usi* UserServiceImpl) DeleteUserById(ctx context.Context, userId, token string) (string, error){
	err := Authenticate(token, usi.roles...)
	if err != nil {
		return "", err
	}

	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")
	
	query := fmt.Sprintf(`{"UserId": %s }`, userId)

	err = collection.DeleteOne(query)
	if err != nil{
		return "", err
	}

	return "User removed successfully", nil
}

//* userData:User should not contain the userId
func(usi* UserServiceImpl) UpdateUser(ctx context.Context, userData User, token string) (User, error){
	err := Authenticate(token)
	if err != nil {
		return User{}, err
	}


	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")
	query := fmt.Sprintf(`{"Username": %s }`, userData.Username)

	res, err := collection.FindOne(query) 
	if err != nil {
		return User{}, err
	}

	var existingUser User
	res.Decode(&existingUser)

	delQuery := fmt.Sprintf(`{"UserId": %s }`, existingUser.UserId)
	err = collection.DeleteOne(delQuery)
	if err != nil{
		return User{}, err
	}

	userData.UserId = existingUser.UserId
	err = collection.InsertOne(userData)
	if err != nil{
		return User{}, err
	}

	return userData, nil
}

//* userData:User should not contain the userId
func(usi* UserServiceImpl) RegisterUser(ctx context.Context, userData User, token string) (User, error){
	err := Authenticate(token)
	if err != nil {
		return User{}, err
	}

	collection := usi.db.GetDatabase("ts-user-mongo").GetCollection("user")
	query := fmt.Sprintf(`{"Username": %s }`, userData.Username)

	res, err := collection.FindOne(query) 
	if err == nil {
		var existingUser User
		res.Decode(&existingUser)
		if existingUser.UserId != ""{
			return User{}, errors.New("User with this username already registered")
		}
	}


	userData.UserId = uuid.New().String()
	err = collection.InsertOne(userData)
	if err != nil{
		return User{}, err
	}

	_, err = usi.authService.CreateDefaultUser(ctx, userData, token)
	if err != nil {
		return User{}, err
	}


	return userData, nil
}

