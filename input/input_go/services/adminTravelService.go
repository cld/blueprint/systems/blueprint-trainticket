package services

import (
	"errors"
	"sync"
	"context"
)

func CreateAdminTravelService (travelService TravelService, travel2Service Travel2Service) *AdminTravelServiceImpl {
	return &AdminTravelServiceImpl{
		travelService: travelService,
		travel2Service: travel2Service,
		roles: []string{"ROLE_ADMIN"},
	}
}

type AdminTravelService interface{
	GetAllTravels(ctx context.Context, token string) ([]Trip, []Train, []Route, error)
	AddTravel(ctx context.Context, trip Trip, token string) (Trip, error)
	UpdateTravel(ctx context.Context, trip Trip, token string) (Trip, error)
	DeleteTravel(ctx context.Context, tripId, token string) (string, error)
}

type AdminTravelServiceImpl struct{
	travelService TravelService
	travel2Service Travel2Service
	roles []string
}

func (atsi* AdminTravelServiceImpl) GetAllTravels(ctx context.Context, token string) ([]Trip, []Train, []Route, error){

	err := Authenticate(token, atsi.roles...)
	if err != nil {
		return nil, nil, nil, err
	}
	
	var err1, err2 error
	var trip1, trip2 []Trip
	var trainType1, trainType2 []Train
	var route1, route2 []Route
	var wg sync.WaitGroup
	wg.Add(2)

	go func() { 
		defer wg.Done()
		trip1, trainType1, route1, err1 = atsi.travelService.AdminQueryAll(ctx, token)
	}()

	go func() { 
		defer wg.Done()
		trip2, trainType2, route2, err2 = atsi.travel2Service.AdminQueryAll(ctx, token)
	}()

	wg.Wait()

	if err1 != nil{
		return nil, nil, nil, err1
	}
	if err2 != nil{
		return nil, nil, nil, err2
	}

	
	trips := append(trip1, trip2...)
	trainTypes := append(trainType1, trainType2...)
	routes := append(route1, route2...)

	if len(trips) == 0{
		return nil, nil, nil, errors.New("No trips found")
	}

	return trips, trainTypes, routes, nil
}

func (atsi* AdminTravelServiceImpl) AddTravel(ctx context.Context, trip Trip, token string) (Trip, error){

	err := Authenticate(token, atsi.roles...)
	if err != nil {
		return Trip{}, err
	}
	
	if trip.Id[0:1] == "D" || trip.Id[0:1] == "G"{
		trip, err = atsi.travelService.CreateTrip(ctx, trip, token)
	} else{
		trip, err = atsi.travel2Service.CreateTrip(ctx, trip, token)
	}

	if err != nil {
		return Trip{}, err
	}

	return trip, nil
}


func (atsi* AdminTravelServiceImpl) UpdateTravel(ctx context.Context, trip Trip, token string) (Trip, error){

	err := Authenticate(token, atsi.roles...)
	if err != nil {
		return Trip{}, err
	}
	
	if trip.Id[0:1] == "D" || trip.Id[0:1] == "G"{
		trip, err = atsi.travelService.UpdateTrip(ctx, trip, token)
	} else{
		trip, err = atsi.travel2Service.UpdateTrip(ctx, trip, token)
	}

	if err != nil {
		return Trip{}, err
	}

	return trip, nil
}

func (atsi* AdminTravelServiceImpl) DeleteRoute(ctx context.Context, tripId, token string) (string, error){
	err := Authenticate(token, atsi.roles...)
	if err != nil {
		return "", err
	}
	
	if tripId[0:1] == "D" || tripId[0:1] == "G"{
		_, err = atsi.travelService.DeleteTrip(ctx, tripId, token)
	}else{
		_, err = atsi.travel2Service.DeleteTrip(ctx, tripId, token)
	}
	if err != nil {
		return "", err
	}

	return "Trip deleted.", nil	
}
