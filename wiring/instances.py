default_client_opts : Modifier = ClientPool(max_clients=10)
web_conn : Modifier = WebServer(framework="default")
normal_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': True})
hidden_ports_deployer : Modifier = Deployer(framework="docker", deployer_opts={'public_ports': False})

#Tracing
jaegerTracer : Tracer = JaegerTracer().WithServer(normal_deployer)
jaegerTracerModifier: Callable[str, Modifier] = lambda x : TracerModifier(tracer=jaegerTracer, tracer_opts={service_name : x, sampling_rate: 1})

web_server_modifiers : Callable[str, List[Modifier]] = lambda x : [jaegerTracerModifier(x), web_conn, normal_deployer]
queue_modifiers : Callable[str, List[Modifier]]  = lambda x : [jaegerTracerModifier(x), hidden_ports_deployer]

#Verification-Code Service
verificationCodeService : Service = VerificationCodeServiceImpl().WithServer(web_server_modifiers("verificationCodeService")).WithClient(default_client_opts)

#Authentication Service + DB
authDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
authService : Service = AuthServiceImpl(authDatabase = authDatabase, verificationCodeService = verificationCodeService).WithServer(web_server_modifiers("authService")).WithClient(default_client_opts)

#User Service + DB
userDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
userService : Service = UserServiceImpl(userDatabase = userDatabase, authService = authService).WithServer(web_server_modifiers("userService")).WithClient(default_client_opts)

#Admin-User Service
adminUserService : Service = AdminUserServiceImpl(userService = userService).WithServer(web_server_modifiers("adminUserService")).WithClient(default_client_opts)

#Station Service + DB
stationDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
stationService : Service = StationServiceImpl(stationDatabase = stationDatabase).WithServer(web_server_modifiers("stationService")).WithClient(default_client_opts)

#Order Service + DB
orderDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
orderService : Service = OrderServiceImpl(orderDatabase = orderDatabase, stationService = stationService).WithServer(web_server_modifiers("orderService")).WithClient(default_client_opts)

#Order-Other Service + DB   
orderOtherDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
orderOtherService : Service = OrderOtherServiceImpl(orderOtherDatabase = orderOtherDatabase, stationService = stationService).WithServer(web_server_modifiers("orderOtherService")).WithClient(default_client_opts)

#Admin-Order Service
adminOrderService : Service = AdminOrderServiceImpl(orderService = orderService, orderOtherService = orderOtherService).WithServer(web_server_modifiers("adminOrderService")).WithClient(default_client_opts)

#Route service
routeDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
routeService : Service = RouteServiceImpl(routeDatabase = routeDatabase).WithServer(web_server_modifiers("routeService")).WithClient(default_client_opts)

#Admin-Route Service
adminRouteService : Service = AdminRouteServiceImpl(routeService = routeService).WithServer(web_server_modifiers("adminRouteService")).WithClient(default_client_opts)

#Train Service + DB
trainDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
trainService : Service = TrainServiceImpl(trainDatabase = trainDatabase).WithServer(web_server_modifiers("trainService")).WithClient(default_client_opts)

#Price Service + DB
priceDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
priceService : Service = PriceServiceImpl(priceDatabase = priceDatabase).WithServer(web_server_modifiers("priceService")).WithClient(default_client_opts)

#Basic Service
basicService : Service = BasicServiceImpl(stationService = stationService, trainService = trainService, routeService = routeService, priceService = priceService).WithServer(web_server_modifiers("basicService")).WithClient(default_client_opts)

#Ticket-Info Service
ticketInfoService : Service = TicketInfoServiceImpl(basicService = basicService).WithServer(web_server_modifiers("ticketInfoService")).WithClient(default_client_opts)

#Travel Service + travel2DB
travelDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
travelService : Service = TravelServiceImpl(travelDatabase = travelDatabase, routeService = routeService, trainService = trainService, ticketInfoService = ticketInfoService, orderService = orderService, seatService = seatService).WithServer(web_server_modifiers("travelService")).WithClient(default_client_opts)

#Travel-2 Service + DB
travel2Database: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
travel2Service : Service = Travel2ServiceImpl(travel2Database = travel2Database, routeService = routeService, trainService = trainService, ticketInfoService = ticketInfoService, orderOtherService = orderOtherService, seatService = seatService).WithServer(web_server_modifiers("travel2Service")).WithClient(default_client_opts)

#Admin-Travel Service
adminTravelService: Service = AdminTravelServiceImpl(travelService = travelService, travel2Service = travel2Service).WithServer(web_server_modifiers("adminTravelService")).WithClient(default_client_opts)

#Config Service + DB
configDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
configService : Service = ConfigServiceImpl(configDatabase = configDatabase).WithServer(web_server_modifiers("configService")).WithClient(default_client_opts)

#Contact Service + DB
contactDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
contactService : Service = ContactServiceImpl(contactDatabase = contactDatabase).WithServer(web_server_modifiers("contactService")).WithClient(default_client_opts)

#Admin-Basic-Info Service
adminBasicInfoService: Service = AdminBasicInfoServiceImpl(stationService = stationService, trainService = trainService, configService = configService, priceService = priceService, contactService = contactService).WithServer(web_server_modifiers("adminBasicInfoService")).WithClient(default_client_opts)

#Seat Service
seatService: Service = SeatServiceImpl(travelService = travelService, travel2Service = travel2Service, orderService = orderService, orderOtherService = orderOtherService, configService = configService).WithServer(web_server_modifiers("seatService")).WithClient(default_client_opts)

#Insurance Service + DB
insuranceDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
insuranceService: Service = InsuranceServiceImpl(insuranceDatabase = insuranceDatabase).WithServer(web_server_modifiers("insuranceService")).WithClient(default_client_opts)

#Food-Map Service + DB
foodMapDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
foodMapService: Service = FoodMapServiceImpl(foodMapDatabase = foodMapDatabase).WithServer(web_server_modifiers("foodMapService")).WithClient(default_client_opts)

#Food Delivery Queue
foodDeliveryQueue : Queue = RabbitMQ(queue_name="food_delivery").WithServer(queue_modifiers("foodDeliveryQueue"))

#Food Service
foodDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
foodService: Service = FoodServiceImpl(foodDatabase = foodDatabase, foodMapService = foodMapService, travelService = travelService, stationService = stationService, foodDeliveryQueue = foodDeliveryQueue).WithServer(web_server_modifiers("foodService")).WithClient(default_client_opts)

#Security Service + DB
securityDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
securityService: Service = SecurityServiceImpl(securityDatabase = securityDatabase, orderService = orderService, orderOtherService = orderOtherService).WithServer(web_server_modifiers("securityService")).WithClient(default_client_opts)

#Consign-Price
consignPriceDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
consignPriceService: Service = ConsignPriceServiceImpl(consignPriceDatabase = consignPriceDatabase).WithServer(web_server_modifiers("consignPriceService")).WithClient(default_client_opts)

#Consign Service + DB
consignDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
consignService: Service = ConsignServiceImpl(consignDatabase = consignDatabase, consignPriceService = consignPriceService).WithServer(web_server_modifiers("consignService")).WithClient(default_client_opts)

#e-mail queue
emailQueue : Queue = RabbitMQ(queue_name="email").WithServer(queue_modifiers("emailQueue"))

#Preserve Service
preserveService: Service = PreserveServiceImpl(securityService = securityService, contactService = contactService, travelService = travelService, stationService = stationService, foodService = foodService, insuranceService = insuranceService, consignService = consignService, orderService = orderService, seatService = seatService, userService = userService, ticketInfoService = ticketInfoService).WithServer(web_server_modifiers("preserveService")).WithClient(default_client_opts)

#Preserve-Other Service
preserveOtherService: Service = PreserveOtherServiceImpl(securityService = securityService, contactService = contactService, travel2Service = travel2Service, stationService = stationService, foodService = foodService, insuranceService = insuranceService, consignService = consignService, orderOtherService = orderOtherService, seatService = seatService, userService = userService, ticketInfoService = ticketInfoService).WithServer(web_server_modifiers("preserveOtherService")).WithClient(default_client_opts)

#TODO emailQueue consumer part??
#Notification service
notificationService: Service = NotificationServiceImpl().WithServer(web_server_modifiers("notificationService")).WithClient(default_client_opts)

#Payment Service + DB 
paymentDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
paymentService: Service = PaymentServiceImpl(paymentDatabase = paymentDatabase).WithServer(web_server_modifiers("paymentService")).WithClient(default_client_opts)

#Inside-Payment Service + DB
insidePaymentDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
insidePaymentService: Service = InsidePaymentServiceImpl(insidePaymentDatabase = insidePaymentDatabase, orderService = orderService, orderOtherService = orderOtherService, paymentService = paymentService).WithServer(web_server_modifiers("insidePaymentService")).WithClient(default_client_opts)

#Cancel Service
cancelService: Service = CancelServiceImpl(orderService = orderService, orderOtherService = orderOtherService, userService = userService, notificationService = notificationService, insidePaymentService = insidePaymentService).WithServer(web_server_modifiers("cancelService")).WithClient(default_client_opts)

#Execute Service
executeService: Service = ExecuteServiceImpl(orderService = orderService, orderOtherService = orderOtherService).WithServer(web_server_modifiers("executeService")).WithClient(default_client_opts)

#Rebook Service
rebookService: Service = RebookServiceImpl(orderService = orderService, orderOtherService = orderOtherService, insidePaymentService = insidePaymentService, seatService = seatService, stationService = stationService, travelService = travelService, travel2Service = travel2Service).WithServer(web_server_modifiers("rebookService")).WithClient(default_client_opts)

#Delivery Service + DB
deliveryDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
consumer_queue_service_modifiers : Callable[str, List[Modifier]]  = lambda x : [jaegerTracerModifier(x), hidden_ports_deployer, web_conn]
deliveryService: QueueService = DeliveryServiceImpl(foodDeliveryQueue = foodDeliveryQueue, deliveryDatabase = deliveryDatabase).WithServer(consumer_queue_service_modifiers("deliveryService")).WithClient(default_client_opts)

#Voucher Service + (SQL) DB
voucherDatabase : RelationalDB = MySqlDB().WithServer(hidden_ports_deployer)
voucherService: Service = VoucherServiceImpl(voucherDatabase = voucherDatabase, orderService = orderService, orderOtherService = orderOtherService).WithServer(web_server_modifiers("voucherService")).WithClient(default_client_opts)

#Ticket-Office Service + Database
ticketOfficeDatabase: NoSQLDatabase = MongoDB().WithServer(hidden_ports_deployer)
ticketOfficeService: Service = TicketOfficeServiceImpl(ticketOfficeDatabase = ticketOfficeDatabase).WithServer(web_server_modifiers("ticketOfficeService")).WithClient(default_client_opts)

#Route-Plan Service
routePlanService: Service = RoutePlanServiceImpl(routeService = routeService, travelService = travelService, travel2Service = travel2Service, stationService = stationService).WithServer(web_server_modifiers("routePlanService")).WithClient(default_client_opts)

#Travel-Plan Service
travelPlanService: Service = TravelPlanServiceImpl(routePlanService = routePlanService, seatService = seatService, travelService = travelService, travel2Service = travel2Service, ticketInfoService = ticketInfoService, stationService = stationService).WithServer(web_server_modifiers("travelPlanService")).WithClient(default_client_opts)

#Front-End Service
frontEndService: Service = FrontEndServiceImpl(travelService = travelService, travel2Service = travel2Service, verificationCodeService = verificationCodeService, authService = authService, userService = userService, trainService = trainService, configService = configService, securityService = securityService, executeService = executeService, contactService = contactService, orderService = orderService, orderOtherService = orderOtherService, preserveService = preserveService, preserveOtherService = preserveOtherService, priceService = priceService, ticketInfoService = ticketInfoService, basicService = basicService, notificationService = notificationService, insidePaymentService = insidePaymentService, paymentService = paymentService, cancelService = cancelService, stationService = stationService, rebookService = rebookService, routeService = routeService, insuranceService = insuranceService, ticketOfficeService = ticketOfficeService, travelPlanService = travelPlanService, routePlanService = routePlanService, foodService = foodService, adminBasicInfoService = adminBasicInfoService, adminOrderService = adminOrderService, adminRouteService = adminRouteService, adminTravelService = adminTravelService, adminUserService = adminUserService).WithServer(web_server_modifiers("frontEndService")).WithClient(default_client_opts)